ForSpec is a constraint logic-based specification language over algebraic data types for writing formal structural and behavioral specifications.
The syntax and semantics of ForSpec largely overlaps with FORMULA (http://formula.codeplex.com) language developed at Microsoft Research.

ForSpec has a built-in high-performance forward inference engine for executing the specifications.
Furthermore, ForSpec is integrated with the FORMULA API and can generate equivalent FORMULA specifications from a ForSpec specification.
This has the advantage that the static type checking, concrete and symbolic execution engines of FORMULA can be used for ForSpec specifications as well.
Finally, ForSpec ships with a DocLatex tool that can generate Latex documentation from the specifications.

Licensing
=========

ForSpec is licensed under the Microsoft Public License (Ms-PL) just as FORMULA (http://formula.codeplex.com/license). 