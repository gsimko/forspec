﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace DocLatex
{
    class Program
    {
        static string inputName;
        static TextWriter output;
        static void WriteLineToOutput(string n)
        {
            if (output == null)
                output = File.CreateText(inputName + ".gentex");
            output.WriteLine(n);
        }
        static void WriteToOutput(string n)
        {
            if (output == null)
                output = File.CreateText(inputName + ".gentex");
            output.Write(n);
        }

        static StringBuilder sourceCode = new StringBuilder();
        static void BufferSourceCode(string code)
        {
            sourceCode.AppendLine(code);
        }
        static void WriteSourceCode()
        {
            if (sourceCode.Length == 0) return;
            // Calculate the number of whitespaces in each row
            var code = sourceCode.ToString();
            bool doCount = true;
            int minSpaces = int.MaxValue;
            int curSpaces = 0;
            foreach (var c in code)
            {
                if (c == '\n')
                {
                    doCount = true;
                    curSpaces = 0;
                }
                else if (doCount)
                {
                    if (Char.IsWhiteSpace(c))
                        curSpaces++;
                    else
                    {
                        minSpaces = Math.Min(curSpaces, minSpaces);
                        doCount = false;
                    }
                }
            }
            // Trim minSpaces character from each line
            StringBuilder trimmed = new StringBuilder();
            int curTrims = 0;
            foreach (var c in code)
            {
                if (c == '\n')
                {
                    trimmed.Append('\n');
                    curTrims = 0;
                }
                else
                {
                    if (curTrims++ >= minSpaces)
                        trimmed.Append(c);                    
                }
            }
            WriteLineToOutput(@"\begin{lstlisting}");
            WriteToOutput(trimmed.ToString());
            WriteLineToOutput(@"\end{lstlisting}");
            sourceCode = new StringBuilder();
        }

        static void Main(string[] args)
        {
            if (args.Length<1)
            { 
                return;
            }
            if (args.Length == 2)
            {
                if (!args[0].Equals("-s"))
                    return;
                Strip(args[1]);
                return;
            }
            inputName = args[0];
            using (var file = File.OpenText(inputName))
            {                
                string line;
                bool skip = false; // Indicates to skip the following lines
                bool inMathMode = false; // Indicates if we are in math mode
                bool finishedWithColon = false;
                while ((line = file.ReadLine()) != null)
                {
                    // Handle empty lines
                    if (line.Equals(""))
                        continue;
                    // Handle skip comments
                    if (line.Contains(@"</skip>"))
                    {
                        skip = false;
                        continue;
                    }
                    if (line.Contains(@"<skip>"))
                        skip = true;
                    if (skip)
                        continue;
                    // Handle @output ... @
                    if (line.Contains(@"@output"))
                    {
                        var match = Regex.Match(line, "@output ([^@]*)@");
                        WriteSourceCode();
                        if (output != null)
                        {                            
                            output.Close();
                        }
                        output = File.CreateText(inputName+"."+match.Groups[1].Value+".gentex");
                        continue;
                    }
                    
                    // Handle everything else
                    var trim = line.Trim();

                    bool slash3 = trim.StartsWith(@"///");
                    bool slash2 = slash3 ? true : trim.StartsWith(@"//");

                    if (slash3 || (slash2 && line.EndsWith(":")))
                    {
                        // Add end lstlisting, if necessary                        
                        WriteSourceCode();

                        // Remove slashes from comment
                        var comment = slash3 ? trim.Substring(3) : trim.Substring(2);
                                                
                        // Replace _ with \_ if not in math mode
                        StringBuilder commentModified = new StringBuilder();
                        for (int i=0; i<comment.Length; ++i)
                        {
                            var c = comment[i];
                            if (c == '$')
                                inMathMode = !inMathMode;
                            else if (c == '\\')
                            {
                                var subs = comment.Substring(i);
                                if (subs.StartsWith(@"\begin{align") ||
                                    subs.StartsWith(@"\begin{equation"))
                                    inMathMode = true;
                                else if (subs.StartsWith(@"\end{align") ||
                                         subs.StartsWith(@"\end{equation"))
                                    inMathMode = false;
                            }
                            if (!inMathMode && c == '_')
                                commentModified.Append(@"\_");
                            else
                                commentModified.Append(c);
                        }
                        comment = commentModified.ToString().Trim();
                            
                        // Replace section formatting
                        var h3 = new Regex(@"<h3>(.*)</h3>");
                        comment = h3.Replace(comment, m => @"\hthree{" + m.Groups[1].Value + @"}");

                        //var title = new Regex(@"@title(.*)@");
                        //comment = title.Replace(comment, m => @"\section{" + m.Groups[1].Value + @"}");
                        if (comment.Contains(@"@title"))
                            continue;

                        var section = new Regex(@"@section (.*)@");
                        comment = section.Replace(comment, m => @"\section{" + m.Groups[1].Value + "}\n");

                        var subsection = new Regex(@"@subsection (.*)@");
                        comment = subsection.Replace(comment, m => @"\subsection{" + m.Groups[1].Value + "}\n");

                        var subsubsection = new Regex(@"@subsubsection (.*)@");
                        comment = subsubsection.Replace(comment, m => @"\subsubsection{" + m.Groups[1].Value + "}\n");

                        var paragraph = new Regex(@"@paragraph (.*)@");
                        comment = paragraph.Replace(comment, m => @"\paragraph{" + m.Groups[1].Value + "}\n");

                        WriteLineToOutput(comment);

                        if (slash3)
                            finishedWithColon = comment.EndsWith(":");                                
                    }
                    else
                    {
                        //if (!finishedWithColon)
                            //  continue;                        
                        var semdef = new Regex(@"([A-Z]+)([\w\s]*:[\s\w]+?->)");
                        line = semdef.Replace(line,
                            m =>
                                //(m.Groups[1].Value.Length <= 2 ? @"$\mathcal{" : @"$\textit{") +
                                @"$\mathcal{" +
                                m.Groups[1].Value +
                                @"}$" + m.Groups[2].Value);
                                
                        var semfun = new Regex(@"([A-Z]+)(\w*)\s*\[\[(.+?)\]\]");
                        line = semfun.Replace(line, 
                            m =>                                
                                //(m.Groups[1].Value.Length <= 2 ? @"$\mathcal{": @"$\textit{") +
                                @"$\mathcal{" +
                                m.Groups[1].Value + 
                                @"}" + 
                                m.Groups[2].Value +
                                @"\, \color{ForSpecDenotation}\llbracket\text{" + 
                                m.Groups[3].Value + 
                                @"}\rrbracket$");

                        line = line.Replace(@"=>", @"$\Rightarrow$");
                        line = line.Replace(@"->", @"$\rightarrow$");

                        if (!trim.Equals(""))
                            BufferSourceCode(line);
                    }
                }
                WriteSourceCode();
            }
            output.Close();
        }

        public static void Strip(String filename)
        {
            using (var file = File.OpenText(filename))
            {
                using (var outf = File.CreateText(filename + "_stripped.4ml"))
                {
                    string line;
                    bool commented = false;
                    while ((line = file.ReadLine()) != null)
                    {
                        bool linecomment = false;
                        {
                            var pos = line.IndexOf("//");
                            if (pos != -1)
                            {
                                var towrite = line.Substring(0, pos).Trim();
                                if (!string.IsNullOrEmpty(towrite))
                                    outf.WriteLine(towrite);
                                linecomment = true;
                            }
                        }

                        bool stripping = true;
                        while (stripping)
                        {
                            stripping = false;
                            if (commented)
                            {
                                var pos = line.IndexOf("*/");
                                if (pos != -1)
                                {
                                    //if (pos + 2 < line.Length)
                                        line = line.Substring(pos + 2);
                                    commented = false;
                                    stripping = true;
                                }
                            }
                            {
                                var pos = line.IndexOf("/*");
                                if (pos != -1)
                                {
                                    if (pos > 0 && !linecomment)
                                    {
                                        var towrite = line.Substring(0, pos).Trim();
                                        if (!string.IsNullOrEmpty(towrite))
                                            outf.WriteLine(towrite);
                                    }
                                    //if (pos + 2 < line.Length)
                                        line = line.Substring(pos + 2);
                                    commented = true;
                                    stripping = true;
                                }
                            }
                        }
                        if (!commented && !linecomment)
                            outf.WriteLine(line);
                    }
                }
            }
        }
    }
}
