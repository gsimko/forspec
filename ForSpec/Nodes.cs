﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;

namespace FormulaBe.Nodes
{
    public abstract class Node : ICloneable
    {
        public Node() { }
        public Node(string name) { Name = name; }
        public Span Span = Span.Unknown;
        public virtual IEnumerable<Node> Children 
        { 
            get { throw new NotImplementedException(); }
        }
        public virtual string Name
        {
            get;
            set;
        }
        public void SetDirty() { dirtyHashcode = true; dirtyToString = true; }
        protected bool dirtyHashcode = true;
        protected int storedHashcode = 0;
        protected bool dirtyToString = true;
        protected string storedToString = null;
        public virtual Node Simplify(TypeChecking typeChecker)
        {
            return this;
        }
        public override string ToString()
        {
            return String.Format("{0}", Name);
        }
        public override bool Equals(object obj)
        {
            throw new NotImplementedException();
        }
        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
        public virtual object Clone() { throw new NotImplementedException(); }
        public virtual Node Substitute(Node match, Node substitution) { throw new NotImplementedException(); }
        public void ReplaceNames(Dictionary<string,string> map)
        {
            SetDirty();
            // don't replace names of fields
            if (this is Field)
            {
            }
            else if (this is Var)
            {
                string varname = ((Var)this).AccessorFreeVar.Name;
                if (!String.IsNullOrEmpty(Name) && Name != "_" && map.ContainsKey(varname))
                {
                    var ipos = Name.IndexOf('.');
                    if (ipos != -1)
                        Name = map[varname] + Name.Substring(ipos);
                    else
                        Name = map[varname];
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Name) && Name != "_" && map.ContainsKey(Name))
                    Name = map[Name];
            }
            foreach (var child in Children)
                child.ReplaceNames(map);
        }
        public void ReplaceNamesExceptVars(Dictionary<string, string> map)
        {
            SetDirty();
            // don't replace names of fields or vars
            if (Name != null && !(this is Field || this is Var) && map.ContainsKey(Name))
                Name = map[Name];
            foreach (var child in Children)
                child.ReplaceNamesExceptVars(map);
        }
        public void RemoveNamespace(string namespaceToRemove)
        {
            SetDirty();
            // don't replace names of fields and variables
            if (Name != null && Name.StartsWith(namespaceToRemove + "."))
            {
                Name = Name.Substring(namespaceToRemove.Length + 1);
            }
            foreach (var child in Children)
                child.RemoveNamespace(namespaceToRemove);
        }
        void ReplaceNamespaces(Node node, string force_namespace, TypeChecking typeChecker)
        {
            node.SetDirty();
            UserFuncTerm func = node as UserFuncTerm;
            if (func != null && func.Namespace != null)
            {
                // Don't force new namespace, if a subnamespace is already used
                if (!func.Namespace.StartsWith(force_namespace))
                {
                    func.Name = force_namespace + func.PureName;
                    func.Function.context = force_namespace;
                    func.Function.decl = typeChecker.typeDecls[func.Name];
                }
            }
            foreach (var child in node.Children)
                ReplaceNamespaces(child, force_namespace, typeChecker);
        }
        Node GetReplaceVar(Var v, Dictionary<Var, Node> map, string force_namespace, TypeChecking typeChecker)
        {
            if (map.ContainsKey(v.AccessorFreeVar))
            {
                Node newnode = (Node)map[v.AccessorFreeVar].Clone();
                var replacement = v.Substitute(v.AccessorFreeVar, newnode);
                if (!String.IsNullOrEmpty(force_namespace))
                    ReplaceNamespaces(replacement, force_namespace, typeChecker);
                return replacement;
            }
            return v;
        }
        /// <summary>
        /// Replace variables according to a mapping.
        /// The namespace of the replacements are renamed according to their container terms.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="force_namespace"></param>
        /// <param name="typeChecker"></param>
        public void ReplaceVarsInPlace(Dictionary<Var, Node> map, string force_namespace, TypeChecking typeChecker)
        {
            SetDirty();
            if (this is UserFuncTerm)
            {
                var func = (UserFuncTerm)this;
                var decl = func.Function.decl as ConDecl;
                for (int i = 0; i < func.args.Count; ++i)
                {
                    var fieldName = decl.Fields[i].Type.Name;
                    int ipos = fieldName.IndexOf('.');
                    if (ipos != -1)
                        force_namespace = fieldName.Substring(0,ipos+1);
                    else
                        force_namespace = null;
                    if (func.args[i] is Var)
                        func.args[i] = GetReplaceVar((Var)func.args[i], map, force_namespace, typeChecker);
                    else 
                        func.args[i].ReplaceVarsInPlace(map, force_namespace, typeChecker);
                }
            }
            else 
            {
                if (this is BuiltinFuncTerm)
                {
                    var func = (FuncTerm)this;
                    for (int i = 0; i < func.args.Count; ++i)
                        if (func.args[i] is Var)
                            func.args[i] = GetReplaceVar((Var)func.args[i], map, force_namespace, typeChecker);                
                }
                else if (this is RelConstr)
                {
                    var rel = (RelConstr)this;
                    if (rel.Arg1 is Var)
                        rel.Arg1 = GetReplaceVar((Var)rel.Arg1, map, force_namespace, typeChecker);
                    if (rel.Arg2 is Var)
                        rel.Arg2 = GetReplaceVar((Var)rel.Arg2, map, force_namespace, typeChecker);
                }
                foreach (var child in Children)
                    child.ReplaceVarsInPlace(map, force_namespace, typeChecker);
            }
        }        
        
        public static bool EvaluateBuiltinEq(Node arg1, Node arg2, TypeChecking typeChecker)
        {
            if ((arg1 is Var && (arg1.Name == "_")) ||
                (arg2 is Var && (arg2.Name == "_")))
                return true;
            var uf1 = arg1 as UserFuncTerm;
            var uf2 = arg2 as UserFuncTerm;
            if (uf1 != null && uf2 != null)
            {
                if (uf1.Name != uf2.Name || uf1.args.Count != uf2.args.Count)
                    return false;
                for (int i = 0; i < uf1.args.Count; ++i)
                    if (!EvaluateBuiltinEq(uf1.args[i], uf2.args[i], typeChecker))
                        return false;
                return true;
            }
            var bf1 = arg1.Simplify(typeChecker);
            var bf2 = arg2.Simplify(typeChecker);            
            return bf1.Equals(bf2);
        }
        public static bool EvaluateBuiltinRelation(RelConstr term, TypeChecking typeChecker)
        {
            switch (term.Op)
            {
                case RelKind.Eq:
                    return EvaluateBuiltinEq(term.Arg1, term.Arg2, typeChecker);
                case RelKind.Neq:
                    return !EvaluateBuiltinEq(term.Arg1, term.Arg2, typeChecker);
            }
            var e1 = term.Arg1.Simplify(typeChecker) as RationalCnst;
            var e2 = term.Arg2.Simplify(typeChecker) as RationalCnst;
            
            if (e1 != null && e2 != null)
                switch (term.Op)
                {
                    case RelKind.Ge: return e1.Cnst >= e2.Cnst;
                    case RelKind.Gt: return e1.Cnst > e2.Cnst;
                    case RelKind.Le: return e1.Cnst <= e2.Cnst;
                    case RelKind.Lt: return e1.Cnst < e2.Cnst;            
                }
            throw new Exception(String.Format("Unsupported relation {0}", term));
        }
        public List<T> GetNodesOfType<T>() where T : class
        {
            List<T> outNodes = new List<T>();
            GetNodesOfType<T>(outNodes);
            return outNodes;
        }
        private void GetNodesOfType<T>(List<T> outNodes) where T : class
        {
            var a = this as T;
            if (a != null)
                outNodes.Add(a);
            foreach (var child in Children)
                child.GetNodesOfType<T>(outNodes);
        }
        public HashSet<Var> GetVariables()
        {
            List<Var> ids = new List<Var>();
            GetNodesOfType<Var>(ids);
            var vars = ids.Select(x => { int ipos = x.Name.IndexOf('.'); return (ipos == -1) ? x : new Var(x.Name.Substring(0, ipos)); });
            HashSet<Var> result = new HashSet<Var>(vars);
            result.Remove(new Var("_"));
            return result;
        }
        private void GetPositiveVariables(List<Var> outVar)
        {
            var v = this as Var;
            if (v != null)
                outVar.Add(v);
            var rc = this as RelConstr;
            if (rc != null && rc.Op == RelKind.No) return;
            var bf = this as BuiltinFuncTerm;
            if (bf != null && (bf.Function == OpKind.ToList || bf.Function == OpKind.Count || bf.Function == OpKind.Max || bf.Function == OpKind.Min)) return;
            foreach (var child in Children)
                child.GetPositiveVariables(outVar);
        }
        public HashSet<Var> GetPositiveVariables()
        {
            List<Var> ids = new List<Var>();
            GetPositiveVariables(ids);
            var vars = ids.Select(x => { int ipos = x.Name.IndexOf('.'); return (ipos == -1) ? x : new Var(x.Name.Substring(0, ipos)); });
            HashSet<Var> result = new HashSet<Var>(vars);
            result.Remove(new Var("_"));
            return result;
        }
        public HashSet<Var> GetVariablesWithAccessor()
        {
            List<Var> ids = new List<Var>();
            GetNodesOfType<Var>(ids);
            HashSet<Var> result = new HashSet<Var>(ids);
            result.Remove(new Var("_"));
            return result;
        }
        public List<Var> GetListOfVariables()
        {
            List<Var> ids = new List<Var>();
            GetNodesOfType<Var>(ids);
            List<Var> result = new List<Var>();
            HashSet<Var> contains = new HashSet<Var>();
            foreach (var v in ids)
                if (v.Name != "_" && !contains.Contains(v))
                {
                    result.Add(v);
                    contains.Add(v);
                }
            return result;
        }
        private void GetDefinedVars(List<Var> defVars)
        {
            if (this is RelConstr && ((RelConstr)this).Op == RelKind.No)
                return;
            if (this is BuiltinFuncTerm && ((BuiltinFuncTerm)this).Function == OpKind.ToList)
                return;
            if (this is Var && this.Name != "_")
            {
                defVars.Add((Var)this);
                /*int ipos = Name.IndexOf('.');
                if (ipos == -1)
                    defVars.Add((Var)this);
                else*/
                    //defVars.Add(new Var(Name.Substring(0, ipos)));
            }
            else
                foreach (var child in Children)
                    child.GetDefinedVars(defVars);
        }
        public List<Var> GetListOfPositiveVariables()
        {
            List<Var> defVars = new List<Var>();
            GetDefinedVars(defVars);
            return defVars;
        }
        public bool isGround()
        {
            return !(this is Var) && Children.All(x => x.isGround());
        }
        public bool Contains(Node node)
        {
            return this.Equals(node) || Children.Any(x => x.Contains(node));
        }
        public void ExpandQualifiedName()
        {
            /*if (this is Id)
            {
                var id = (Id)this;
                if (!String.IsNullOrEmpty(id.context))
                {
                    var ipos = id.Name.LastIndexOf('.');
                    if (ipos != -1)
                        id.Name = id.context + "." + id.Name.Substring(ipos + 1);
                    else
                        id.Name = id.context + "." + id.Name;
                }
            }*/
            SetDirty();
            if (this is UserFuncTerm)
            {
                var func = (UserFuncTerm)this;
                if (!String.IsNullOrEmpty(func.Function.context))
                {
                    var ipos = func.Name.LastIndexOf('.');
                    if (ipos != -1)
                        func.Name = func.Function.context + "." + func.Name.Substring(ipos+1);
                    else
                        func.Name = func.Function.context + "." + func.Name;
                }
            }
            foreach (var child in Children)
                child.ExpandQualifiedName();
        }
        public void AppendNamespace(string ns)
        {
            SetDirty();
            if (this is Id)
            {
                var id = (Id)this;
                id.Name = ns + id.Name;
            }
            else if (this is UserFuncTerm)
            {
                var func = (UserFuncTerm)this;
                func.Function.AppendNamespace(ns);
            }
            foreach (var child in Children)
                child.AppendNamespace(ns);
        }
    }
    public class NodeList<T> : Node, IEnumerable<T> where T : Node
    {
        public NodeList(T head, NodeList<T> rest) { this.head = head; this.rest = rest; }
        public T head;
        public NodeList<T> rest;

        public IEnumerator<T> GetEnumerator()
        {
            NodeList<T> current = this;
            while (current != null)
            {
                yield return current.head;
                current = current.rest;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    
    /*public class CardPair : Node
    {
        public CardPair(Span Span, Id typeId, int cardinality) : base(Span) { TypeId = typeId; Cardinality = cardinality; }
        public Id TypeId;
        public int Cardinality;
        public List<Node> constraints = new List<Node>();        
    }*/
    
    public class Compr : Node, IEquatable<Compr>
    {
        public Compr() { }
        public Compr(IEnumerable<Node> heads, IEnumerable<Body> bodies)
        {
            if (heads != null)
                foreach (var h in heads)
                    this.Heads.Add(h);
            if (bodies != null)
                foreach (var b in bodies)
                    this.Bodies.Add(b);
        }
        public void AddHead(Node head, bool Add = true) { if (Add) Heads.Add(head); else Heads.Insert(0,head); }
        public void AddBody(Body body, bool Add = true) { if (Add) Bodies.Add(body); else Bodies.Insert(0,body); }
        public List<Node> Heads = new List<Node>();
        public List<Body> Bodies = new List<Body>();
        public override IEnumerable<Node> Children
        {
            get
            {
                foreach (var n in Heads) yield return n;
                foreach (var n in Bodies) yield return n;
            }
        }
        public override string ToString()
        {
            return String.Format("{{{0} | {1}}}",
                String.Join(",", Heads.Select(x => x.ToString())),
                String.Join(",", Bodies.Select(x => x.ToString())));
        }
        public bool Equals(Compr obj)
        {
            if (Heads.Count != obj.Heads.Count || Bodies.Count != obj.Bodies.Count)
                return false;
            for (int i=0; i<Heads.Count; ++i)
                if (!Heads[i].Equals(obj.Heads[i]))
                    return false;
            for (int i=0; i<Bodies.Count; ++i)
                if (!Bodies[i].Equals(obj.Bodies[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is Compr ? Equals((Compr)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            foreach (var h in Heads)
                hash = hash * 37 + h.GetHashCode();
            foreach (var b in Bodies)
                hash = hash * 37 + b.GetHashCode();
            return hash;
        }
        public override object Clone()
        {
            var res = new Compr();
            foreach (var h in Heads)
                res.Heads.Add((Node)h.Clone());
            foreach (var b in Bodies)
                res.Bodies.Add((Body)b.Clone());
            return res;
        }
        public override Node Substitute(Node match, Node substitution)
        {
            return Equals(match) ? substitution : this;
        }

    }    
    public class ContractItem : Node
    {
        public ContractItem(ContractKind kind) { ContractKind = kind; }
        public List<Body> specification = new List<Body>();
        public ContractKind ContractKind;
        public override IEnumerable<Node> Children
        {
            get { return specification; }
        }
        public override string ToString()
        {
            return String.Join(",", specification.Select(x => x.ToString()));
        }
    }
    public abstract class Module : Node
    {
        public void AddRule(RuleNode rule) { Rules.Add(rule); }
        public void AddTypeDecl(TypeDecl typeDecl) { TypeDecls.Add(typeDecl); }
        public List<RuleNode> Rules = new List<RuleNode>();
        public List<TypeDecl> TypeDecls = new List<TypeDecl>();
        public string ParsedFrom;
        public override IEnumerable<Node> Children
        {
            get
            {
                foreach (var n in Rules) yield return n;
                foreach (var n in TypeDecls) yield return n;
            }
        }
        public abstract IEnumerable<ModRef> ModRefs { get; }
    }
    public class Domain : Module
    {
        public Domain(string name) { Name = name; }
        public Domain(string name, ComposeKind kind, IEnumerable<ModRef> compositions)
        {
            Name = name;
            ComposeKind = kind;
            if (compositions != null)
            {
                if (ComposeKind == FormulaBe.ComposeKind.Imports)
                    foreach (var comp in compositions)
                        this.Imports.Add(comp);
                else
                    foreach (var comp in compositions)
                        this.Compositions.Add(comp);
            }
        }
        public virtual bool IsModel
        {
            get { return false; }
        }
        public void AddComposition(ModRef composition) { Compositions.Add(composition); }
        public void AddImport(ModRef import) { Imports.Add(import); }
        public ComposeKind ComposeKind;
        public List<ModRef> Compositions = new List<ModRef>();
        public List<ModRef> Imports = new List<ModRef>();
        public List<ContractItem> Conforms = new List<ContractItem>();
        public override IEnumerable<Node> Children
        {
            get
            {
                foreach (var n in Compositions) yield return n;
                foreach (var n in Imports) yield return n;
                foreach (var n in Rules) yield return n;
                foreach (var n in TypeDecls) yield return n;
                foreach (var n in Conforms) yield return n;
            }
        }
        public override IEnumerable<ModRef> ModRefs
        {
            get
            {
                foreach (var n in Compositions) yield return n;
                foreach (var n in Imports) yield return n;
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();            
            sb.Append("domain ").Append(Name);
            if (Compositions.Count > 0 || Imports.Count > 0)
                switch (ComposeKind)
                {
                    case ComposeKind.Extends: sb.Append(" extends "); break;
                    case ComposeKind.Includes: sb.Append(" includes "); break;
                    case ComposeKind.Imports: sb.Append(" imports "); break;
                }
            sb.Append(String.Join(", ", Compositions.Select(x => x.ToString())));
            sb.Append(String.Join(", ", Imports.Select(x => x.ToString()))).Append('\n');
            
            sb.Append("{\n");
            sb.Append(String.Join("\n", TypeDecls.Select(x => "  " + x.ToString().Replace("\n", "\n  ")))).Append('\n');
            sb.Append(String.Join("\n", Rules.Select(x => "  " + x.ToString().Replace("\n", "\n  ")))).Append('\n');
            sb.Append(String.Join("\n", Conforms.Select(x => "  " + x.ToString().Replace("\n", "\n  ")))).Append('\n');
            sb.Append("}\n");
            return sb.ToString();
        }
    }
    public class Transform : Module
    {
        public Transform(string name) { Name = name; }
        public void AddInput(Param input) { Inputs.Add(input);  }
        public void AddOutput(Param input) { Outputs.Add(input); }
        public List<Param> Inputs = new List<Param>();
        public List<Param> Outputs = new List<Param>();
        public List<ContractItem> Contracts = new List<ContractItem>();
        public override IEnumerable<Node> Children
        {
            get
            {
                foreach (var n in Rules) yield return n;
                foreach (var n in Inputs) yield return n;
                foreach (var n in Outputs) yield return n;
                foreach (var n in Contracts) yield return n;                
                foreach (var n in TypeDecls) yield return n;
            }
        }
        public override IEnumerable<ModRef> ModRefs
        {
            get
            {
                foreach (var n in Inputs) if (n.Type is ModRef) yield return (ModRef)n.Type;
                foreach (var n in Outputs) if (n.Type is ModRef) yield return (ModRef)n.Type;
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("transform ").Append(Name).Append(" (");
            sb.Append(String.Join(", ", Inputs.Select(x => x.ToString())));
            sb.Append(") returns ");
            sb.Append(String.Join(", ", Outputs.Select(x => x.ToString()))).Append('\n');
            sb.Append("{\n");
            sb.Append(String.Join("\n", TypeDecls.Select(x => "  " + x.ToString().Replace("\n", "\n  ")))).Append('\n');
            sb.Append(String.Join("\n", Rules.Select(x => "  " + x.ToString().Replace("\n", "\n  ")))).Append('\n');
            sb.Append("}\n");
            return sb.ToString();
        }
    }
    
    
    public class Id : Node, IEquatable<Id>
    {
        public Id(string name) : base(name) { }
        public TypeDecl decl;
        public string context;
        public override IEnumerable<Node> Children
        {
            get { yield break; }
        }
        public override string ToString()
        {
            return Log.g_DetailedNodeInformation ? String.Format("{0}#id",Name) : Name;
        }
        public bool Equals(Id obj) { return Name.Equals(obj.Name); }
        public override bool Equals(object obj) { return obj is Id ? Equals((Id)obj) : false; }
        public override int GetHashCode() { return Name.GetHashCode(); }
        public override object Clone()
        {
            var res = new Id(Name) { Span = Span };
            res.decl = decl;
            res.context = context;
            return res;
        }
        public override Node Substitute(Node match, Node substitution)
        {
            return Equals(match) ? substitution : this;
        }
    }

    public class Model : Domain
    {
        public Model(string name, bool isPartial) : base(name) { IsPartial = isPartial; }
        public void AddIncludes(ModRef include) { includes.Add(include); }
        public void SetDomain(ModRef domain) { Contract.Requires(domain != null); Domain = domain; }
        public bool IsPartial;
        public ModRef Domain;
        public List<ModRef> includes = new List<ModRef>();
        public List<ContractItem> contracts = new List<ContractItem>();
        public List<ModelFact> facts = new List<ModelFact>();
        public override bool IsModel
        {
            get { return true; }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("model ").Append(Name).Append(" of ").Append(Domain.ToString()).Append('\n');
            sb.Append("{\n");
            sb.Append(String.Join("\n", facts.Select(x => "  " + x.ToString().Replace("\n", "\n  "))));
            if (facts.Count>0) sb.Append('\n');
            sb.Append("}\r\n");
            return sb.ToString();
        }
        public override IEnumerable<Node> Children
        {
            get
            {
                yield return Domain;
                foreach (var n in includes) yield return n;
                foreach (var n in contracts) yield return n;
                foreach (var n in Rules) yield return n;
                foreach (var n in TypeDecls) yield return n;
                foreach (var n in facts) yield return n;
            }
        }
        public override IEnumerable<ModRef> ModRefs
        {
            get
            {
                yield return Domain;
                foreach (var n in includes) yield return n;
            }
        }
    }
    public class ModelFact : Node
    {
        public ModelFact(Id binding, UserFuncTerm match) { Binding = binding; Match = match; }
        public Id Binding;
        public UserFuncTerm Match;
        public override IEnumerable<Node> Children
        {
            get
            {
                if (Binding != null) yield return Binding;
                yield return Match;
            }
        }
        public override string ToString()
        {
            if (Binding != null)
                return String.Format("{0} is {1}.", Binding, Match);
            return Match.ToString();
        }
        public override object Clone()
        {
            if (Binding != null)
                return new ModelFact((Id)Binding.Clone(), (UserFuncTerm)Match.Clone());
            return new ModelFact(null, (UserFuncTerm)Match.Clone());
        }
    }
    public class ModRef : Node
    {
        public ModRef(string name, string rename, string location) : base(name) 
        { 
            Rename = rename; 
            Location = location; 
        }
        public string Rename;
        public string Location;
        public override IEnumerable<Node> Children
        {
            get { yield break;  }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(Rename))
                sb.Append(Rename).Append(":");
            sb.Append(Name);
            if (!String.IsNullOrEmpty(Location))
                sb.Append(" at ").Append('"').Append(Location).Append('"');
            return sb.ToString();
        }
    }
    public class Param : Node
    {
        public Param(string name, Node type) : base(name) { Type = type; }
        public Node Type;
        public override IEnumerable<Node> Children
        {
            get { yield return Type; }
        }
        public override string ToString()
        {
            if (Type is ModRef)
                return Type.ToString();
            else
                return String.Format("{0}:{1}", Name, Type.ToString());
        }
    }
    public class Program : Node
    {
        public Program(ProgramName name) : base(name.Uri.OriginalString) { Name = name; }
        public new ProgramName Name;
        public List<Module> modules = new List<Module>();
        public override IEnumerable<Node> Children
        {
            get { 
                foreach (var m in modules) yield return m;
            }
        }
    }
    /*public class Range : Node
    {
        public Range(Rational lower, Rational upper) { Lower = lower; Upper = upper; }
        public Rational Lower;
        public Rational Upper;
        public override IEnumerable<Node> Children
        {
            get { yield break; }
        }
    }*/
    
    
    /*public class Setting : Node
    {
        public Setting(Id key, Cnst value) { Key = key; Value = value; }
        public Id Key;
        public Cnst Value;
        public override IEnumerable<Node> Children
        {
            get
            {
                yield return Key;
                yield return Value;
            }
        }
    }*/
}
