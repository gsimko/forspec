﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe.Nodes
{
    public abstract class Cnst : Node
    {        
    }
    public class StringCnst : Cnst, IEquatable<StringCnst>
    {
        public StringCnst(string cnst) { this.Cnst = cnst; this.Name = cnst; }
        public string Cnst;
        public override IEnumerable<Node> Children { get { yield break; } }
        public override string ToString() { return Cnst; }
        public bool Equals(StringCnst obj) { return Cnst.Equals(obj.Cnst); }
        public override bool Equals(Object obj) { if (obj is StringCnst) return Equals((StringCnst)obj); else return false; }
        public override int GetHashCode() { return Cnst.GetHashCode(); }
        public override object Clone() { return new StringCnst(Cnst) { Span = Span }; }
        public override Node Substitute(Node match, Node substitution)
        {
            return this.Equals(match) ? substitution : (Node)Clone();
        }
    }
    public class RationalCnst : Cnst, IEquatable<RationalCnst>
    {
        public RationalCnst(Rational cnst)
        {
            this.Cnst = cnst;
        }
        public Rational Cnst;
        public override IEnumerable<Node> Children { get { yield break; } }
        public override string ToString() { return Cnst.ToString(); }
        public bool Equals(RationalCnst obj) { return Cnst.Equals(obj.Cnst); }
        public override bool Equals(Object obj) { if (obj is RationalCnst) return Equals((RationalCnst)obj); else return false; }
        public override int GetHashCode() { return Cnst.GetHashCode(); }
        public override object Clone() { return new RationalCnst(Cnst) { Span = Span }; }
        public override Node Substitute(Node match, Node substitution)
        {
            return this.Equals(match) ? substitution : (Node)Clone();
        }
    }
    public class BoolCnst : Cnst, IEquatable<BoolCnst>
    {
        public BoolCnst(bool cnst) { this.Cnst = cnst; }
        public bool Cnst;
        public override IEnumerable<Node> Children { get { yield break; } }
        public override string ToString() { return Cnst ? "TRUE" : "FALSE"; }
        public bool Equals(BoolCnst obj) { return Cnst.Equals(obj.Cnst); }
        public override bool Equals(Object obj) { if (obj is BoolCnst) return Equals((BoolCnst)obj); else return false; }
        public override int GetHashCode() { return Cnst.GetHashCode(); }
        public override object Clone() { return new BoolCnst(Cnst) { Span = Span }; }
        public override Node Substitute(Node match, Node substitution)
        {
            return this.Equals(match) ? substitution : (Node)Clone();
        }
    }
    public class EnumCnst : Cnst, IEquatable<EnumCnst>
    {
        public EnumCnst(string cnst) { this.Cnst = cnst; this.Name = cnst; }
        public string Cnst;
        public override IEnumerable<Node> Children { get { yield break; } }
        public override string ToString() 
        { 
            return Log.g_DetailedNodeInformation ? String.Format("{0}#enum",Cnst) : Cnst; 
        }
        public bool Equals(EnumCnst obj) { return Cnst.Equals(obj.Cnst); }
        public override bool Equals(Object obj) { if (obj is EnumCnst) return Equals((EnumCnst)obj); else return false; }
        public override int GetHashCode() { return Cnst.GetHashCode(); }
        public override object Clone() { return new EnumCnst(Cnst) { Span = Span }; }
        public override Node Substitute(Node match, Node substitution)
        {
            return this.Equals(match) ? substitution : (Node)Clone();
        }
    }
    // Type declarations
    // ConDecl
    // MapDecl
    // EnumDecl
    // UnionDecl
    public abstract class TypeDecl : Node
    {
    }
    public class ConstantTypeDecl : TypeDecl, IEquatable<ConstantTypeDecl>
    {
        public ConstantTypeDecl(string name) { Name = name; }
        public override string ToString()
        {
            return Name;
        }
        public override object Clone() { return this; }
        public bool Equals(ConstantTypeDecl obj)
        {
            return Name == obj.Name;
        }
        public override bool Equals(object obj)
        {
            return obj is ConstantTypeDecl ? Equals((ConstantTypeDecl)obj) : false;
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
    public class BuiltinTypeDecl : TypeDecl, IEquatable<BuiltinTypeDecl>
    {
        private BuiltinTypeDecl(BuiltinType type) { this.type = type; }
        public static BuiltinTypeDecl StringType = new BuiltinTypeDecl(BuiltinType.String);
        public static BuiltinTypeDecl Booleantype = new BuiltinTypeDecl(BuiltinType.Boolean);
        public static BuiltinTypeDecl IntegerType = new BuiltinTypeDecl(BuiltinType.Integer);
        public static BuiltinTypeDecl RealType = new BuiltinTypeDecl(BuiltinType.Real);
        public BuiltinType type;
        public override string ToString()
        {
            return type.ToString();
        }
        public override object Clone() { return this; }
        public bool Equals(BuiltinTypeDecl obj)
        {
            return type == obj.type;
        }
        public override bool Equals(object obj)
        {
            return obj is BuiltinTypeDecl ? Equals((BuiltinTypeDecl)obj) : false;
        }
        public override int GetHashCode()
        {
            return unchecked((int)type);
        }
    }
    public class Field : Node, IEquatable<Field>
    {
        public Field(string name, Id type, bool isAny) { Name = name; Type = type; IsAny = isAny; }
        public Id Type;
        public bool IsAny;
        public override IEnumerable<Node> Children { get { yield return Type; } }
        public override string ToString()
        {
            return String.IsNullOrEmpty(Name) ? Type.ToString() : String.Format("{0}:{1}", Name, Type.ToString());
        }
        public override object Clone() { return new Field(Name, (Id)Type.Clone(), IsAny) { Span = Span }; }
        public bool Equals(Field obj) { return Type.Equals(obj.Type) && IsAny == obj.IsAny; }
        public override bool Equals(object obj)
        {
            return obj is Field ? Equals((Field)obj) : false;
        }
        public override int GetHashCode()
        {
            return unchecked(Type.GetHashCode() + (IsAny ? 37 : 0));
        }
    }
    public class ConDecl : TypeDecl, IEquatable<ConDecl>
    {
        private ConDecl(Span span, string name) { Span = span; Name = name; }
        public static ConDecl Create(Span Span, string name, bool isNew, IEnumerable<Field> fields) 
        { 
            var res = new ConDecl(Span, name);
            res.IsNew = isNew;
            res.IsMap = false;
            if (fields != null)
                foreach (var node in fields)
                    res.Fields.Add((Field)node);
            return res;
        }
        public static ConDecl CreateMap(Span Span, string name, MapKind kind, bool isPartial, IEnumerable<Field> domain, IEnumerable<Field> codomain)
        {
            var res = new ConDecl(Span, name);
            res.IsPartial = isPartial;
            res.Kind = kind;
            res.IsMap = true;
            if (domain != null)
                foreach (var node in domain)
                {
                    res.Fields.Add(node);
                    res.CodomainFieldStart++;
                }
            if (codomain != null)
                foreach (var node in codomain)
                    res.Fields.Add(node);
            return res;
        }
        public static ConDecl CreateGoalDriven(Span Span, string name, bool isNew, IEnumerable<Field> domain, IEnumerable<Field> codomain)
        {
            var res = new ConDecl(Span, name);
            res.IsNew = isNew;
            res.IsGoalDriven = true;
            res.IsMap = false;
            if (domain != null)
                foreach (var node in domain)
                {
                    res.Fields.Add(node);
                    res.CodomainFieldStart++;
                }
            if (codomain != null)
                foreach (var node in codomain)
                    res.Fields.Add(node);
            return res;
        }
        public static ConDecl CreateDenotational(Span Span, string name, IEnumerable<Field> domain, IEnumerable<Field> codomain)
        {
            var res = new ConDecl(Span, name);
            res.IsDenotational = true;
            res.IsMap = false;
            if (domain != null)
                foreach (var node in domain)
                {
                    res.Fields.Add(node);
                    res.CodomainFieldStart++;
                }
            if (codomain != null)
                foreach (var node in codomain)
                    res.Fields.Add(node);
            return res;
        }

        public bool IsMap;
        public bool IsNew;
        public bool IsGoalDriven;
        public bool IsDenotational;
        public List<Field> Fields = new List<Field>();
        public MapKind Kind;
        public bool IsPartial;

        // index where the codomain start        
        public int CodomainFieldStart;

        public override IEnumerable<Node> Children
        {
            get { return Fields; }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(Name);

            if (IsDenotational)
            {
                sb.Append(" : ");
                sb.Append(String.Join(", ", Fields.Take(CodomainFieldStart).Select(x => x.ToString())));
                sb.Append(" -> ");
                sb.Append(String.Join(", ", Fields.Skip(CodomainFieldStart).Select(x => x.ToString())));
                sb.Append('.');
            }
            else
            {
                sb.Append(" ::= ");
                if (IsMap)
                {
                    switch (Kind)
                    {
                        case MapKind.Fun: sb.Append("fun "); break;
                        case MapKind.Bij: sb.Append("bij "); break;
                        case MapKind.Inj: sb.Append("inj "); break;
                        case MapKind.Sur: sb.Append("sur "); break;
                    }
                    sb.Append("(");
                    sb.Append(String.Join(", ", Fields.Take(CodomainFieldStart).Select(x => x.ToString())));
                    sb.Append(IsPartial ? " -> " : " => ");
                    sb.Append(String.Join(", ", Fields.Skip(CodomainFieldStart).Select(x => x.ToString())));
                    sb.Append(").");
                }
                else
                {
                    if (IsNew) sb.Append("new ");
                    if (IsGoalDriven)
                    {
                        sb.Append("[");
                        sb.Append(String.Join(", ", Fields.Take(CodomainFieldStart).Select(x => x.ToString())));
                        sb.Append(" => ");
                        sb.Append(String.Join(", ", Fields.Skip(CodomainFieldStart).Select(x => x.ToString())));
                        sb.Append("].");
                    }
                    else
                    {
                        sb.Append("(");
                        sb.Append(String.Join(", ", Fields.Select(x => x.ToString())));
                        sb.Append(").");
                    }
                }
            }
            return sb.ToString();
        }

        public override object Clone()
        {
            var res = IsMap ? 
                        CreateMap(Span, Name, Kind, IsPartial, null, null) :
                        Create(Span, Name, IsNew, null);
            res.IsPartial = IsPartial;
            res.CodomainFieldStart = CodomainFieldStart;
            res.CodomainFieldStart = CodomainFieldStart;
            foreach (var field in Fields)
                res.Fields.Add((Field)field.Clone());
            return res;
        }
        public bool Equals(ConDecl obj)
        {
            if (Name != obj.Name || IsMap != obj.IsMap || IsNew != obj.IsNew || IsPartial != obj.IsPartial || 
                IsGoalDriven != obj.IsGoalDriven || CodomainFieldStart != obj.CodomainFieldStart ||
                Kind != obj.Kind || CodomainFieldStart != obj.CodomainFieldStart)
                return false;
            if (Fields.Count != obj.Fields.Count)
                return false;
            for (int i = 0; i < Fields.Count; ++i)
                if (!Fields[i].Equals(obj.Fields[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is ConDecl ? Equals((ConDecl)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = Name.GetHashCode();
            foreach (var c in Fields)
                hash = hash * 37 + c.GetHashCode();
            return hash;
        }
    }
    public class EnumDecl : TypeDecl, IEquatable<EnumDecl>
    {
        public EnumDecl(string name, IEnumerable<EnumCnst> elements) 
        { 
            Name = name; 
            if (elements != null)
                foreach (var e in elements)
                    this.Elements.Add(e);
        }
        public void AddElement(EnumCnst arg, bool Add = true) { if (Add) Elements.Add(arg); else Elements.Insert(0, arg); }
        public List<EnumCnst> Elements = new List<EnumCnst>();
        public override IEnumerable<Node> Children
        {
            get { return Elements; }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(Name);
            sb.Append(" ::= ");
            sb.Append("{");
            sb.Append(String.Join(", ", Elements.Select(x => x.ToString())));
            sb.Append("}.");
            return sb.ToString();
        }
        public override object Clone()
        {
            var res = new EnumDecl(Name, null) { Span = Span };
            foreach (var e in Elements)
                res.Elements.Add((EnumCnst)e.Clone());
            return res;
        }
        public bool Equals(EnumDecl obj)
        {
            if (Name != obj.Name || Elements.Count != obj.Elements.Count) return false;
            for (int i = 0; i < Elements.Count; ++i)
                if (!Elements[i].Equals(obj.Elements[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is EnumDecl ? Equals((EnumDecl)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = Name.GetHashCode();
            foreach (var e in Elements)
                hash = hash * 27 + e.GetHashCode();
            return hash;
        }
    }
    public class UnnDecl : TypeDecl, IEquatable<UnnDecl>
    {
        public UnnDecl(string name, bool isExtension, IEnumerable<Id> components) 
        { 
            Name = name; 
            IsExtension = isExtension;
            if (components != null)
                foreach (var comp in components)
                    this.components.Add(comp);
        }
        public void AddComponent(Id component) { components.Add(component); }
        public List<Id> components = new List<Id>();
        public bool IsExtension;
        public override IEnumerable<Node> Children
        {
            get { return components; }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(Name);
            sb.Append(" ::= ");
            sb.Append(String.Join(" + ", components.Select(x => x.ToString())));
            sb.Append(".");
            return sb.ToString();
        }
        public override object Clone()
        {
            var res = new UnnDecl(Name, IsExtension, null) { Span = Span };
            foreach (var e in components)
                res.components.Add((Id)e.Clone());
            return res;
        }
        public bool Equals(UnnDecl obj)
        {
            if (Name != obj.Name || components.Count != obj.components.Count) return false;
            for (int i = 0; i < components.Count; ++i)
                if (!components[i].Equals(obj.components[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is UnnDecl ? Equals((UnnDecl)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = Name.GetHashCode();
            foreach (var c in components)
                hash = hash * 27 + c.GetHashCode();
            return hash;
        }
    }
}
