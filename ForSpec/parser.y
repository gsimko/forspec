%namespace FormulaBe
%using FormulaBe.Nodes
%partial
%visibility internal

%YYSTYPE Nodes.Node

%token DOMAIN MODEL TRANSFORM SYSTEM MACHINE PARTIAL DENOTATION
%token ENSURES REQUIRES CONFORMS
%token LCBRACE RCBRACE
%token LPAREN RPAREN
%token LSBRACE RSBRACE

%token INCLUDES EXTENDS IMPORTS OF RETURNS
%token AT COLON
%token RENAMES RANGE 
%token SOME ATLEAST ATMOST INITIALLY NEXT PROPERTY BOOT

%token EQ TYPEDEF PLUSEQ RULE PIPE WHERE
%token DOT
%token SEMICOLON
%token COMMA 
 
%token NO IS WEAKARROW STRONGARROW
%token NEW INJ BIJ SUR FUN ANY
%token BAREID QUALID DIGITS REAL FRAC
%token STRSNGSTART STRSNG STRSNGESC STRSNGEND
%token STRMULSTART STRMUL STRMULESC STRMULEND
%token NE LT GT GE LE

%token COUNT STRJOIN TOSTRING TOLIST RFLISMEMBER TONATURAL
%token TRUE FALSE

%left  PLUS MINUS
%left  MOD
%left  DIV
%left  MUL 
%left  UMINUS

%token QSTART QEND UQSTART UQEND QUOTERUN QUOTEESCAPE

%token RUNAWAYSTRING 
%token ALIENCHAR
%token maxParseToken 
%token LEX_WHITE LEX_COMMENT LEX_ERROR

%%

Program
   : EOF
   | ModuleList
   ;    

ModuleList
   : Module
   | Module ModuleList
   ;

Module 
   : Domain
   | Model
   | Transform
   //| DenotationalSpecification
   ;
   
/**************  Models ************/

Model
   : ModelSig LCBRACE RCBRACE
   | ModelSig LCBRACE ModelSentences RCBRACE
   ;

ModelSig
   : MODEL     
	 BAREID
	 OF
	 ModRef    { StartModel($2.Name, false, $4, ToSpan(@1)); }

   | PARTIAL   
	 MODEL     
	 BAREID
	 OF
	 ModRef    { StartModel($3.Name, true, $5, ToSpan(@1)); }
   ;

ModelSentences
     : ModelSentence
     | ModelSentence ModelSentences
     ;

ModelSentence
	 : ModelFact
	 | ModelContract
	 ;
	 
ModelFact 
	 : HeadTerm DOT             { AppendFact(null, $1); }
	 | BAREID IS HeadTerm DOT   { AppendFact($1, $3); }
	 ;

ModelContract
	 : ENSURES BodyList DOT             { AppendPropContract(ContractKind.EnsuresProp, $2, ToSpan(@1)); }
	 | REQUIRES BodyList DOT            { AppendPropContract(ContractKind.RequiresProp, $2, ToSpan(@1)); }
	 //| REQUIRES SOME DIGITS Id DOT      { AppendCardContract(ContractKind.RequiresSome, $3.Cnst, $4, ToSpan(@1)); } 
	 //| REQUIRES ATMOST DIGITS Id DOT    { AppendCardContract(ContractKind.RequiresAtMost, $3.Cnst, $4, ToSpan(@1)); } 
	 //| REQUIRES ATLEAST DIGITS Id DOT   { AppendCardContract(ContractKind.RequiresAtLeast, $3.Cnst, $4, ToSpan(@1)); } 
	 ;
	 
/**************  Transforms  ************/

Transform 
	: TRANSFORM BAREID    { StartTransform($2.Name, ToSpan(@1)); }
	  LPAREN TransformInput RPAREN
	  RETURNS
	  LPAREN TransformOutput RPAREN
	  LCBRACE 
	  TransformBody 
	  RCBRACE
	;

TransformInput: 
    ParamList     { AppendParameters($1,isInput:true); }
	;

TransformOutput 
	:ParamList    { AppendParameters($1,isInput:false); }
	;
 
ParamList 
	: ModRefRename                     { $$ = new NodeList<Param>(new Param(((ModRef)$1).Rename, $1) { Span = ToSpan(@1) }, null); }
	| ModRefRename COMMA ParamList     { $$ = new NodeList<Param>(new Param(((ModRef)$1).Rename, $1) { Span = ToSpan(@1) }, (NodeList<Param>)$3); }
	;

TransformBody
	: TransSentence
	| TransSentence TransformBody
	;
	 
TransSentence
	: Rule                      { AppendRule($1); }
	| TypeDecl	                { AppendTypeDecl($1); }
	| ENSURES BodyList DOT      { AppendPropContract(ContractKind.EnsuresProp, $2, ToSpan(@1)); }
	| REQUIRES BodyList DOT     { AppendPropContract(ContractKind.RequiresProp, $2, ToSpan(@1)); }
	;

/**************  Domains  ************/

Domain 
	: DOMAIN BAREID    { StartDomain((Id)$2, ToSpan(@1)); }
	  DomainSig
	  LCBRACE DomSentences RCBRACE
	;

DomainSig
	:
    | EXTENDS  ModRefs   { AppendCompositions($2, ComposeKind.Extends); }
	| INCLUDES ModRefs   { AppendCompositions($2, ComposeKind.Includes); }
	| IMPORTS  ModRefs   { AppendCompositions($2, ComposeKind.Imports); }
	;

DomSentences 
    : DomSentence
	| DomSentence DomSentences   
	;

DomSentence 
	: Rule                    { AppendRule($1); }
	| TypeDecl                { AppendTypeDecl($1); }
	| CONFORMS BodyList DOT   { AppendPropContract(ContractKind.ConformsProp, $2, ToSpan(@1)); }
	;

/**************  Module References ************/

ModRefs : ModRef                   { $$ = new NodeList<ModRef>((ModRef)$1, null); }
		| ModRef COMMA ModRefs     { $$ = new NodeList<ModRef>((ModRef)$1, (NodeList<ModRef>)$3); }
		;

ModRef 
		: ModRefRename
	    | BAREID              { $$ = MkModRef(ToSpan(@1), $1.Name, null, null); }
	    | BAREID AT String    { $$ = MkModRef(ToSpan(@1), $1.Name, null, $3.Name); }
	    ;

ModRefRename
		: BAREID RENAMES BAREID            { $$ = MkModRef(ToSpan(@1), $3.Name, $1.Name, null); }
		| BAREID RENAMES BAREID AT String  { $$ = MkModRef(ToSpan(@1), $3.Name, $1.Name, ((StringCnst)$5).Cnst); }
		;

/**************** Type Decls *****************/

TypeDecl 
	: BAREID TYPEDEF UnnBody DOT                  { $$ = MkUnnOrEnumDecl(ToSpan(@1), $1.Name, false, (NodeList<Node>)$3); }
	| Id     PLUSEQ UnnBody DOT                   { $$ = MkUnnOrEnumDecl(ToSpan(@1), $1.Name, true, (NodeList<Node>)$3); }
	| BAREID TYPEDEF LPAREN Fields RPAREN DOT     { $$ = Nodes.ConDecl.Create(ToSpan(@1), $1.Name, false, (NodeList<Field>)$4); }
	| BAREID TYPEDEF NEW LPAREN Fields RPAREN DOT { $$ = Nodes.ConDecl.Create(ToSpan(@1), $1.Name, true, (NodeList<Field>)$5); }
	| BAREID TYPEDEF FUN 
	  LPAREN Fields WEAKARROW Fields RPAREN DOT   { $$ = Nodes.ConDecl.CreateMap(ToSpan(@1), $1.Name, MapKind.Fun, true, (NodeList<Field>)$5, (NodeList<Field>)$7); }	  
	| BAREID TYPEDEF FUN 
	  LPAREN Fields STRONGARROW Fields RPAREN DOT   { $$ = Nodes.ConDecl.CreateMap(ToSpan(@1), $1.Name, MapKind.Fun, false, (NodeList<Field>)$5, (NodeList<Field>)$7); }	  
	| BAREID TYPEDEF 
	  LSBRACE Fields STRONGARROW Fields RSBRACE DOT { $$ = Nodes.ConDecl.CreateGoalDriven(ToSpan(@1), $1.Name, false, (NodeList<Field>)$4, (NodeList<Field>)$6); }
	| BAREID COLON Fields WEAKARROW Fields DOT    { $$ = Nodes.ConDecl.CreateDenotational(ToSpan(@1), $1.Name, (NodeList<Field>)$3, (NodeList<Field>)$5); }
	;

Fields 
	: Field                  { $$ = new Nodes.NodeList<Field>((Field)$1,null); }
	| Field COMMA Fields     { $$ = new Nodes.NodeList<Field>((Field)$1,(NodeList<Field>)$3); }
	;

Field 
	  : UnnBody                      { $$ = MkField(ToSpan(@1), null, (NodeList<Node>)$1, isAny:false); }
	  | ANY UnnBody                  { $$ = MkField(ToSpan(@1), null, (NodeList<Node>)$2, isAny:true); }
	  | BAREID COLON UnnBody         { $$ = MkField(ToSpan(@1), $1.Name, (NodeList<Node>)$3, isAny:false); }
	  | BAREID COLON ANY UnnBody     { $$ = MkField(ToSpan(@1), $1.Name, (NodeList<Node>)$4, isAny:true); }
	  ;

/**************** Type Terms *****************/

UnnBody 
	: UnnCmp                { $$ = new Nodes.NodeList<Nodes.Node>((Node)$1, null); }
	| UnnCmp PLUS UnnBody   { $$ = new Nodes.NodeList<Nodes.Node>((Node)$1, (NodeList<Node>)$3); }
	;
		
UnnCmp 
	: Id
	| Constant
	| LCBRACE EnumList RCBRACE   { $$ = $2; }
	;
	
EnumList 
	: EnumCnst                   { $$ = new Nodes.NodeList<EnumCnst>((EnumCnst)$1,null); }
	| EnumCnst COMMA EnumList    { $$ = new Nodes.NodeList<EnumCnst>((EnumCnst)$1,(NodeList<EnumCnst>)$3); }
	;

EnumCnst
	: BAREID      { $$ = new Nodes.EnumCnst($1.Name) { Span = ToSpan(@1) };  }
	| QUALID      { $$ = new Nodes.EnumCnst($1.Name) { Span = ToSpan(@1) };  }
	| Constant    { $$ = new Nodes.EnumCnst($1.Name) { Span = ToSpan(@1) };  }
	;

/************* Facts, Rules, and Comprehensions **************/

Rule
	 : HeadList DOT                    { $$ = new Nodes.RuleNode((NodeList<UserFuncTerm>)$1,null); }
	 | HeadList RULE BodyList DOT      { $$ = new Nodes.RuleNode((NodeList<UserFuncTerm>)$1,(NodeList<Body>)$3); }
	 | DenotationalHead WHERE BodyList DOT     { $$ = new Nodes.RuleNode((NodeList<UserFuncTerm>)$1,(NodeList<Body>)$3); }
	 | DenotationalHead DOT                    { $$ = new Nodes.RuleNode((NodeList<UserFuncTerm>)$1,null); }
	 ;

HeadList
	: Head                     { $$ = new NodeList<UserFuncTerm>((UserFuncTerm)$1, null) { Span = ToSpan(@1) }; }
    | Head COMMA HeadList      { $$ = new NodeList<UserFuncTerm>((UserFuncTerm)$1, (NodeList<UserFuncTerm>)$3) { Span = ToSpan(@1) }; }
	;

Head
	: Id                                    { $$ = MkUserFuncTerm(ToSpan(@1),$1,null,null); }
	| UserFuncTermHead
	;

UserFuncTermHead
    : Id LPAREN HeadTerms RPAREN            { $$ = MkUserFuncTerm(ToSpan(@1),$1,$3,null); }
	| Id LPAREN HeadTerms RPAREN 
	  STRONGARROW HeadTerm                  { $$ = MkUserFuncTerm(ToSpan(@1),$1,$3,$6); }
	| Id LPAREN HeadTerms RPAREN 
	  STRONGARROW LPAREN HeadTerms RPAREN   { $$ = MkUserFuncTerm(ToSpan(@1),$1,$3,$7); }	
	;

// id [[id]] = term term
// id [[id]] term term = term term
// id [[id]] = (term,term) (term,term)
// id [[id]] term term = (term,term) (term,term)
DenotationalHead
    : Id LSBRACE LSBRACE Id RSBRACE RSBRACE EQ FuncTermsNoExpSpaced       { $$ = MkDenotationalHead(ToSpan(@1),$1,$4,null,$8); }
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE
	  HeadTermsSpaced EQ FuncTermsNoExpSpaced                             { $$ = MkDenotationalHead(ToSpan(@1),$1,$4,$7,$9); }
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE EQ FuncTermsNoExpSpacedParen  { $$ = MkDenotationalHead(ToSpan(@1),$1,$4,null,$8); }
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE
	  HeadTermsSpacedParen EQ FuncTermsNoExpSpacedParen                   { $$ = MkDenotationalHead(ToSpan(@1),$1,$4,$7,$9); }
	;

// Separated by comma
HeadTerms
    : HeadTerm                     { $$ = new NodeList<Node>($1, null) { Span = ToSpan(@1) }; }
	| HeadTerm COMMA HeadTerms     { $$ = new NodeList<Node>($1, (NodeList<Node>)$3) { Span = ToSpan(@1) }; }
	;

// Separated by whitespace
HeadTermsSpaced
    : HeadTerm                     { $$ = new NodeList<Node>($1, null) { Span = ToSpan(@1) }; }
	| HeadTerm HeadTermsSpaced     { $$ = new NodeList<Node>($1, (NodeList<Node>)$2) { Span = ToSpan(@1) }; }
	;

// Separated by whitespace
HeadTermsSpacedParen
    : LPAREN HeadTerms RPAREN                       { $$ = new NodeList<Node>($2, null) { Span = ToSpan(@1) }; }
	| LPAREN HeadTerms RPAREN HeadTermsSpacedParen  { $$ = new NodeList<Node>($2, (NodeList<Node>)$4) { Span = ToSpan(@1) }; }
	;
	
HeadTerm
    : Id
	| Constant
	| UserFuncTermHead
	;		

BodyList 
	: Body                         { $$ = new Nodes.NodeList<Body>(new Body((NodeList<Node>)$1),null); }
	| Body SEMICOLON BodyList      { $$ = new Nodes.NodeList<Body>(new Body((NodeList<Node>)$1),(NodeList<Body>)$3); }
	;

Body 
	: Constraint                   { $$ = new Nodes.NodeList<Node>($1,null); }
	| Constraint COMMA Body        { $$ = new Nodes.NodeList<Node>($1,(NodeList<Node>)$3); }
	;

Compr  
	 //: LCBRACE FuncTermList RCBRACE                  { $$ = new Nodes.Compr($2,null); }
	 : LCBRACE FuncTermList PIPE BodyList RCBRACE    { $$ = new Nodes.Compr((NodeList<Node>)$2,(NodeList<Body>)$4); }
	 ;

/******************* Terms and Constraints *******************/

Constraint
	: Find
	| Relation	
	| DenotationalApp         { $$ = new Nodes.Find(null, $1) { Span = ToSpan(@1) }; }
	;
		
Find : FindMatch   		      { $$ = new Nodes.Find(null, $1) { Span = ToSpan(@1) }; }
	 | Id IS FindMatch        { $$ = new Nodes.Find(new Var($1.Name){ Span = $1.Span }, $3) { Span = ToSpan(@1) }; }
	 ;
	 
FindMatch
	: Id
	| UserFuncTerm
	;

UserFuncTerm
    : Id LPAREN FuncTermsNoExp RPAREN            { $$ = MkUserFuncTerm(ToSpan(@1),$1,$3,null); }
	| Id LPAREN FuncTermsNoExp RPAREN 
	  STRONGARROW FuncTermNoExp                  { $$ = MkUserFuncTerm(ToSpan(@1),$1,$3,$6); }
	| Id LPAREN FuncTermsNoExp RPAREN 
	  STRONGARROW LPAREN FuncTermsNoExp RPAREN   { $$ = MkUserFuncTerm(ToSpan(@1),$1,$3,$7); }	
	;

Relation 
	: NO Compr               { $$ = new RelConstr(RelKind.No, $2) { Span = ToSpan(@1) }; }
	| NO FuncTerm		     { $$ = MkNoConstr(ToSpan(@1), $2); }
	| NO Id IS FuncTerm      { $$ = MkNoConstr(ToSpan(@1), $2, $4); } 
    | FuncTerm EQ FuncTerm   { $$ = MkEquality(ToSpan(@1), $1, $3); }
	| FuncTerm NE FuncTerm   { $$ = new RelConstr(RelKind.Neq, $1, $3) { Span = ToSpan(@1) }; }
	| FuncTerm COLON UnnBody { $$ = MkInlineTypeCheck(ToSpan(@1), $1, $3); }
	// | FuncTerm COLON Id      { $$ = new RelConstr(RelKind.Typ, $1, $3) { Span = ToSpan(@1) }; }
	| exp LT exp             { $$ = new RelConstr(RelKind.Lt, $1, $3) { Span = ToSpan(@1) }; }
	| exp LE exp             { $$ = new RelConstr(RelKind.Le, $1, $3) { Span = ToSpan(@1) }; }
	| exp GT exp             { $$ = new RelConstr(RelKind.Gt, $1, $3) { Span = ToSpan(@1) }; }
	| exp GE exp             { $$ = new RelConstr(RelKind.Ge, $1, $3) { Span = ToSpan(@1) }; }
	;
	
// id [[id]] = term
// id [[id]] term term = term
// id [[id]] = (term,term)
// id [[id]] term term = (term,term)
DenotationalApp
    : Id LSBRACE LSBRACE Id RSBRACE RSBRACE EQ FuncTermNoExp    { $$ = MkDenotationalTerm(ToSpan(@1),$1,$4,null,$8); }
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE
	  FuncTermsNoExpSpaced EQ FuncTermNoExp                     { $$ = MkDenotationalTerm(ToSpan(@1),$1,$4,$7,$9); }
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE EQ 
	  LPAREN FuncTermsNoExp RPAREN                              { $$ = MkDenotationalTerm(ToSpan(@1),$1,$4,null,$9); }
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE
	  FuncTermsNoExpSpacedParen EQ LPAREN FuncTermsNoExp RPAREN { $$ = MkDenotationalTerm(ToSpan(@1),$1,$4,$7,$10); }
	;

// Separated by comma
FuncTermsNoExp
    : FuncTermNoExp                          { $$ = new NodeList<Node>($1, null) { Span = ToSpan(@1) }; }
	| FuncTermNoExp COMMA FuncTermsNoExp     { $$ = new NodeList<Node>($1, (NodeList<Node>)$3) { Span = ToSpan(@1) }; }
	;

// Separated by whitespace	
FuncTermsNoExpSpaced
    : FuncTermNoExp                          { $$ = new NodeList<Node>($1, null) { Span = ToSpan(@1) }; }
	| FuncTermNoExp FuncTermsNoExpSpaced     { $$ = new NodeList<Node>($1, (NodeList<Node>)$2) { Span = ToSpan(@1) }; }
	;

// Separated by whitespace	
FuncTermsNoExpSpacedParen
    : LPAREN FuncTermsNoExp RPAREN                            { $$ = new NodeList<Node>($2, null) { Span = ToSpan(@1) }; }
	| LPAREN FuncTermsNoExp RPAREN FuncTermsNoExpSpacedParen  { $$ = new NodeList<Node>($2, (NodeList<Node>)$4) { Span = ToSpan(@1) }; }
	;

FuncTermNoExp
    : Id
	| Constant
	| UserFuncTerm
	| Id LSBRACE LSBRACE Id RSBRACE RSBRACE      { $$ = MkUserFuncTerm(ToSpan(@1),$1,new NodeList<Node>($4,null),null); }
	| Id LPAREN LSBRACE LSBRACE Id RSBRACE RSBRACE FuncTermsNoExpSpaced RPAREN   { $$ = MkUserFuncTerm(ToSpan(@1),$1,new NodeList<Node>($5,null),$8); }
	;	

FuncTermList
    : FuncTerm                     { $$ = new NodeList<Node>($1, null) { Span = ToSpan(@1) }; }
	| FuncTerm COMMA FuncTermList  { $$ = new NodeList<Node>($1, (NodeList<Node>)$3) { Span = ToSpan(@1) }; }
	;
	
FuncTerm
    : exp
	| BuiltinOperations
	| UserFuncTerm
	;

exp
    : Id
	| LPAREN exp RPAREN
	| Constant
	| MINUS exp %prec UMINUS { $$ = MkExpr(ToSpan(@1), OpKind.Neg, $2, null); }
	| exp MUL exp            { $$ = MkExpr(ToSpan(@1), OpKind.Mul, $1, $3);  }
	| exp DIV exp            { $$ = MkExpr(ToSpan(@1), OpKind.Div, $1, $3);  }
	| exp PLUS exp           { $$ = MkExpr(ToSpan(@1), OpKind.Add, $1, $3);  }
	| exp MINUS exp          { $$ = MkExpr(ToSpan(@1), OpKind.Sub, $1, $3);  }
	| exp MOD exp            { $$ = MkExpr(ToSpan(@1), OpKind.Mod, $1, $3);  }
	;
	
BuiltinOperations 
	: ListOperations
	| StringOperations
	| ReflectionOperations
	| COUNT LPAREN Compr RPAREN          { var func = new BuiltinFuncTerm(OpKind.Count) { Span = ToSpan(@1) }; func.AddArg($3); $$ = func; }
	| TONATURAL LPAREN Id RPAREN         { var func = new BuiltinFuncTerm(OpKind.ToNatural) { Span = ToSpan(@1) }; func.AddArg($3); $$ = func; }
	;
	
ReflectionOperations
	: RFLISMEMBER LPAREN Id COMMA Id RPAREN             { $$ = MkRflIsMember(ToSpan(@1),$3,$5); }
	;

ListOperations
	: TOLIST LPAREN Id COMMA EnumCnst COMMA Compr RPAREN      { $$ = MkToList(ToSpan(@1),$3,$5,$7); }
	;

StringOperations
	: TOSTRING LPAREN FuncTermNoExp RPAREN                   { $$ = MkToString(ToSpan(@3),$3); }
	| STRJOIN LPAREN StringResult COMMA StringResult RPAREN   { $$ = MkStrJoin(ToSpan(@3),$3,$5); }
	;

StringResult
	: String
	| StringOperations
	| Id
	;

Id
	 : BAREID
	 | QUALID
	 ;
	 
Constant 
	 : DIGITS      { $$ = ParseNumeric($1.Name, false, ToSpan(@1)); }
	 | REAL        { $$ = ParseNumeric($1.Name, false, ToSpan(@1)); }
	 | FRAC        { $$ = ParseNumeric($1.Name, true, ToSpan(@1)); }
	 | String
	 | TRUE        { $$ = new BoolCnst(true) { Span = ToSpan(@1) }; }
	 | FALSE       { $$ = new BoolCnst(false) { Span = ToSpan(@1) }; }
	 ;

String : StrStart StrBodyList StrEnd    { $$ = GetString(ToSpan(@1)); }
	   | StrStart StrEnd                { $$ = GetString(ToSpan(@1)); }
	   ;

StrStart : STRSNGSTART { StartString(ToSpan(@1)); }
		 ;

StrBodyList 
        : StrBody
		| StrBody StrBodyList
		;

StrBody : STRSNG     { AppendString($1.Name); }
	    | STRSNGESC  { AppendSingleEscape($1.Name); }
		;

StrEnd  : STRSNGEND { EndString(ToSpan(@1)); }
		;

%%
