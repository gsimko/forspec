﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe
{
    interface IGraph<T>
    {
        IEnumerable<T> GetVertices();
        IEnumerable<T> GetNeighbors(T vertex);
    }
    class Tarjan<T>
    {
        private List<List<T>> SCC;

        public int Count
        {
            get { return SCC.Count; }
        }
        public IEnumerable<T> this[int index]
        {
            get { if (index >= SCC.Count) throw new IndexOutOfRangeException(); return SCC[index]; }
        }

        public Tarjan()
        {
        }

        class Internal
        {
            public int index;
            public int lowlink;
        }
        Dictionary<T, Internal> vertexInfo;
        IGraph<T> myGraph;
        Stack<T> stack;
        int index;
        public void SetGraph(IGraph<T> graph)
        {
            myGraph = graph;
            SCC = new List<List<T>>();
            index = 0;
            stack = new Stack<T>();
            vertexInfo = new Dictionary<T, Internal>();
            foreach (var v in graph.GetVertices())
            {
                Internal info = new Internal();
                info.index = -1;
                vertexInfo.Add(v, info);
            }
            foreach (var v in vertexInfo.Keys)
            {
                if (vertexInfo[v].index == -1)
                    strongConnect(v);
                if (stack.Count > 0)
                    throw new UserException("Tarjan algorithm should have left the stack empty", null);
            }
            if (stack.Count > 0)
                throw new UserException("Tarjan algorithm should have left the stack empty", null);
        }
        private void strongConnect(T vertex)
        {
            var vinfo = vertexInfo[vertex];
            vinfo.index = index;
            vinfo.lowlink = index;
            stack.Push(vertex);
            index++;

            foreach (var n in myGraph.GetNeighbors(vertex))
            {
                var ninfo = vertexInfo[n];
                if (ninfo.index == -1)
                {
                    strongConnect(n);
                    vinfo.lowlink = Math.Min(vinfo.lowlink, ninfo.lowlink);
                }
                else if (stack.Contains(n))
                {
                    vinfo.lowlink = Math.Min(vinfo.lowlink, ninfo.index);
                }
            }

            // If root node, extract SCC
            if (vinfo.index == vinfo.lowlink)
            {
                var scc = new List<T>();
                T w;
                do
                {
                    w = stack.Pop();
                    scc.Add(w);
                } while (!vertex.Equals(w));
                SCC.Add(scc);
            }
        }
    }
}
