﻿namespace FormulaBe
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Text;
    using Nodes;
    using Microsoft.Formula.Common;
    using Microsoft.Formula.API;

    public sealed class ParseResult
    {
        public List<Flag> Flags = new List<Flag>();
        public List<Program> Programs = new List<Program>();
        public List<Module> OrderedModules = new List<Module>();
        public HashSet<String> ToLoad = new HashSet<String>();
        public HashSet<String> Loaded = new HashSet<String>();
        public bool Succeeded = true;

        internal Program Program = null;
                
        public void AddFlag(Flag f)
        {
            Succeeded = Succeeded && f.Severity != SeverityKind.Error;
            Flags.Add(f);
        }

        public void ClearFlags()
        {
            Flags.Clear();
            Succeeded = true;
        }
    }
}
