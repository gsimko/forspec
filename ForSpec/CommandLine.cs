﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FormulaBe
{
    using Nodes;

    class Log
    {
        static TextWriter tw = File.CreateText("debugger.log");
        public static int g_Level = 40;
        public static bool g_DetailedNodeInformation = false; // print detailed node information (for debugging)
        public static void WriteLine(int level, string msg)
        {
            if (level <= g_Level)
            {
                tw.WriteLine(msg);
                tw.Flush();
            }
        }
        public static void WriteLine(int level, string msg, params object[] args)
        {
            if (level <= g_Level)
            {
                tw.WriteLine(msg, args);
                tw.Flush();
            }
        }
    }

    class ConsoleSink
    {       

        public System.IO.TextWriter Writer
        {
            get { return Console.Out; }
        }

        public void WriteMessage(string msg)
        {
            SetSeverityColor(SeverityKind.Default);
            Console.Write(msg);
            ResetSeverityColor();
        }

        public void WriteMessage(SeverityKind severity, string msg)
        {
            SetSeverityColor(severity);
            Console.Write(msg);
            ResetSeverityColor();
        }

        public void WriteMessageLine(string msg)
        {
            SetSeverityColor(SeverityKind.Default);
            Console.WriteLine(msg);
            ResetSeverityColor();
        }
        public void WriteMessageLine(string format, params object[] args)
        {
            SetSeverityColor(SeverityKind.Default);
            Console.WriteLine(format, args);
            ResetSeverityColor();
        }

        public void WriteMessageLine(SeverityKind severity, string msg)
        {
            SetSeverityColor(severity);
            Console.WriteLine(msg);
            ResetSeverityColor();
        }
        public void WriteMessageLine(SeverityKind severity, string msg, params object[] args)
        {
            SetSeverityColor(severity);
            Console.WriteLine(msg, args);
            ResetSeverityColor();
        }
        private void SetSeverityColor(SeverityKind severity)
        {
            switch (severity)
            {
                case SeverityKind.Info:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case SeverityKind.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case SeverityKind.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }
        }
        private void ResetSeverityColor()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
        }        
    }
        
    class CommandLine
    {        
        static ConsoleSink Sink = new ConsoleSink();

        //public static Dictionary<string,Program> Programs = new Dictionary<string,Program>();
        public static Dictionary<string,Module> Modules = new Dictionary<string,Module>();

        public static TextWriter Debug = File.CreateText("debug.log");

        public static void Load(string s)
        {
            try
            {
                var startTime = DateTime.Now.Ticks;
                ParseResult resultB = FormulaBeParser.ParseAll(s);
                var endTime = DateTime.Now.Ticks;
                Sink.WriteMessageLine(SeverityKind.Info, "{0:0.00}s", new TimeSpan(endTime - startTime).TotalSeconds);
                if (resultB.Succeeded)
                {
                    /*foreach (var p in resultB.Programs)
                    {
                        string name = p.Name.Uri.AbsoluteUri;
                        if (Programs.ContainsKey(name))
                        {
                            Sink.WriteMessageLine(String.Format("Overwriting program {0}", name));
                            Programs[name] = p;
                        }
                        else
                            Programs.Add(name,p);
                    }*/
                    foreach (var p in resultB.OrderedModules)
                    {
                        string name = p.Name;
                        if (Modules.ContainsKey(name))
                        {
                            Sink.WriteMessageLine(SeverityKind.Warning, "Overwriting module {0}", name);
                            Modules[name] = p;
                        }
                        else
                        {
                            Sink.WriteMessageLine(SeverityKind.Info, "Loading module {0}", name);
                            Modules.Add(name, p);
                        }
                        //Debug.WriteLine(p);
                        //Debug.Flush();
                    }
                    foreach (var p in Modules.Values)
                    {
                        //var tc = new TypeChecking();
                        //tc.Check(p, Modules);
                    }
                    /*foreach (var p in resultB.OrderedModules)
                    {
                        Sink.WriteMessageLine(String.Format("Typechecking module {0}", p.Name), SeverityKind.Info);
                        var tc = new TypeChecking();
                        var merge = MergeModules.MergeAncestors(Modules, p);

                        Debug.WriteLine(merge);
                        Debug.Flush();
                        tc.Check(merge);
                    }*/
                }
            } 
            catch (UserException e)
            {
                Sink.WriteMessageLine(SeverityKind.Error, "({0}, {1}): {2}",
                            e.node != null ? e.node.Span.StartLine : 0,
                            e.node != null ? e.node.Span.StartCol : 0,
                            e.msg);
            }
            catch (FlagException e)
            {
                foreach (var flag in e.flags)
                {
                    if (flag.ProgramName != null)
                        Sink.WriteMessageLine(flag.Severity, "In file {0}", flag.ProgramName.ToString());
                    Sink.WriteMessageLine(flag.Severity, "({0}, {1}): {2}",
                                flag.Span.StartLine,
                                flag.Span.StartCol,
                                flag.Message);
                }
            }
        }
        public static void Execute(string s)
        {
            try
            {
                Module m;
                if (Modules.TryGetValue(s, out m) && m is Domain)
                {
                    Sink.WriteMessageLine(SeverityKind.Info, "Finding fix-point for {0}", s);
                    var E = new Execution();
                    var merge = MergeModules.MergeAncestors(Modules, m, true);
                    Debug.WriteLine(merge); Debug.Flush();
                    var fixpoint = E.Execute(merge, Modules);
                    var model = MergeModules.ToModel(fixpoint);
                    model.Name = s + "_fp";                    
                    Modules.Add(model.Name, model);
                    Sink.WriteMessageLine(SeverityKind.Info, "Model {0} created", model.Name);
                }
                else
                    Sink.WriteMessageLine(SeverityKind.Warning, "Domain/Model not found.");
            }
            catch (UserException e)
            {
                Sink.WriteMessageLine(SeverityKind.Error, "({0}, {1}): {2}\n{3}",
                            e.node != null ? e.node.Span.StartLine : 0,
                            e.node != null ? e.node.Span.StartCol : 0,
                            e.msg,
                            e.node);
            }
            catch (FlagException e)
            {
                foreach (var flag in e.flags)
                    Sink.WriteMessageLine(flag.Severity, "({0}, {1}): {2}",
                                flag.Span.StartLine,
                                flag.Span.StartCol,
                                flag.Message);
            }
        }
        public static void ExecuteTransform(string[] output, string[] input)
        {
            try
            {
                Module m;
                Sink.WriteMessageLine(SeverityKind.Info, "Executing transformation {0}", input);
                if (Modules.TryGetValue(input[0], out m) && m is Transform)
                {
                    Transform tf = (Transform)m;
                    if (input.Length != tf.Inputs.Count + 1)
                    {
                        Sink.WriteMessageLine(SeverityKind.Warning, "Transformation needs {0} arguments but got {1}.", tf.Inputs.Count, input.Length);
                        return;
                    }
                    if (output.Length != tf.Outputs.Count)
                    {
                        Sink.WriteMessageLine(SeverityKind.Warning, "Transformation produces {0} outputs.", tf.Outputs.Count, output.Length);
                        return;
                    }
                    var startTime = DateTime.Now.Ticks;                    
                    var E = new Execution();
                    //var merge = MergeModules.MergeInputs(Modules, (Transform)m, new List<string>(s.Skip(1)));
                    //Debug.Write(merge); Debug.Flush();
                    //foreach (var mod in Modules)
                        //Debug.WriteLine(mod.Value); 
                    //Debug.Flush();
                    var merge = MergeModules.MergeAncestorsAndInputs(Modules, (Transform)m, new List<string>(input.Skip(1)));
                    Debug.WriteLine(merge); Debug.Flush();
                    var result = E.Execute(merge, Modules);
                    foreach (var model in MergeModules.ToModels(Modules, result, (Transform)m, output))
                        Modules.Add(model.Name, model);
                    var endTime = DateTime.Now.Ticks;
                    Sink.WriteMessageLine(SeverityKind.Info, "{0:0.00}s", new TimeSpan(endTime - startTime).TotalSeconds);
                }
                else
                    Sink.WriteMessageLine(SeverityKind.Warning, "Transformation not found.");
            }
            catch (UserException e)
            {
                Sink.WriteMessageLine(SeverityKind.Error, "({0}, {1}): {2}",
                            e.node != null ? e.node.Span.StartLine : 0,
                            e.node != null ? e.node.Span.StartCol : 0,
                            e.msg);
            }
            catch (FlagException e)
            {
                foreach (var flag in e.flags)
                    Sink.WriteMessageLine(flag.Severity, "({0}, {1}): {2}",
                                flag.Span.StartLine,
                                flag.Span.StartCol,
                                flag.Message);
            }
        }
        public static void ToFormula(string fileName)
        {
            try
            {
                using (var file = File.CreateText(fileName))
                {
                    FormulaEncoding enc = new FormulaEncoding();
                    enc.Print(Modules, file);
                }
            }
            catch (UserException e)
            {
                Sink.WriteMessageLine(SeverityKind.Error, "({0}, {1}): {2}",
                            e.node != null ? e.node.Span.StartLine : 0,
                            e.node != null ? e.node.Span.StartCol : 0,
                            e.msg);
            }
            catch (FlagException e)
            {
                foreach (var flag in e.flags)
                    Sink.WriteMessageLine(flag.Severity, "({0}, {1}): {2}",
                                flag.Span.StartLine,
                                flag.Span.StartCol,
                                flag.Message);
            }
        }
        public static void ToFormula(string fileName, string moduleName)
        {
            try
            {
                using (var file = File.CreateText(fileName))
                {
                    FormulaEncoding enc = new FormulaEncoding();
                    enc.Print(Modules, file);
                }
            }
            catch (UserException e)
            {
                Sink.WriteMessageLine(SeverityKind.Error, "({0}, {1}): {2}",
                            e.node != null ? e.node.Span.StartLine : 0,
                            e.node != null ? e.node.Span.StartCol : 0,
                            e.msg);
            }
            catch (FlagException e)
            {
                foreach (var flag in e.flags)
                    Sink.WriteMessageLine(flag.Severity, "({0}, {1}): {2}",
                                flag.Span.StartLine,
                                flag.Span.StartCol,
                                flag.Message);
            }
        }
        public static void PrintConstants(string moduleName)
        {
            Module m;
            if (Modules.TryGetValue(moduleName, out m))
            {
                TypeChecking tc = new TypeChecking();
                tc.Check(m, Modules);

                Sink.WriteMessageLine("Constants in module {0}:", moduleName);
                foreach (var c in tc.constants)
                    Sink.WriteMessageLine("\t{0}",c);
            }            
            else
                Sink.WriteMessageLine("Module {0} not found", moduleName);
        }
        public static void PrintTypeDeclarations(string moduleName)
        {
            Module m;
            if (Modules.TryGetValue(moduleName, out m))
            {
                TypeChecking tc = new TypeChecking();
                tc.Check(m, Modules);

                Sink.WriteMessageLine("Type declarations in module {0}:", moduleName);
                foreach (var t in tc.typeDecls)
                    Sink.WriteMessageLine("\t{0}", t);
            }
            else
                Sink.WriteMessageLine("Module {0} not found", moduleName);
        }
        public static void PrintModule(string moduleName, TextWriter to)
        {
            Module m;
            if (Modules.TryGetValue(moduleName, out m))
            {
                to.WriteLine(m);
            }
            else
                Sink.WriteMessageLine("Module {0} not found", moduleName);
        }
        public static void Repl(string args)
        {
            TextReader tr = Console.In;
            string line = args;
            if (!String.IsNullOrEmpty(line))
                Console.WriteLine(">> {0}", args);
            while (true)
            {
                if (String.IsNullOrEmpty(line))
                {
                    Sink.WriteMessage(">> ");
                    line = tr.ReadLine();
                }
                line = line.TrimStart(new char[]{' ','\t'});
                int firstsemicolon = line.IndexOf(";");
                string cmd;
                if (firstsemicolon != -1)
                {
                    cmd = line.Substring(0, firstsemicolon);
                    line = line.Substring(firstsemicolon + 1);
                }
                else
                {
                    cmd = line;
                    line = null;
                }
                if (cmd.Equals("exit") || cmd.Equals("x"))
                {
                    return;
                }
                else if (cmd.StartsWith("render") || cmd.StartsWith("r "))
                {
                    var split = cmd.Split(new char[] { ' ', '\t' });
                    if (split.Length < 2)
                        Sink.WriteMessageLine("Not enough parameters. Please specify the module name.");
                    else
                        PrintModule(split[1], Console.Out);
                }
                else if (cmd.StartsWith("constants") || cmd.StartsWith("c "))
                {
                    var split = cmd.Split(new char[] { ' ', '\t' });
                    if (split.Length < 2)
                        Sink.WriteMessageLine("Not enough parameters. Please specify the module name.");
                    else
                        PrintConstants(split[1]);
                }
                else if (cmd.StartsWith("typ") || cmd.StartsWith("t "))
                {
                    var split = cmd.Split(new char[] { ' ', '\t' });
                    if (split.Length < 2)
                        Sink.WriteMessageLine("Not enough parameters. Please specify the module name.");
                    else
                        PrintTypeDeclarations(split[1]);
                }
                else if (cmd.StartsWith("list") || cmd.Equals("ls"))
                {
                    Sink.WriteMessageLine("Currently loaded modules:");
                    foreach (var m in Modules)
                        Sink.WriteMessageLine("\t{0}", m.Key);
                }                
                else if (cmd.StartsWith("load") || cmd.StartsWith("l "))
                {
                    var split = cmd.Split(new char[] { ' ', '\t' });
                    if (split.Length < 2)
                        Sink.WriteMessageLine("Not enough parameters. Please specify the module name.");
                    else
                        Load(split[1]);
                }
                else if (cmd.StartsWith("save") || cmd.StartsWith("sv "))
                {
                    var split = cmd.Split(new char[] { ' ', '\t' });
                    if (split.Length < 3)
                        Sink.WriteMessageLine("Not enough parameters. Please specify the module name and the output filename.");
                    else
                    {
                        using (var output = File.CreateText(split[2]))
                            PrintModule(split[1], output);
                    }
                }
                else if (Modules.ContainsKey(cmd) && Modules[cmd] is Domain)
                {
                    Execute(cmd);
                }
                else if (cmd.Contains("("))
                {
                    var ipos = cmd.IndexOf('=');
                    string output = ipos != -1 ? cmd.Substring(0,ipos) : null;
                    string input = ipos != -1 ? cmd.Substring(ipos+1) : cmd;
                    var osplit = output.Split(new char[] { ',', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    var isplit = input.Split(new char[] { '(', ',', ')', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    ExecuteTransform(osplit,isplit);
                }
                else if (cmd.StartsWith("4ml"))
                {
                    var split = cmd.Split(new char[] { ' ', '\t' });
                    if (split.Length < 2)
                        Sink.WriteMessageLine("Not enough parameters. Please specify the module name.");
                    else if (split.Length == 2)
                        ToFormula(split[1]);
                    else
                        ToFormula(split[1],split[2]);
                }
                else
                    Sink.WriteMessageLine("Unknown command.");
            }
        }

        public static void Main(string[] args)
        {
            try
            {                
                //args = new string[] { "load test.4ml; load AVM2CyPhyML.4ml; a,b = AVM2CyPhyML(test);" };
                //args = new string[] { "l monitor.4mlb; a=T(M)" };
                //args = new string[] { "l operational.4mlb; l superstep.4mlb; R,I,T = fire(superstep); r R" };
                //args = new string[] { "load MAAB.4ml; load tmp_MAAB_example.4ml; o = MAAB(QuadrotorDemo);" };
                Repl(String.Join(" ", args));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
