﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Formula.API;
using Microsoft.Formula.API.Nodes;
using System.IO;

namespace FormulaBe
{
    using Nodes;
    class FormulaEncoding
    {
        public FormulaEncoding()
        {
        }

        public void Print(Dictionary<string,Module> modules, TextWriter wr)
        {
            var prog = Microsoft.Formula.API.Factory.Instance.MkProgram(new Microsoft.Formula.API.ProgramName("inmemory"));
            foreach (var mod in modules.Values)
            {
                Module merge = null;
                if (mod is Domain && !((Domain)mod).IsModel)
                {
                    merge = MergeModules.MergeAncestors(modules, mod, false);
                    MergeModules.SanitizeDots(merge);
                }
                else if (mod is Transform)
                {
                    merge = MergeModules.ResolveNamespaces(modules, (Transform)mod);
                    MergeModules.SanitizeFrom2ndDot(merge);
                    MergeModules.AddNamespaceBeforeData(mod, merge);
                }
                else
                {
                    merge = MergeModules.ResolveNamespaces(modules, (Model)mod);
                    MergeModules.SanitizeDots(merge);                    
                }
                var mod4ml = ToModuleAST(merge);
                Microsoft.Formula.API.Factory.Instance.AddModule(prog, mod4ml);
                mod4ml.Print(wr);
            }            
        }

        static string Sanitize(string id)
        {
            var res = id.Replace('#', '_');
            return res;
        }

        public Microsoft.Formula.API.Span ToSpan(Span span)
        {
            return new Microsoft.Formula.API.Span(span.StartLine, span.StartCol, span.EndLine, span.EndCol);
        }

        public Microsoft.Formula.API.RelKind ToRelKind(RelKind kind)
        {
            switch (kind)
            {
                case RelKind.Eq: return Microsoft.Formula.API.RelKind.Eq;
                case RelKind.Neq: return Microsoft.Formula.API.RelKind.Neq;
                case RelKind.Ge: return Microsoft.Formula.API.RelKind.Ge;
                case RelKind.Gt: return Microsoft.Formula.API.RelKind.Gt;
                case RelKind.Le: return Microsoft.Formula.API.RelKind.Le;
                case RelKind.Lt: return Microsoft.Formula.API.RelKind.Lt;
                case RelKind.No: return Microsoft.Formula.API.RelKind.No;
                case RelKind.Typ: return Microsoft.Formula.API.RelKind.Typ;
            }
            throw new NotImplementedException();
        }

        public Microsoft.Formula.API.OpKind ToOpKind(OpKind kind)
        {
            switch (kind)
            {
                case OpKind.Add: return Microsoft.Formula.API.OpKind.Add;
                case OpKind.Sub: return Microsoft.Formula.API.OpKind.Sub;
                case OpKind.Mul: return Microsoft.Formula.API.OpKind.Mul;
                case OpKind.Div: return Microsoft.Formula.API.OpKind.Div;
                case OpKind.Mod: return Microsoft.Formula.API.OpKind.Mod;
                case OpKind.Neg: return Microsoft.Formula.API.OpKind.Neg;
                case OpKind.ToList: return Microsoft.Formula.API.OpKind.ToList;
                case OpKind.ToString: return Microsoft.Formula.API.OpKind.ToString;
                case OpKind.StrJoin: return Microsoft.Formula.API.OpKind.StrJoin;
                case OpKind.ToNatural: return Microsoft.Formula.API.OpKind.ToNatural;
                case OpKind.RflIsMember: return Microsoft.Formula.API.OpKind.RflIsMember;
                case OpKind.Count: return Microsoft.Formula.API.OpKind.Count;
            }
            throw new NotImplementedException();
        }
        public Microsoft.Formula.API.ComposeKind ToComposeKind(ComposeKind kind)
        {
            switch (kind)
            {
                case ComposeKind.Extends: return Microsoft.Formula.API.ComposeKind.Extends;
                case ComposeKind.Includes: return Microsoft.Formula.API.ComposeKind.Includes;
                case ComposeKind.None: return Microsoft.Formula.API.ComposeKind.None;
            }
            throw new NotImplementedException();
        }
        public Microsoft.Formula.API.MapKind ToMapKind(MapKind kind)
        {
            switch (kind)
            {
                case MapKind.Fun: return Microsoft.Formula.API.MapKind.Fun;
                case MapKind.Bij: return Microsoft.Formula.API.MapKind.Bij;
                case MapKind.Inj: return Microsoft.Formula.API.MapKind.Inj;
                case MapKind.Sur: return Microsoft.Formula.API.MapKind.Sur;
            }
            throw new NotImplementedException();
        }
        public Microsoft.Formula.API.ContractKind ToContractKind(ContractKind kind)
        {
            switch (kind)
            {
                case ContractKind.ConformsProp: return Microsoft.Formula.API.ContractKind.ConformsProp;
                case ContractKind.EnsuresProp: return Microsoft.Formula.API.ContractKind.EnsuresProp;
                case ContractKind.RequiresAtLeast: return Microsoft.Formula.API.ContractKind.RequiresAtLeast;
                case ContractKind.RequiresAtMost: return Microsoft.Formula.API.ContractKind.RequiresAtMost;
                case ContractKind.RequiresProp: return Microsoft.Formula.API.ContractKind.RequiresProp;
                case ContractKind.RequiresSome: return Microsoft.Formula.API.ContractKind.RequiresSome;
            }
            throw new NotImplementedException();
        }

        public AST<Microsoft.Formula.API.Nodes.Node> ToTypeDeclAST(TypeDecl decl)
        {
            if (decl is ConDecl)
                return ToConDeclAST((ConDecl)decl);
            else if (decl is EnumDecl)
                return ToEnumDeclAST((EnumDecl)decl);
            else if (decl is UnnDecl)
                return ToUnnDeclAST((UnnDecl)decl);
            throw new UserException("Invalid type declaration", decl);
        }

        public AST<Microsoft.Formula.API.Nodes.Node> ToModuleAST(Module mod)
        {
            if (mod is Model)
                return ToModelAST((Model)mod);
            else if (mod is Domain)
                return ToDomainAST((Domain)mod);
            else if (mod is Transform)
                return ToTransformAST((Transform)mod);
            throw new UserException("Invalid module", mod);
        }
                
        public AST<Microsoft.Formula.API.Nodes.Body> ToBodyAST(Body node)
        {
            var body = Factory.Instance.MkBody(ToSpan(node.Span));
            foreach (var con in node.constraints)
            {
                if (con is Find)
                    body = Factory.Instance.AddConjunct(body, ToFindAST((Find)con));
                else if (con is RelConstr)
                    body = Factory.Instance.AddConjunct(body, ToRelConstrAST((RelConstr)con));
                else
                    throw new UserException("Invalid constraint", node);
            }
            return body;
        }
        public AST<Microsoft.Formula.API.Nodes.Node> ToCnstAST(Cnst node)
        {
            if (node is RationalCnst)
            {
                var cnst = ((RationalCnst)node).Cnst;
                //var rat = new Microsoft.Formula.Common.Rational(cnst.Numerator, cnst.Denominator);
                // TODO: fix, after FORMULA is fixed
                var rat = new Microsoft.Formula.Common.Rational(cnst.Numerator / cnst.Denominator, 1);
                return Factory.Instance.MkCnst(rat, ToSpan(node.Span));
            }
            else if (node is StringCnst)
            {
                var cnst = ((StringCnst)node).Cnst;
                if (cnst.Length>=2 && cnst[0]=='"' && cnst[cnst.Length-1]=='"')
                    return Factory.Instance.MkCnst(cnst.Substring(1,cnst.Length-2), ToSpan(node.Span));
                return Factory.Instance.MkCnst(cnst, ToSpan(node.Span));
            }
            else if (node is EnumCnst)
                return Factory.Instance.MkId(((EnumCnst)node).Cnst, ToSpan(node.Span));
            else if (node is BoolCnst)
                return Factory.Instance.MkId(((BoolCnst)node).ToString(), ToSpan(node.Span)); 
            throw new UserException("Invalid constant", node);
        }
        public AST<Microsoft.Formula.API.Nodes.Compr> ToComprAST(Compr node)
        {
            var comp = Factory.Instance.MkComprehension(ToSpan(node.Span));
            foreach (var x in node.Heads)
            {
                if (x is Id)
                    comp = Factory.Instance.AddHead(comp, ToIdAST((Id)x));
                else if (x is Var)
                    comp = Factory.Instance.AddHead(comp, ToIdAST((Var)x));
                else if (x is BoolCnst)
                    comp = Factory.Instance.AddHead(comp, ToIdAST((BoolCnst)x));
                else
                    comp = Factory.Instance.AddHead(comp, ToFuncTermAST((UserFuncTerm)x));
            }
            foreach (var x in node.Bodies)
                comp = Factory.Instance.AddBody(comp, ToBodyAST(x));
            return comp;
        }
        public AST<Microsoft.Formula.API.Nodes.ContractItem> ToContractItemAST(ContractItem ctr)
        {
            var node = Factory.Instance.MkContract(ToContractKind(ctr.ContractKind), ToSpan(ctr.Span));
            foreach (var x in ctr.specification)
                node = Factory.Instance.AddContractSpec(node, ToBodyAST(x));
            return node;
        }
        public AST<Microsoft.Formula.API.Nodes.Domain> ToDomainAST(Domain dom)
        {
            var node = Factory.Instance.MkDomain(dom.Name, ToComposeKind(dom.ComposeKind), ToSpan(dom.Span));
            foreach (var x in dom.Compositions)
                node = Factory.Instance.AddDomainCompose(node, ToModRefAST(x));
            foreach (var x in dom.Rules)
                if (!x.Heads[0].Name.EndsWith(".conforms"))
                    node = Factory.Instance.AddRule(node, ToRuleAST(x));
            foreach (var x in dom.TypeDecls)
                if (!x.Name.Equals("_") && 
                    !x.Name.EndsWith("._") && 
                    !x.Name.Equals("Data") &&
                    !x.Name.Equals("Constant"))
                    node = Factory.Instance.AddTypeDecl(node, ToTypeDeclAST(x));
            foreach (var x in dom.Conforms)
                node = (AST<Microsoft.Formula.API.Nodes.Domain>)Factory.Instance.AddContract(node, ToContractItemAST(x));
            return node;
        }
        public AST<Microsoft.Formula.API.Nodes.Transform> ToTransformAST(Transform tf)
        {
            var node = Factory.Instance.MkTransform(tf.Name, ToSpan(tf.Span));
            foreach (var x in tf.Inputs)
                node = Factory.Instance.AddTransInput(node, ToParamAST(x));
            foreach (var x in tf.Outputs)
                node = Factory.Instance.AddTransOutput(node, ToParamAST(x));
            foreach (var x in tf.Contracts)
                node = (AST<Microsoft.Formula.API.Nodes.Transform>)Factory.Instance.AddContract(node, ToContractItemAST(x));
            foreach (var x in tf.Rules)
                node = Factory.Instance.AddRule(node, ToRuleAST(x));
            foreach (var x in tf.TypeDecls)
                node = Factory.Instance.AddTypeDecl(node, ToTypeDeclAST(x));
            return node;
        }
        public AST<Microsoft.Formula.API.Nodes.Node> ToMatchAST(Node node)
        {
            if (node is Id)
                return ToIdAST((Id)node);
            else if (node is UserFuncTerm)
                return ToFuncTermAST((UserFuncTerm)node);
            throw new UserException("Invalid match term", node);
        }
        public AST<Microsoft.Formula.API.Nodes.Find> ToFindAST(Find node)
        {
            if (node.Binding != null)
                return Factory.Instance.MkFind(ToIdAST(node.Binding), ToMatchAST(node.Match), ToSpan(node.Span));
            else
                return Factory.Instance.MkFind(null, ToMatchAST(node.Match), ToSpan(node.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Node> ToArgAST(Node node)
        {
            if (node is Id)
                return ToIdAST((Id)node);
            else if (node is Var)
                return ToIdAST((Var)node);
            else if (node is Cnst)
                return ToCnstAST((Cnst)node);
            else if (node is Compr)
                return ToComprAST((Compr)node);
            else if (node is UserFuncTerm)
                return ToFuncTermAST((UserFuncTerm)node);
            else if (node is BuiltinFuncTerm)
                return ToFuncTermAST((BuiltinFuncTerm)node);
            throw new UserException("Invalid argument", node);
        }
        public AST<Microsoft.Formula.API.Nodes.Node> ToComprAST(Node node)
        {
            if (node is Compr)
                return ToComprAST((Compr)node);
            else if (node is UserFuncTerm)
            {
                var func = ToFuncTermAST((UserFuncTerm)node);
                var find = Factory.Instance.MkFind(null, func);
                var body = Factory.Instance.AddConjunct(Factory.Instance.MkBody(), find);
                var compr = Factory.Instance.MkComprehension();
                compr = Factory.Instance.AddHead(compr, Factory.Instance.MkCnst("TRUE"));
                compr = Factory.Instance.AddBody(compr, body);
                return compr;
            }
            throw new UserException("Invalid argument", node);
        }
        public AST<Microsoft.Formula.API.Nodes.FuncTerm> ToFuncTermAST(UserFuncTerm node)
        {
            AST<Microsoft.Formula.API.Nodes.Node>[] argList = new AST<Microsoft.Formula.API.Nodes.Node>[node.args.Count];
            int co = 0;
            foreach (var arg in node.args)
                argList[co++] = ToArgAST(arg);
            return Factory.Instance.MkFuncTerm(ToIdAST(node.Function), ToSpan(node.Span), argList);
        }
        public AST<Microsoft.Formula.API.Nodes.FuncTerm> ToFuncTermAST(BuiltinFuncTerm node)
        {
            AST<Microsoft.Formula.API.Nodes.Node>[] argList = new AST<Microsoft.Formula.API.Nodes.Node>[node.args.Count];            
            int co = 0;
            foreach (var arg in node.args)
                argList[co++] = ToArgAST(arg);
            if (node.Function == OpKind.ToList)
                argList[0] = Factory.Instance.MkId("#" + node.args[0].Name);
            return Factory.Instance.MkFuncTerm(ToOpKind(node.Function), ToSpan(node.Span), argList);
        }
        public AST<Microsoft.Formula.API.Nodes.Id> ToIdAST(Id id)
        {
            return Factory.Instance.MkId(Sanitize(id.Name), ToSpan(id.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Id> ToIdAST(BoolCnst id)
        {
            return Factory.Instance.MkId(id.ToString(), ToSpan(id.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Id> ToIdAST(Var id)
        {
            return Factory.Instance.MkId(Sanitize(id.Name), ToSpan(id.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Model> ToModelAST(Model m)
        {
            var node = Factory.Instance.MkModel(m.Name, m.IsPartial, ToModRefAST(m.Domain), ToComposeKind(m.ComposeKind), ToSpan(m.Span));
            foreach (var x in m.includes)
                node = Factory.Instance.AddModelCompose(node, ToModRefAST(x));
            foreach (var x in m.contracts)
                node = (AST<Microsoft.Formula.API.Nodes.Model>)Factory.Instance.AddContract(node, ToContractItemAST(x));
            foreach (var x in m.facts)
                node = Factory.Instance.AddFact(node, ToModelFactAST(x));
            return node;
        }
        public AST<Microsoft.Formula.API.Nodes.ModelFact> ToModelFactAST(ModelFact mf)
        {
            var node = 
                mf.Binding == null ? 
                    Factory.Instance.MkModelFact(null, ToFuncTermAST(mf.Match), ToSpan(mf.Span)) :
                    Factory.Instance.MkModelFact(ToIdAST(mf.Binding), ToFuncTermAST(mf.Match), ToSpan(mf.Span));
            return node;
        }
        public AST<Microsoft.Formula.API.Nodes.ModRef> ToModRefAST(ModRef m)
        {
            //return Factory.Instance.MkModRef(m.Name, m.Rename, "env://" + m.Location, ToSpan(m.Span));
            return Factory.Instance.MkModRef(m.Name, m.Rename, null, ToSpan(m.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Param> ToParamAST(Param p)
        {
            if (p.Type is Id)
                return Factory.Instance.MkParam(p.Name, ToIdAST((Id)p.Type), ToSpan(p.Span));
            else if (p.Type is Cnst)
                return Factory.Instance.MkParam(p.Name, ToCnstAST((Cnst)p.Type), ToSpan(p.Span));
            else if (p.Type is UserFuncTerm)
                return Factory.Instance.MkParam(p.Name, ToFuncTermAST((UserFuncTerm)p.Type), ToSpan(p.Span));
            else if (p.Type is ModRef)
                return Factory.Instance.MkParam(p.Name, ToModRefAST((ModRef)p.Type), ToSpan(p.Span));
            throw new UserException("Invalid parameter", p);
        }
        public AST<Microsoft.Formula.API.Nodes.Program> ToProgramAST(Program p)
        {
            var node = Factory.Instance.MkProgram(new Microsoft.Formula.API.ProgramName(p.Name.Uri.OriginalString));
            foreach (var x in p.modules)
                node = Factory.Instance.AddModule(node, ToModuleAST(x));
            /*var ConfigAST = Config.ToConfigAST();
            foreach (var x in Config.settings)
                ConfigAST = Factory.Instance.AddSetting(ConfigAST, x.Key.ToIdAST(), x.Value.ToCnstAST());
            node.Node.Config = ConfigAST.Node;*/
            return node;
        }
        /*public AST<Microsoft.Formula.API.Nodes.Range> ToRangeAST(Range r)
        {
            return Factory.Instance.MkRange(r.Lower, r.Upper, ToSpan(r.Span));
        }*/
        public AST<Microsoft.Formula.API.Nodes.RelConstr> ToRelConstrAST(RelConstr r)
        {
            if (r.Op == RelKind.No)
                return Factory.Instance.MkNo((AST<Microsoft.Formula.API.Nodes.Compr>)ToComprAST(r.Arg1), ToSpan(r.Span));
            return Factory.Instance.MkRelConstr(ToRelKind(r.Op), ToArgAST(r.Arg1), ToArgAST(r.Arg2), ToSpan(r.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Rule> ToRuleAST(RuleNode r)
        {
            var node = Factory.Instance.MkRule(ToSpan(r.Span));
            foreach (var x in r.Heads)
                if (x.args.Count == 0)
                    node = Factory.Instance.AddHead(node, ToIdAST(x.Function));
                else
                    node = Factory.Instance.AddHead(node, ToFuncTermAST(x));
            foreach (var x in r.Bodies)
                node = Factory.Instance.AddBody(node, ToBodyAST(x));
            return node;
        }
        public AST<Microsoft.Formula.API.Nodes.Setting> ToSettingAST(Setting s)
        {
            throw new NotImplementedException();
        }
        public AST<Microsoft.Formula.API.Nodes.Field> ToFieldAST(Field f, bool isAny)
        {
            return Factory.Instance.MkField(f.Name, ToIdAST(f.Type), isAny, ToSpan(f.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.Node> ToConDeclAST(ConDecl d)
        {
            if (!d.IsMap)
            {
                var node = Factory.Instance.MkConDecl(Sanitize(d.Name), d.IsNew, ToSpan(d.Span));
                foreach (var x in d.Fields)
                    node = Factory.Instance.AddField(node, ToFieldAST(x, d.IsNew));
                return node;
            }
            else
            {
                var node = Factory.Instance.MkMapDecl(Sanitize(d.Name), ToMapKind(d.Kind), d.IsPartial, ToSpan(d.Span));
                int i = 0;
                foreach (var x in d.Fields)
                {
                    if (i++ < d.CodomainFieldStart)
                        node = Factory.Instance.AddMapDom(node, ToFieldAST(x, false));
                    else
                        node = Factory.Instance.AddMapCod(node, ToFieldAST(x, false));
                }
                return node;
            }
        }
        public AST<Microsoft.Formula.API.Nodes.UnnDecl> ToEnumDeclAST(EnumDecl u)
        {
            var node = Factory.Instance.MkEnum(ToSpan(u.Span));
            foreach (var x in u.Elements)
                node = Factory.Instance.AddElement(node, ToCnstAST(x));
            return Factory.Instance.MkUnnDecl(Sanitize(u.Name), node, ToSpan(u.Span));
        }
        public AST<Microsoft.Formula.API.Nodes.UnnDecl> ToUnnDeclAST(UnnDecl u)
        {
            var node = Factory.Instance.MkUnion(ToSpan(u.Span));
            foreach (var x in u.components)
                node = Factory.Instance.AddUnnCmp(node, ToIdAST(x));
            return Factory.Instance.MkUnnDecl(Sanitize(u.Name), node, ToSpan(u.Span));
        }
    }    
}
