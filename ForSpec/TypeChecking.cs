﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe
{
    using Nodes;
    public class TypeChecking
    {
        public TypeChecking()
        {
        }

        public IEnumerable<ConDecl> GetConDecls()
        {
            return conDecls;
        }

        public IEnumerable<UnnDecl> GetUnnDecls()
        {
            return unnDecls;
        }

        public IEnumerable<ConstantTypeDecl> GetConstDecls()
        {
            return constDecls;
        }

        //public Dictionary<string, HashSet<string>> typeDependencies = new Dictionary<string, HashSet<string>>();
        public Dictionary<TypeDecl, HashSet<TypeDecl>> typeDependencies = new Dictionary<TypeDecl, HashSet<TypeDecl>>();
        //public HashSet<TypeDecl> typeDecls = new HashSet<TypeDecl>();
        public Dictionary<string, TypeDecl> typeDecls = new Dictionary<string, TypeDecl>();
        public HashSet<string> constants = new HashSet<string>();
        List<Flag> flags = new List<Flag>();

        LinkedList<ConDecl> conDecls;
        LinkedList<UnnDecl> unnDecls;
        LinkedList<ConstantTypeDecl> constDecls;

        Module module;
        Dictionary<string, Module> modules;

        /// <summary>
        /// Perform type checking on merged modules.
        /// </summary>
        public void Check(Module module, Dictionary<string, Module> modules)
        {
            this.module = module;
            this.modules = modules;
            //if (module is Transform)
                //throw new UserException("Typechecking requires flattened module (no transformations)", module);
            //if (domain.Compositions.Count > 0 || domain.Imports.Count > 0)
                //throw new UserException("Typechecking requires merged domain (that is, without compositions)", domain);
                        
            // Extract all user defined types and constants
            //ExtractTypes();

            // add 0-ary functions
            /*foreach (var rule in module.Rules)
                foreach (var head in rule.Heads)
                    if (head.args.Count == 0)
                        head.Function.decl = BuiltinTypeDecl.ConstantType;

            foreach (var decl in module.TypeDecls)
                AddTypeInfo(decl,null);*/
            
            // Add type information to rules
            /*foreach (var rule in module.Rules)
            {
                foreach (var head in rule.Heads)
                {
                    AddTypeInfo(head);
                }
                foreach (var body in rule.Bodies)
                    foreach (var cons in body.constraints)
                    {
                        AddTypeInfo(cons);
                    }
            }

            // Add type information to conforms
            foreach (var rule in module.Rules)
            {
                foreach (var head in rule.Heads)
                {
                    AddTypeInfo(head);
                }
                foreach (var body in rule.Bodies)
                    foreach (var cons in body.constraints)
                    {
                        AddTypeInfo(cons);
                    }
            }*/            
            constants.Add("TRUE");
            constants.Add("FALSE");

            AddConstantsInModule(module);
            //AddTypeDeclrsInModule(module);
            ExtractTypes();
            /*AddTypeInfo(module, module, null);
            module.ExpandQualifiedName();*/
            
            if (flags.Count > 0)
                throw new FlagException(flags);
                        
            // check for cycles
            HashSet<TypeDecl> visited = new HashSet<TypeDecl>();
            HashSet<TypeDecl> processed = new HashSet<TypeDecl>();
            foreach (var decl in typeDecls.Values)
            {                
                hasCycle(decl, visited, processed);
            }
            if (flags.Count > 0)
                throw new FlagException(flags);
            
            AddTypeDependenciesInModule(module);

            // Extract type elements, and subtypes
            subElements = new Dictionary<TypeDecl, HashSet<Node>>();
            subTypes = new Dictionary<TypeDecl, HashSet<TypeDecl>>();
            foreach (var decl in typeDecls.Values)
            {
                subElements[decl] = new HashSet<Node>();
                subTypes[decl] = new HashSet<TypeDecl>();
                subElements[decl].Add(decl);
                subTypes[decl].Add(decl);
            }
            subElements[BuiltinTypeDecl.Booleantype].Add(new BoolCnst(false));
            subElements[BuiltinTypeDecl.Booleantype].Add(new BoolCnst(true));
            subElements[BuiltinTypeDecl.StringType].Add(BuiltinTypeDecl.StringType);
            subElements[BuiltinTypeDecl.IntegerType].Add(BuiltinTypeDecl.IntegerType);
            subElements[BuiltinTypeDecl.RealType].Add(BuiltinTypeDecl.RealType);
            subTypes[BuiltinTypeDecl.Booleantype].Add(BuiltinTypeDecl.Booleantype);
            subTypes[BuiltinTypeDecl.StringType].Add(BuiltinTypeDecl.StringType);
            subTypes[BuiltinTypeDecl.IntegerType].Add(BuiltinTypeDecl.IntegerType);
            subTypes[BuiltinTypeDecl.RealType].Add(BuiltinTypeDecl.RealType);
            foreach (var decl in typeDecls.Values)
            {
                AddTypeContainment(decl);
                AddTypeElement(decl);
            }
            parentElements = new Dictionary<Node, HashSet<TypeDecl>>();
            parentTypes = new Dictionary<TypeDecl, HashSet<TypeDecl>>();
            foreach (var kvp in subElements)
                foreach (var e in kvp.Value)
                {
                    if (!parentElements.ContainsKey(e))
                        parentElements[e] = new HashSet<TypeDecl>();
                    parentElements[e].Add(kvp.Key);
                }
            foreach (var kvp in subTypes)
                foreach (var e in kvp.Value)
                {
                    if (!parentTypes.ContainsKey(e))
                        parentTypes[e] = new HashSet<TypeDecl>();
                    parentTypes[e].Add(kvp.Key);
                }

            conDecls = new LinkedList<ConDecl>();
            unnDecls = new LinkedList<UnnDecl>();
            constDecls = new LinkedList<ConstantTypeDecl>();
            foreach (var type in typeDecls.Values)
            {
                if (type is ConDecl)
                    conDecls.AddLast((ConDecl)type);
                else if (type is UnnDecl)
                    unnDecls.AddLast((UnnDecl)type);
                else if (type is ConstantTypeDecl)
                    constDecls.AddLast((ConstantTypeDecl)type);
            }
            
            NodeWelltyped(module);
            if (flags.Count > 0)
                throw new FlagException(flags);
        }

        private void hasCycle(TypeDecl current, HashSet<TypeDecl> visited, HashSet<TypeDecl> processed)
        {            
            visited.Add(current);
            if (current is UnnDecl)
                foreach (var child in ((UnnDecl)current).components)
                {
                    var decl = typeDecls[child.Name];
                    if (visited.Contains(decl) && !processed.Contains(decl))
                        flags.Add(new Flag(SeverityKind.Error, current, "cycle in type declarations", 0));
                    hasCycle(decl, visited, processed);
                }
            processed.Add(current);
        }

        public TypeDecl GetTypeOf(Node node)
        {
            if (node is UserFuncTerm)
            {
                var func = (UserFuncTerm)node;
                if (func.Function.decl == null)
                    if (!typeDecls.TryGetValue(func.Function.Name, out func.Function.decl))
                        throw new UserException(node, "Type {0} undefined", func.Function.Name);
                return func.Function.decl;
            }
            else if (node is Id)
            {
                var id = (Id)node;
                if (id.decl == null)
                    if (!typeDecls.TryGetValue(id.Name, out id.decl))
                        throw new UserException(node, "Type {0} undefined", id.Name);
                return id.decl;
            }
            throw new UserException(node, "Invalid type query for {0}", node.ToString());
        }

        private void AddConstantsInModule(Module module)
        {
            var ecnsts = module.GetNodesOfType<Cnst>();
            constants.UnionWith(ecnsts.Select(x => x.ToString()));
            foreach (var modref in module.ModRefs)
                AddConstantsInModule(modules[modref.Name]);
        }

        private void AddTypeDeclrsInModule(Module module)
        {
            var types = module.GetNodesOfType<TypeDecl>();
            //typeDecls.UnionWith(types);
            foreach (var type in types)
                typeDecls[type.Name] = type;

            foreach (var modref in module.ModRefs)
                AddTypeDeclrsInModule(modules[modref.Name]);
        }

        private void AddTypeDependenciesInModule(Module module)
        {
            // add type dependencies based on the rules
            foreach (var rule in module.Rules)
                foreach (var head in rule.Heads)
                {
                    var uhead = (UserFuncTerm)head;
                    foreach (var body in rule.Bodies)
                    {
                        List<UserFuncTerm> deps = body.GetNodesOfType<UserFuncTerm>();
                        //typeDependencies[head.Name].UnionWith(deps.Select(x => x.Name));
                        typeDependencies[uhead.Function.decl].UnionWith(deps.Select(x => x.Function.decl));
                        
                        List<Find> deps2 = body.GetNodesOfType<Find>();
                        foreach (var find in deps2)
                            if (find.Match is Id)
                                typeDependencies[uhead.Function.decl].Add(((Id)find.Match).decl);
                    }
                }
            foreach (var modref in module.ModRefs)
                AddTypeDependenciesInModule(modules[modref.Name]);
        }

        Dictionary<TypeDecl, HashSet<TypeDecl>> parentTypes = new Dictionary<TypeDecl, HashSet<TypeDecl>>();
        Dictionary<Node, HashSet<TypeDecl>> parentElements = new Dictionary<Node, HashSet<TypeDecl>>();
        Dictionary<TypeDecl, HashSet<TypeDecl>> subTypes = new Dictionary<TypeDecl, HashSet<TypeDecl>>();
        Dictionary<TypeDecl, HashSet<Node>> subElements = new Dictionary<TypeDecl, HashSet<Node>>();
        private void AddTypeContainment(TypeDecl node)
        {
            if (node is UnnDecl)
            {
                var unn = (UnnDecl)node;
                var decls = subTypes[node];
                foreach (var comp in unn.components)
                {
                    var decl = typeDecls[comp.Name];
                    AddTypeContainment(decl);
                    decls.UnionWith(subTypes[decl]);
                    decls.Add(decl);
                }
            }
        }
        private void AddTypeElement(TypeDecl node)
        {
            if (node is UnnDecl)
            {
                var unn = (UnnDecl)node;
                var decls = subElements[node];
                foreach (var comp in unn.components)
                {
                    var decl = typeDecls[comp.Name];
                    AddTypeElement(decl);
                    decls.UnionWith(subElements[decl]);
                }
            }
            else if (node is EnumDecl)
            {
                var e = (EnumDecl)node;
                var decls = subElements[node];
                decls.UnionWith(e.Elements);
            }
        }

        private void ExtractTypes()
        {
            foreach (var typedecl in module.TypeDecls)
            {
                if (!typeDecls.ContainsKey(typedecl.Name))
                {
                    typeDecls[typedecl.Name] = typedecl;

                    if (typedecl is EnumDecl)
                    {
                        var e = (EnumDecl)typedecl;
                        foreach (var c in e.Elements)
                            typeDecls[c.Name] = e;
                    }
                    typeDependencies[typedecl] = new HashSet<TypeDecl>();
                }
            }
            // add 0-ary functions
            foreach (var rule in module.Rules)
                foreach (var head in rule.Heads)
                    if (head.args.Count == 0 && !typeDecls.ContainsKey(head.Name))
                    {
                        head.Function.decl = new ConstantTypeDecl(head.Name);
                        typeDecls[head.Name] = head.Function.decl; 
                        typeDependencies[head.Function.decl] = new HashSet<TypeDecl>();
                    }
            typeDecls.Add("String", BuiltinTypeDecl.StringType);
            typeDecls.Add("Integer", BuiltinTypeDecl.IntegerType);
            typeDecls.Add("Real", BuiltinTypeDecl.RealType);
            typeDecls.Add("Boolean", BuiltinTypeDecl.Booleantype);
            var ids = module.GetNodesOfType<Id>();
            foreach (var id in ids)
            {
                if (!typeDecls.ContainsKey(id.Name))
                    flags.Add(new Flag(SeverityKind.Error, id, Constants.UndefinedSymbol.ToString("identifier", id.Name), 0));
                else
                    id.decl = typeDecls[id.Name];
            }
            var funcs = module.GetNodesOfType<UserFuncTerm>();
            foreach (var func in funcs)
            {
                if (!typeDecls.ContainsKey(func.Function.Name))
                    flags.Add(new Flag(SeverityKind.Error, func, Constants.UndefinedSymbol.ToString("term", func.Name), 0));
                else
                    func.Function.decl = typeDecls[func.Function.Name];
            }
        }

        private void AddTypeInfo(Node node, Module module, string parent_namespace)
        {
            var prefix = (parent_namespace != null) ? (parent_namespace+".") : "";
            if (node is Id)
            {
                var id = (Id)node;                
                var name = id.Name.Contains('.') ? id.Name : (prefix + id.Name);
                                
                if (name == "String") { id.decl = BuiltinTypeDecl.StringType; return; }
                if (name == "Integer") { id.decl = BuiltinTypeDecl.IntegerType; return; }
                if (name == "Real") { id.decl = BuiltinTypeDecl.RealType; return; }
                if (name == "Boolean") { id.decl = BuiltinTypeDecl.Booleantype; return; }
                if (name.Contains('.'))
                    LookupDirectTypeInfo(name, module, "", out id.decl, out id.context);
                else
                    LookupTypeInfo(name, module, "", out id.decl, out id.context);
                if (id.decl == null)
                    flags.Add(new Flag(SeverityKind.Error, node, String.Format("Missing type {0}", id.Name), 0, null));
                else
                    typeDependencies[id.decl] = new HashSet<TypeDecl>();
            }
            if (node is UserFuncTerm)
            {
                var func = (UserFuncTerm)node;
                AddTypeInfo(func.Function, module, parent_namespace);
                var ipos = func.Function.Name.LastIndexOf('.');
                var ns = (ipos == -1) ? parent_namespace : (prefix + func.Function.Name.Substring(0, ipos));
                foreach (var child in node.Children)
                    AddTypeInfo(child, module, ns);
                return;
            }
            foreach (var child in node.Children)
                AddTypeInfo(child, module, parent_namespace);

            // if node is module, add types for its subcomponents
            var mod = node as Module;
            if (mod != null)
                foreach (var modref in mod.ModRefs)
                {
                    var nextModule = modules[modref.Name];
                    AddTypeInfo(nextModule, nextModule, parent_namespace);
                }
        }

        private TypeDecl FindType(string typename, Module module)
        {
            var decl = module.TypeDecls.Find(x => x.Name == typename);
            if (decl != null) return decl;

            foreach (var rule in module.Rules)
                foreach (var head in rule.Heads)
                    if (head.args.Count == 0 && head.Function.Name == typename)
                        return new ConstantTypeDecl(head.Name);
            return null;
        }

        private void LookupDirectTypeInfo(string typename, Module module, string accesspath, out TypeDecl decl, out string context)
        {
            var prefix = String.IsNullOrEmpty(accesspath) ? "" : (accesspath + ".");
            var ipos = typename.IndexOf('.');
            if (ipos == -1)
            {
                LookupTypeInfo(typename, module, accesspath, out decl, out context);
                return;
            }          
            
            string modname = typename.Substring(0,ipos);
            string restTypename = typename.Substring(ipos+1);
            string nextAccesspath = prefix + modname;
            if (module is Domain && ((Domain)module).IsModel)
                nextAccesspath = accesspath;
            foreach (var modref in module.ModRefs)
                if (modref.Rename == modname)
                {
                    LookupDirectTypeInfo(restTypename, modules[modref.Name], nextAccesspath, out decl, out context);
                    return;
                }
            foreach (var modref in module.ModRefs)
                if (modref.Name == modname)
                {
                    LookupDirectTypeInfo(restTypename, modules[modref.Name], nextAccesspath, out decl, out context);
                    return;
                }
            // If not found yet, look systematically in every submodule
            decl = null;
            context = null;
            foreach (var modref in module.ModRefs)
            {
                TypeDecl newdecl;
                LookupDirectTypeInfo(restTypename, modules[modref.Name], nextAccesspath, out newdecl, out context);
                if (decl != null && newdecl != null && !decl.Equals(newdecl))
                {
                    flags.Add(new Flag(SeverityKind.Error, module, String.Format("Ambiguous type {0}", typename), 0));
                }
                else if (decl == null) decl = newdecl;      
            }
            context = (decl != null) ? accesspath : null;
        }

        private void LookupTypeInfo(string typename, Module module, string accesspath, out TypeDecl decl, out string context)
        {
            var prefix = String.IsNullOrEmpty(accesspath) ? "" : (accesspath + ".");
            decl = FindType(typename, module);
            if (decl != null)
            {
                context = accesspath;
                return;
            }
            context = null;

            foreach (var comp in module.ModRefs)            
            {
                TypeDecl newdecl;
                var nextAccesspath = prefix + comp.Name;
                if (module is Domain && ((Domain)module).IsModel)
                    nextAccesspath = accesspath;
                LookupTypeInfo(typename, modules[comp.Name], nextAccesspath, out newdecl, out context);
                if (decl != null && newdecl != null && !decl.Equals(newdecl))
                {
                    flags.Add(new Flag(SeverityKind.Error, module, String.Format("Ambiguous type {0}", typename), 0));
                }
                else if (decl == null) decl = newdecl;                
            }
        }

        private bool CheckType(Id typeName, Node node)
        {
            if (typeName.Name == "String" ||
                typeName.Name == "Boolean" ||
                typeName.Name == "Real" ||
                typeName.Name == "Integer")
                return true;
            //if (!typeDecls.ContainsKey(typeName))
            if (typeName.decl == null)
            {
                flags.Add(new Flag(SeverityKind.Error, node, String.Format("Undefined type {0}", typeName), 0));
                return false;
            }
            return true;
        }
                
        public bool isSubtype(TypeDecl type, TypeDecl ancestor)
        {
            HashSet<TypeDecl> typeDecls;
            return parentTypes.TryGetValue(type, out typeDecls) && typeDecls.Contains(ancestor);
        }

        private bool isSubelement(Node elem, TypeDecl ancestor)
        {
            HashSet<TypeDecl> typeDecls;
            return parentElements.TryGetValue(elem, out typeDecls) && typeDecls.Contains(ancestor);
        }

        public bool IsEnumElement(Cnst arg, TypeDecl decl)
        {
            return 
                (arg is RationalCnst && (isSubelement(BuiltinTypeDecl.IntegerType, decl) || isSubelement(BuiltinTypeDecl.RealType, decl))) ||
                (arg is StringCnst && (isSubelement(BuiltinTypeDecl.StringType, decl) || isSubelement(new EnumCnst(arg.Name), decl))) ||
                (arg is BoolCnst && isSubelement(arg, decl)) ||
                (arg is EnumCnst && isSubelement(arg, decl));
        }

        public bool NodeWelltyped(Node node)
        {
            bool valid = true;
            if (node is UserFuncTerm)
            {
                var func = (UserFuncTerm)node;
                if (!CheckType(func.Function, func))
                    return false;
                // 0-ary functerm (constant)
                if (func.args.Count == 0)
                    return true;
                /*var decl = typeDecls[func.Function.Name] as ConDecl;
                if (decl == null)
                {
                    flags.Add(new Flag(SeverityKind.Error, node, String.Format("Invalid type for {0}", func), 0));
                    return false;
                }*/
                var decl = (ConDecl)func.Function.decl;
                if (func.args.Count != decl.Fields.Count)
                {
                    flags.Add(new Flag(SeverityKind.Error, node, String.Format("Invalid number of arguments in {0}", func), 0));
                    return false;
                }
                for (int i=0; i<func.args.Count; ++i)
                {
                    var arg = func.args[i];
                    // if 
                    if (arg is Cnst && !IsEnumElement((Cnst)arg, decl.Fields[i].Type.decl))
                    {
                        flags.Add(new Flag(SeverityKind.Error, node, String.Format("Argument {0} is invalid for {1}", i+1, func), 0));
                        return false;
                    }
                    else if (arg is UserFuncTerm)
                    {
                        var farg = (UserFuncTerm)func.args[i];

                        if (farg.Function.decl == null)
                            farg.Function.decl = typeDecls[farg.Function.Name];
                        if (!isSubtype(farg.Function.decl, decl.Fields[i].Type.decl))
                        {
                            flags.Add(new Flag(SeverityKind.Error, node, String.Format("Argument {0} is invalid for {1}", i + 1, func), 0));
                            return false;
                        }
                    }
                }
            }
            else if (node is Field)
            {
                var field = (Field)node;
                if (!CheckType(field.Type, field))
                    return false;
            }
            else if (node is UnnDecl)
            {
                var decl = (UnnDecl)node;
                foreach (var arg in decl.components)
                    valid &= CheckType(arg, arg);
            }
            foreach (var child in node.Children)
                valid &= NodeWelltyped(child);
            return valid;
        }

        public void GetVariables(Node node, ref HashSet<Id> vars)
        {
            if (node == null) return;
            if (node is Id)
            {
                vars.Add((Id)node);
            }
            foreach (var child in node.Children)
                GetVariables(child, ref vars);
        }

        public void GetDefinedVariables(Node node, ref HashSet<Id> vars)
        {
            if (node == null) return;
            if (node is Id)
            {
                vars.Add((Id)node);
            }
            if (node is RelConstr && ((RelConstr)node).Op == RelKind.No)
                return;
            foreach (var child in node.Children)
                GetDefinedVariables(child, ref vars);
        }
    }
}
