﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe
{
    using System.Diagnostics;
    using Nodes;
        
    class Execution : IGraph<TypeDecl>
    {
        public IEnumerable<TypeDecl> GetVertices()
        {
            return typeChecker.typeDependencies.Keys;
        }

        public IEnumerable<TypeDecl> GetNeighbors(TypeDecl vertex)
        {
            return typeChecker.typeDependencies[vertex];
        }

        //Dictionary<string, TypeDecl> typeDecls = new Dictionary<string, TypeDecl>();

        Dictionary<string, HashSet<FuncTerm>> typeRealizations = new Dictionary<string, HashSet<FuncTerm>>();
        Dictionary<string, HashSet<FuncTerm>> add_to_typeRealizations = new Dictionary<string, HashSet<FuncTerm>>();
        Dictionary<string, List<RuleNode>> typeDependentRules = new Dictionary<string, List<RuleNode>>();
        public TypeChecking typeChecker 
        {
            get;
            private set;
        }
        HashSet<string> constants = new HashSet<string>();

        class EvalRule
        {
            public EvalRule(RuleNode rule, string fact, string sort) { this.rule = rule; this.fact = fact; this.sort = sort; }
            public RuleNode rule;
            public string fact;
            public string sort;
        }

        Module module;
        Dictionary<string, Module> modules;
        int calculatedLevel;
        HashSet<UserFuncTerm> facts = new HashSet<UserFuncTerm>();
        HashSet<UserFuncTerm>[] factsOnLevel;
        Dictionary<TypeDecl, HashSet<UserFuncTerm>> factsOfType;
        Dictionary<TypeDecl, int> typeLevel;

        private void AddFactsFromModule(Module module, string ns)
        {
            var nsprefix = ns + ".";
            foreach (var rule in module.Rules)
                if (rule.Bodies.Count == 0)
                    foreach (var head in rule.Heads)
                    {
                        var h = (UserFuncTerm)head.Clone();
                        if (!String.IsNullOrEmpty(ns))
                            h.Name = nsprefix + h.Name;
                        int level = typeLevel[h.Function.decl];
                        factsOnLevel[level].Add(h);
                        factsOfType[h.Function.decl].Add(h);
                        facts.Add(h);
                    }
            foreach (var modref in module.ModRefs)
            {
                var localns = String.IsNullOrEmpty(modref.Rename) ? modref.Name : modref.Rename;
                var globalns = String.IsNullOrEmpty(ns) ? localns : (nsprefix + localns);
                AddFactsFromModule(modules[modref.Name], globalns);
            }
        }

        public Model Execute (Module module, Dictionary<string, Module> modules)
        {
            this.module = module;
            this.modules = modules;
            typeChecker = new TypeChecking();
            typeChecker.Check(module, modules);
            Rete rete = new Rete(this);
            rete.Initialize();
            //return;
            Console.WriteLine("Rete reused {0} nodes", rete.statReuse);
                        
            // Run Rete algorithm level-by-level
            Tarjan<TypeDecl> tarjan = new Tarjan<TypeDecl>();
            tarjan.SetGraph(this);
            typeLevel = new Dictionary<TypeDecl, int>();
            for (int i = 0; i < tarjan.Count; ++i)
                foreach (var type in tarjan[i])
                    typeLevel[type] = i;
            
            // Create facts storages
            factsOnLevel = new HashSet<UserFuncTerm>[tarjan.Count];
            for (int i = 0; i < factsOnLevel.Length; ++i)
                factsOnLevel[i] = new HashSet<UserFuncTerm>();
            factsOfType = new Dictionary<TypeDecl, HashSet<UserFuncTerm>>();
            foreach (var type in typeLevel.Keys)
                factsOfType[type] = new HashSet<UserFuncTerm>();

            // Add facts
            AddFactsFromModule(module, null);
                        
            calculatedLevel = -1;
            int numExecutedRules = 0;
            for (int i = 0; i < tarjan.Count; ++i)
            {
                //Console.WriteLine(new String('-',20));
                Console.Write("\r");
                Console.Write(new String(' ', 40));
                Console.Write("\r{3}% done. Level {0}/{1} with {2:5} elements", i, tarjan.Count, tarjan[i].Count(), 100*(i+1) / tarjan.Count);
                //Console.WriteLine(String.Join("\n", tarjan[i]));

                Log.WriteLine(10,"-----------------");
                Log.WriteLine(10,String.Join("\n", tarjan[i]));

                Log.WriteLine(10,"Input facts:");
                Queue<UserFuncTerm> Q = new Queue<UserFuncTerm>();
                foreach (var fact in factsOnLevel[i])
                {
                    Log.WriteLine(10,"\t{0}",fact);
                    Q.Enqueue(fact);
                }
                
                while (Q.Count > 0)
                {
                    //Console.WriteLine("Queue length: {0}", Q.Count);
                    var e = Q.Dequeue();
                    //Console.WriteLine("Use fact: {0}", e);
                    Log.WriteLine(10,"Use fact: {0}", e);

                    if (e.ToString().EndsWith(@"out1.DAE.addend(out1.DAE.summa(""CyPhyML_powerflow"", 388), out1.DAE.cvar(""CyPhyML_flow"", 388)))"))
                    {
                        int k = 2;
                    }
                 
                    var newfacts = rete.AddFact(e, e.Function.decl);
                    //Console.WriteLine("Add facts:");
                    foreach (var fact in newfacts)
                    {
                        numExecutedRules++;
                        UserFuncTerm term = (UserFuncTerm)fact;
                        bool success = facts.Add(term);
                        if (success)
                        {
                            Log.WriteLine(10, "\tAdd fact:{0}", term);
                            int level = typeLevel[term.Function.decl];
                            if (level == i && success)
                            {
                                Q.Enqueue(term);
                            }
                            factsOnLevel[level].Add(term);
                            factsOfType[term.Function.decl].Add(term);
                        }
                    }
                }

                // Trigger nodes
                Log.WriteLine(10, "Triggering");
                calculatedLevel = i+1;
                bool addedFact = false;
                foreach (var type in tarjan[i])
                {
                    var triggeredfacts = rete.Trigger(type);
                    foreach (var fact in triggeredfacts)
                    {
                        numExecutedRules++;
                        UserFuncTerm term = (UserFuncTerm)fact;
                        bool success = facts.Add(term);
                        if (success)
                        {
                            Log.WriteLine(10, "\tAdd fact:{0}", term);
                            int level = typeLevel[term.Function.decl];
                            if (level == i && success)
                            {
                                Q.Enqueue(term);
                                addedFact = true;
                            }
                            factsOnLevel[level].Add(term);
                            factsOfType[term.Function.decl].Add(term);                            
                        }
                    }
                }
                // If added new element, repeat this level.
                if (addedFact)
                {
                    Log.WriteLine(10, "Repeat level");
                    --i;
                }
            }
            Console.WriteLine();
            Console.WriteLine(new String('=', 10));
            Console.WriteLine("{0} rules executed.", numExecutedRules);
            Console.WriteLine("{0} facts.", facts.Count);
            //var fs = facts.Select(x => x.ToString()).OrderBy(x => x);
            //fs.All(x => { if (!x.StartsWith("in2.") && !x.StartsWith("out.")) Console.WriteLine(x); return true; });
            //fs.All(x => { Console.WriteLine(x); return true; });
            Model result = new Model(module.Name, false);
            result.Domain = new ModRef(module.Name, null, null);
            foreach (var fact in facts)
            {
                var rule = new RuleNode();
                rule.AddHead(fact);
                result.AddRule(rule);
            }
            return result;
        }
        
        /// <summary>
        /// Returns true if term 'match' can match term 'fact' by applying some substitutions to its variables.
        /// Fact must be grounded.
        /// </summary>
        /// <param name="match"></param>
        /// <param name="fact"></param>
        /// <param name="substitutions"></param>
        /// <returns></returns>
        public bool Match(Node match, Node fact, ref Dictionary<Var, Node> substitutions)
        {            
            // Return substitutions, if match is variable
            if (match is Var)
            {
                if (match.Name != "_")
                {
                    var v = (Var)match;
                    if (!substitutions.ContainsKey(v))
                    {
                        substitutions[v] = fact;
                        return true;
                    }
                    if (!substitutions[v].Equals(fact))
                        return false;
                }
                return true;
            }
            else if (match is Cnst)
            {
                if (fact is Cnst)
                    return match.Equals(fact);
                return false;
            }
            else if (match is Find && fact is UserFuncTerm)
            {
                Find find = (Find)match;
                var func = (UserFuncTerm)fact;
                bool matching = false;
                if (find.Match is Id)
                {
                    if (typeChecker.isSubtype(func.Function.decl, typeChecker.typeDecls[find.Match.Name]))
                        matching = true;
                }
                else if (find.Match is UserFuncTerm)
                {
                    var matchfunc = (UserFuncTerm)find.Match;
                    matching = Match(matchfunc, fact, ref substitutions);
                }

                if (matching && find.Binding != null)
                {
                    var v = find.Binding;
                    if (!substitutions.ContainsKey(v))
                    {
                        substitutions[v] = fact;
                        return true;
                    }
                    if (!substitutions[v].Equals(fact))
                        return false;
                }
                return matching;
            }
            else if (match is UserFuncTerm && fact is UserFuncTerm)
            {
                var funcmatch = (UserFuncTerm)match;
                var funcfact = (UserFuncTerm)fact;
                if (match.Name == fact.Name && funcmatch.args.Count == funcfact.args.Count)
                {
                    for (int i = 0; i < funcfact.args.Count; ++i)
                        if (!Match(funcmatch.args[i], funcfact.args[i], ref substitutions))
                            return false;
                    return true;
                }
                return false;
            }
            return false;
        }               

        public Node FollowAccessors(Node ground_term, IEnumerable<string> accessors)
        {
            foreach (var acc in accessors)
            {
                if (ground_term is UserFuncTerm)
                {
                    var decl = typeChecker.typeDecls[ground_term.Name];
                    if (decl is ConDecl)
                    {
                        var cdecl = (ConDecl)decl;
                        int ipos = cdecl.Fields.FindIndex(x => x.Name.Equals(acc));
                        if (ipos != -1)
                        {
                            ground_term = ((UserFuncTerm)ground_term).args[ipos];
                            continue;
                        }
                    }
                }
                throw new UserException(String.Format("Runtime error: invalid accessor {0} for term {1}", acc, ground_term), ground_term);
            }
            return ground_term;
        }        

        /*public MyToken UpdateToken(MyToken token, Node pattern)
        {
            MyToken mtoken = (MyToken)token;
            Dictionary<Var, Node> substitutions = new Dictionary<Var, Node>();
            if (pattern is RelConstr)
            {
                var rel = (RelConstr)pattern;
                
                var substRel = (RelConstr)rel.Clone();
                substRel.ReplaceVarsInPlace(token.paramValues, null, typeChecker);
                var arg1 = SimplifyTerm(substRel.Arg1);
                var arg2 = SimplifyTerm(substRel.Arg2);

                /*foreach (var kvp in mtoken.paramValues)
                {
                    arg2 = arg2.Substitute(kvp.Key, kvp.Value);
                }*/
                /*if (rel.Op == RelKind.Eq && rel.Arg1 is Var && arg2.isGround())
                {
                    substitutions.Add((Var)rel.Arg1, arg2);
                }
                else
                {*/
                    /*var arg1 = rel.Arg1;
                    foreach (var kvp in mtoken.paramValues)
                        arg1 = arg1.Substitute(kvp.Key, kvp.Value);*/
      /*              if (!arg2.isGround())
                    {
                        var tmp = arg2; arg2 = arg1; arg1 = tmp;
                    }
                    if (!arg2.isGround() || !Match(arg1, arg2, ref substitutions))
                    {
                        Log.WriteLine(8, "Pattern not matching with parameter value {0}", pattern.ToString());
                        return null;
                    }
                //}
            }
            else
            {
                if (!Match((Node)pattern, mtoken.expr, ref substitutions))
                {
                    Log.WriteLine(8, "Pattern not matching: {0} vs. {1}", pattern.ToString(), mtoken.expr);
                    return null;
                }
            }
            MyToken result = new MyToken(mtoken.type, mtoken.expr);
            foreach (var kvp in mtoken.paramValues)
                result.paramValues.Add(kvp.Key, kvp.Value);

            foreach (var s in substitutions)
            {
                if (mtoken.paramValues.ContainsKey(s.Key) &&
                    mtoken.paramValues[s.Key] != s.Value)
                {
                    Log.WriteLine(8, "Variable mismatch");
                    return null;
                }
                result.paramValues.Add(s.Key, s.Value);
            }
            return result;
        }*/

        public Node SimplifyTerm(Node term)
        {
            var func = term as BuiltinFuncTerm;
            // ToList
            if (func != null && func.Function == OpKind.ToList)
            {
                // Check if the fact is already calculated
                UserFuncTerm compr = (UserFuncTerm)func.args[2];
                int level = typeLevel[compr.Function.decl];
                if (level <= calculatedLevel)
                {
                    string consName = func.args[0].Name;
                    Node baseElem = func.args[1];
                    Node buildTerm = baseElem;
                    int comprehensionArgsNum = Int32.Parse(compr.Name.Substring(compr.Name.LastIndexOf('#') + 1));

                    Dictionary<Var,Node> substitutions = new Dictionary<Var,Node>();
                    foreach (var fact in factsOfType[compr.Function.decl].OrderByDescending(x => x.ToString()))
                    {
                        substitutions.Clear();
                        if (!Match(compr, fact, ref substitutions))
                            continue;
                        var cons = new UserFuncTerm(new Id(consName));
                        cons.Function.context = "";
                        cons.Function.decl = typeChecker.typeDecls[consName];
                        for (int i = 0; i < comprehensionArgsNum; ++i)
                            cons.AddArg(fact.args[i]);
                        cons.AddArg(buildTerm);
                        buildTerm = cons;
                    }
                    return buildTerm;
                }
                throw new Exception("internal error");
            }
            else if (func != null && func.Function == OpKind.Count)
            {
                // Check if the fact is already calculated
                UserFuncTerm compr = (UserFuncTerm)func.args[0];
                int level = typeLevel[compr.Function.decl];
                if (level <= calculatedLevel)
                {
                    int count = 0;
                    Dictionary<Var, Node> substitutions = new Dictionary<Var, Node>();
                    foreach (var fact in factsOfType[compr.Function.decl].OrderByDescending(x => x.ToString()))
                    {
                        substitutions.Clear();
                        if (!Match(compr, fact, ref substitutions))
                            continue;
                        count++;
                    }
                    return new RationalCnst(new Rational(count));
                }
                throw new Exception("internal error");
            }
            return term.Simplify(typeChecker);
        }

        public bool isConstraintSatisfied(RelConstr constraint, Dictionary<Var,Node> substitutions)
        {
            RelConstr node = (RelConstr)constraint.Clone();
            node.ReplaceVarsInPlace(substitutions, null, typeChecker);
            if (node.Arg1 is BuiltinFuncTerm)
                node.Arg1 = SimplifyTerm(node.Arg1);
            if (node.Arg2 is BuiltinFuncTerm)
                node.Arg2 = SimplifyTerm(node.Arg2);
            var rel = (RelConstr)node;
            bool res = false;
            if (rel.Op == RelKind.Typ)
            {
                var id = (Id)rel.Arg2;
                if (rel.Arg1 is UserFuncTerm)
                    return typeChecker.isSubtype(((UserFuncTerm)rel.Arg1).Function.decl, id.decl);
                else if (rel.Arg1 is Cnst)
                    return typeChecker.IsEnumElement((Cnst)rel.Arg1, id.decl);                
                Debug.Assert(false);
            }
            else if (rel.Op == RelKind.No)
            {
                // Check if the fact is already calculated
                var decl = ((UserFuncTerm)rel.Arg1).Function.decl;
                int level = typeLevel[decl];
                if (level <= calculatedLevel)
                {
                    Dictionary<Var, Node> subst = new Dictionary<Var, Node>();
                    foreach (var fact in factsOfType[decl])
                    {
                        subst.Clear();
                        if (Match(rel.Arg1, fact, ref subst))
                            return false;
                    }
                    res = true;
                }
                else
                    throw new Exception("internal error");
            }
            else
                res = Node.EvaluateBuiltinRelation(rel, typeChecker);
            //if (res)
            //    Console.WriteLine("\tConstraint {0} satisfied", node);
            //else
            //    Console.WriteLine("\tConstraint {0} not satisfied", node);
            //Console.Write("\t");
            //Console.WriteLine(token.ToString().Replace("\n","\n\t"));
            return res;
        }

        public void GetVariables(Node node, ref HashSet<Id> vars)
        {
            if (node == null) return;
            if (node is Id)
            {
                if (node.Name != "_")
                    vars.Add((Id)node);
            }
            foreach (var child in node.Children)
                GetVariables(child, ref vars);
        }

        private string TrimAfterDot(string text)
        {
            var ipos = text.IndexOf('.');
            if (ipos >= 0) return text.Substring(0, ipos);
            return text;
        }

        HashSet<string> getConstraintVariables(object constraint)
        {
            //RelConstr cons = (RelConstr)constraint;
            HashSet<Var> vars;
            // If constraint is equality, and left side is variable, then that variable is not returned.
            //if (cons.Op == RelKind.Eq && cons.Arg1 is Var)
                //vars = cons.Arg2.GetVariables();
            //else
                vars = ((Node)constraint).GetVariables();
            return new HashSet<string>(vars.Select(x => TrimAfterDot(x.Name)));
        }

        HashSet<string> getPatternVariables(object pattern)
        {
            var vars = ((Node)pattern).GetVariables();
            return new HashSet<string>(vars.Select(x => TrimAfterDot(x.Name)));
        }


        public IEnumerable<TypeDecl> GetTypes()
        {
            return new List<TypeDecl>(typeChecker.typeDecls.Values.Where(x => x is ConDecl));
        }

        public IEnumerable<TypeDecl> GetUnionTypes()
        {
            return new List<TypeDecl>(typeChecker.typeDecls.Values.Where(x => x is UnnDecl));
        }

        public IEnumerable<TypeDecl> GetSubtypes(TypeDecl type)
        {
            var unn = (UnnDecl )type;
            return unn.components.Select(x => x.decl);
        }

        private IEnumerable<RuleNode> GetRulesOfModule(Module module, string ns)
        {
            var nsprefix = String.IsNullOrEmpty(ns) ? "" : (ns + ".");
            foreach (var rule in module.Rules)
            {
                var r = (RuleNode)rule.Clone();
                r.AppendNamespace(nsprefix);
                yield return r;
            }
            foreach (var modref in module.ModRefs)
            {
                var localns = String.IsNullOrEmpty(modref.Rename) ? modref.Name : modref.Rename;
                string globalns = nsprefix + localns;
                if (module is Domain && ((Domain)module).IsModel)
                    globalns = ns;
                var rules = GetRulesOfModule(modules[modref.Name], globalns);
                foreach (var rule in rules)
                    yield return rule;
            }
        }

        public IEnumerable<RuleNode> GetRules()
        {
            var rules = GetRulesOfModule(module, null);
            return rules;
        }

        public IEnumerable<object> GetBodies(object rule)
        {
            var r = (RuleNode)rule;
            return r.Bodies;
        }

        private bool IsFindOrAssignment(Node node)
        {
            if (node is Find) return true;
            if (node is RelConstr)
            {
                RelConstr rel = (RelConstr)node;
                if (rel.Op == RelKind.Eq && rel.Arg1 is Var)
                    return true;
            }
            return false;
        }

        public IEnumerable<object> GetPatterns(object body)
        {
            var r = (Body)body;
            return r.constraints.Where(x => x is Find);
        }

        private static bool IsTriggeredConstraint(RelConstr rel)
        {
            if (rel.Op == RelKind.No)
                return true;
            return false;
        }

        private static bool IsTriggeredElement(RelConstr rel)
        {
            if (IsTriggeredConstraint(rel))
                return true;
            if (rel.Op == RelKind.Eq)
            {
                var bfunc = rel.Arg2 as BuiltinFuncTerm;
                if (bfunc != null && bfunc.Function == OpKind.ToList)
                    return true;
            }
            return false;
        }

        public IEnumerable<object> GetConstraints(object body)
        {
            var r = (Body)body;
            return new List<object>(r.constraints.Where(x => x is RelConstr && !IsTriggeredConstraint((RelConstr)x)));
        }

        public bool IsTriggerable(object body)
        {
            return IsTriggeredElement((RelConstr)body);
        }

        public IEnumerable<object> GetTriggeredConstraints(object body)
        {
            var r = (Body)body;
            return new List<object>(r.constraints.Where(x => x is RelConstr && IsTriggeredConstraint((RelConstr)x)));
        }

        public IEnumerable<object> GetHeads(object rule)
        {
            var r = (RuleNode)rule;
            return r.Heads;
        }

        public TypeDecl GetPatternType(object type)
        {
            if (type is Find)
            {
                var find = (Find)type;
                if (find.Match is UserFuncTerm)
                    return ((UserFuncTerm)find.Match).Function.decl;
                if (find.Match is Id)
                    return ((Id)find.Match).decl;
            }
            if (type is UserFuncTerm)
                return ((UserFuncTerm)type).Function.decl;
            if (type is RelConstr)
            {
                RelConstr rel = (RelConstr)type;
                if (rel.Arg2 is UserFuncTerm)
                    return GetPatternType(rel.Arg2);
                return null;
            }
            //return ((Node)type).Name;
            throw new NotImplementedException();
        }
    }
}
