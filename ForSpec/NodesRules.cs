﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe.Nodes
{
    public class RuleNode : Node
    {
        public RuleNode() { }
        public RuleNode(IEnumerable<UserFuncTerm> heads, IEnumerable<Body> bodies)
        {
            if (heads != null)
                foreach (var h in heads)
                    this.Heads.Add(h);
            if (bodies != null)
                foreach (var b in bodies)
                    this.Bodies.Add(b);
        }
        public void AddHead(UserFuncTerm head, bool Add = true) { if (Add) Heads.Add(head); else Heads.Insert(0, head); }
        public void AddBody(Body body, bool Add = true) { if (Add) Bodies.Add(body); else Bodies.Insert(0, body); }
        public List<UserFuncTerm> Heads = new List<UserFuncTerm>();
        public List<Body> Bodies = new List<Body>();
        public override IEnumerable<Node> Children
        {
            get
            {
                foreach (var n in Heads) yield return n;
                foreach (var n in Bodies) yield return n;
            }
        }
        public override string ToString()
        {
            var h = String.Join(",\n", Heads.Select(x => x.ToString()));
            if (Bodies.Count == 0)
                return h + ".";
            var b = String.Join(";\n  ", Bodies.Select(x => x.ToString()));
            return String.Format("{0}:-\n  {1}.", h, b);
        }
        public override object Clone() 
        {
            var res = new RuleNode() { Span = Span };
            foreach (var h in Heads)
                res.Heads.Add((UserFuncTerm)h.Clone());
            foreach (var b in Bodies)
                res.Bodies.Add((Body)b.Clone());
            return res;
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;
            var res = new RuleNode() { Span = Span };
            foreach (var h in Heads)
                res.Heads.Add((UserFuncTerm)h.Substitute(match, substitution));
            foreach (var b in Bodies)
                res.Bodies.Add((Body)b.Substitute(match, substitution));
            return res;
        }
    }

    public class Body : Node, IEquatable<Body>
    {
        public Body() { }
        public Body(Node constraint)
        {
            if (constraint != null)
                this.constraints.Add(constraint);
        }
        public Body(NodeList<Node> constraints)
        {
            if (constraints != null)
                foreach (var c in constraints)
                    this.constraints.Add(c);
        }
        public void AddConstr(Node constr, bool AddLast = true) { if (AddLast) constraints.Add(constr); else constraints.Insert(0, constr); }
        public List<Node> constraints = new List<Node>();
        public override IEnumerable<Node> Children
        {
            get { return constraints; }
        }
        public override string ToString()
        {
            return String.Join(", ", constraints.Select(x => x.ToString()));
        }
        public override object Clone()
        {
            var res = new Body() { Span = Span };
            foreach (var c in constraints)
                res.constraints.Add((Node)c.Clone());
            return res;
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;
            var res = new Body() { Span = Span };
            foreach (var c in constraints)
                res.constraints.Add((Node)c.Substitute(match,substitution));
            return res;
        }
        public bool Equals(Body obj)
        {
            if (constraints.Count != obj.constraints.Count)
                return false;
            for (int i = 0; i < constraints.Count; ++i)
                if (!constraints[i].Equals(obj.constraints[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is Body ? Equals((Body)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            foreach (var c in constraints)
                hash = hash * 37 + c.GetHashCode();
            return hash;
        }
    }

    public class Find : Node, IEquatable<Find>
    {
        public Find(Var binding, Node match) { Binding = binding; Match = match; }
        public Var Binding;
        public Node Match;
        public override IEnumerable<Node> Children
        {
            get
            {
                if (Binding != null) yield return Binding;
                yield return Match;
            }
        }
        public override string ToString()
        {
            if (Binding != null)
                return String.Format("{0} is {1}", Binding, Match);
            return String.Format("{0}", Match);
        }
        public override object Clone()
        {
            if (Binding != null)
                return new Find((Var)Binding.Clone(), (Node)Match.Clone()) { Span = Span };
            else
                return new Find(null, (Node)Match.Clone()) { Span = Span };
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;            
            return new Find(
                        (Var)Binding.Substitute(match, substitution),
                        (Node)Match.Substitute(match, substitution)) 
                        { Span = Span };
        }
        public bool Equals(Find obj)
        {
            return Equals(Binding, obj.Binding) && Equals(Match, obj.Match);
        }
        public override bool Equals(object obj)
        {
            return obj is Find ? Equals((Find)obj) : false;
        }
        public override int GetHashCode()
        {
            return Match.GetHashCode() + ((Binding == null) ? 0 : 37*Binding.GetHashCode()); 
        }
    }
    
    public class RelConstr : Node, IEquatable<RelConstr>
    {
        public RelConstr(RelKind kind, Node arg) { Op = kind; Arg1 = arg; }
        public RelConstr(RelKind kind, Node arg1, Node arg2) { Op = kind; Arg1 = arg1; Arg2 = arg2; }
        public RelKind Op;
        public Node Arg1;
        public Node Arg2;
        public override IEnumerable<Node> Children
        {
            get
            {
                yield return Arg1;
                if (Arg2 != null) yield return Arg2;
            }
        }
        public override string ToString()
        {
            switch (Op)
            {
                case RelKind.Eq: return Arg1.ToString() + "=" + Arg2.ToString();
                case RelKind.Neq: return Arg1.ToString() + "!=" + Arg2.ToString();
                case RelKind.Ge: return Arg1.ToString() + ">=" + Arg2.ToString();
                case RelKind.Gt: return Arg1.ToString() + ">" + Arg2.ToString();
                case RelKind.Le: return Arg1.ToString() + "<=" + Arg2.ToString();
                case RelKind.Lt: return Arg1.ToString() + "<" + Arg2.ToString();
                case RelKind.No: return "no " + Arg1.ToString();
                case RelKind.Typ: return Arg1.ToString() + ":" + Arg2.ToString();
            }
            return null;
        }
        public override object Clone()        
        {
            if (Arg2 != null)
                return new RelConstr(Op, (Node)Arg1.Clone(), (Node)Arg2.Clone()) { Span = Span };
            else
                return new RelConstr(Op, (Node)Arg1.Clone(), null) { Span = Span };
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;
            Node carg1 = (Node)Arg1.Substitute(match, substitution);
            Node carg2 = Arg2 != null ? (Node)Arg2.Substitute(match, substitution) : null;
            return new RelConstr(Op, carg1, carg2) { Span = Span };
        }
        public bool Equals(RelConstr obj)
        {
            return Op == obj.Op && Equals(Arg1, obj.Arg1) && Equals(Arg2, obj.Arg2);
        }
        public override bool Equals(object obj)
        {
            return obj is RelConstr ? Equals((RelConstr)obj) : false;
        }
        public override int GetHashCode()
        {
            return ((int)Op * 37 + Arg1.GetHashCode()) * 37 + (Arg2 != null ? Arg2.GetHashCode() : 0);
        }
    }
    public class Var : Node, IEquatable<Var>
    {
        public Var(string name) : base(name) { }
        public override IEnumerable<Node> Children
        {
            get { yield break; }
        }
        public Var AccessorFreeVar { get; private set; }
        public override string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                base.Name = value;
                int ipos = Name.IndexOf('.');
                if (ipos != -1)
                    AccessorFreeVar = new Var(value.Substring(0, ipos));
                else
                    AccessorFreeVar = this;
            }
        }
        public override string ToString()
        {
            return Log.g_DetailedNodeInformation ? String.Format("{0}#var", Name) : Name;
        }
        public bool Equals(Var obj) { return base.Name.Equals(obj.Name); }
        public override bool Equals(object obj)
        {
            if (obj is Var) 
                return Equals((Var)obj); 
            else 
                return false;
        }
        public override int GetHashCode() 
        {
            if (!dirtyHashcode)
                return storedHashcode;
            storedHashcode = Name.GetHashCode();
            dirtyHashcode = false;
            return storedHashcode;
        }
        public override object Clone()
        {
            return new Var(Name) { Span = Span };
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;
            // If this is an accessor that matches the match variable, and substitution is a UserFuncTerm, extract the appropriate field
            if (match is Var && substitution is UserFuncTerm)
            {
                var vmatch = (Var)match;
                var func = (UserFuncTerm)substitution;
                var decl = func.Function.decl as ConDecl;
                int ipos = Name.IndexOf('.');
                if (decl != null && ipos != -1)
                {
                    var varname = Name.Substring(0, ipos);                    
                    if (vmatch.Name == varname)
                    {
                        var accname = Name.Substring(ipos+1);
                        // find the field in the declaration                        
                        for (int i = 0; i < decl.Fields.Count; ++i)
                            if (decl.Fields[i].Name == accname)
                                return (Node)func.args[i].Clone();
                        throw new UserException(String.Format("Field {0} not found in type {1}", accname, decl.Name), this);
                    }
                }
            }
            return (Node)Clone();
        }
    }
    public abstract class FuncTerm : Node
    {
        public void AddArg(Node arg, bool Add = true) { if (Add) args.Add(arg); else args.Insert(0, arg); }
        public List<Node> args;
        public override IEnumerable<Node> Children
        {
            get
            {
                return args;
            }
        }
    }
    public class UserFuncTerm : FuncTerm, IEquatable<UserFuncTerm>
    {
        public UserFuncTerm(Id function, IEnumerable<Node> args) 
        { 
            Function = function;
            if (args != null)
                this.args = new List<Node>(args);
            else
                this.args = new List<Node>();
        }
        public UserFuncTerm(Id function) { Function = function; args = new List<Node>(); }
        public Id Function;
        public string Id; // for named terms
        public string Namespace 
        {
            get
            {
                int ipos = Function.Name.LastIndexOf('.');
                if (ipos != -1)
                    return Function.Name.Substring(0, ipos);
                else
                    return null;
            }
        }
        public string PureName
        {
            get
            {
                int ipos = Function.Name.LastIndexOf('.');
                if (ipos != -1)
                    return Function.Name.Substring(ipos + 1);
                else
                    return Function.Name;
            }
        }
        public override string Name 
        { 
            get { return Function.Name; }
            set { Function.Name = value; }
        }
        public override IEnumerable<Node> Children
        {
            get
            {
                foreach (var n in args) yield return n;
            }
        }
        public override string ToString()
        {
            if (!dirtyToString)
                return storedToString;
            if (args.Count > 0)
            {
                string[] sargs = args.Select(x => x.ToString()).ToArray();
                storedToString = String.Format("{0}({1})", Function, String.Join(", ", sargs));
            }
            else
                storedToString = Function.ToString();
            dirtyToString = false;
            return storedToString;
        }
        public bool Equals(UserFuncTerm f)
        {
            return f.ToString() == ToString();
            /*if (f == null || !Function.Equals(f.Function) || f.args.Count != args.Count) return false;
            for (int i = 0; i < args.Count; ++i)
                if (!args[i].Equals(f.args[i]))
                    return false;
            return true;*/
        }
        public override bool Equals(object obj)
        {
            return (obj is UserFuncTerm) ? Equals((UserFuncTerm)obj) : false;
        }
        public override int GetHashCode()
        {
            if (!dirtyHashcode)
                return storedHashcode;
            int hash = Function.GetHashCode();
            for (int i = 0; i < args.Count; ++i)
                hash = hash * 37 + args[i].GetHashCode();
            storedHashcode = hash;
            dirtyHashcode = false;
            return storedHashcode;
        }
        
        public override object Clone()
        {
            var res = new UserFuncTerm((Id)Function.Clone()) { Span = Span };
            foreach (var arg in args)
                res.args.Add((Node)arg.Clone());
            return res;
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;
            var res = new UserFuncTerm(Function, args.Select(x => x.Substitute(match, substitution))) { Span = Span };
            return res;
        }
    }
    public class BuiltinFuncTerm : FuncTerm, IEquatable<BuiltinFuncTerm>
    {
        public BuiltinFuncTerm(OpKind function, IEnumerable<Node> args) 
        { 
            Function = function;
            if (args != null)
                this.args = new List<Node>(args);
            else
                this.args = new List<Node>();
        }
        public BuiltinFuncTerm(OpKind function) { Function = function; args = new List<Node>(); }
        public OpKind Function;
        public override string Name 
        { 
            get { return Function.ToString(); } 
            set { throw new NotImplementedException(); }
        }
        public override string ToString()
        {
            switch (Function)
            {
                case OpKind.Add: return String.Format("({0}+{1})", args[0].ToString(), args[1].ToString());
                case OpKind.Sub: return String.Format("({0}-{1})", args[0].ToString(), args[1].ToString());
                case OpKind.Mul: return String.Format("({0}*{1})", args[0].ToString(), args[1].ToString());
                case OpKind.Div: return String.Format("({0}/{1})", args[0].ToString(), args[1].ToString());
                case OpKind.Mod: return String.Format("({0}%{1})", args[0].ToString(), args[1].ToString());
                case OpKind.Neg: return String.Format("(-{0})", args[0].ToString());
                case OpKind.ToList: return String.Format("toList({0},{1},{2})", args[0].ToString(), args[1].ToString(), args[2].ToString());
                case OpKind.ToString: return String.Format("toString({0})", args[0].ToString());
                case OpKind.StrJoin: return String.Format("strJoin({0},{1})", args[0].ToString(), args[1].ToString());
                case OpKind.Count: return String.Format("count({0})", args[0].ToString());
                case OpKind.ToNatural: return String.Format("toNatural({0})", args[0].ToString());
                case OpKind.RflIsMember: return String.Format("RflIsMember({0},{1})", args[0].ToString(), args[1].ToString());
            }
            return "";
        }
        public bool Equals(BuiltinFuncTerm f)
        {
            if (f == null || f.Function != Function || f.args.Count != args.Count) return false;
            for (int i = 0; i < args.Count; ++i)
                if (!args[i].Equals(f.args[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is BuiltinFuncTerm ? Equals((BuiltinFuncTerm)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = Function.GetHashCode();
            for (int i = 0; i < args.Count; ++i)
                hash = hash * 37 + args[i].GetHashCode();
            return hash;
        }
        public override Node Simplify(TypeChecking typeChecker)
        {
            if (args.Count == 1)
            {
                Node sarg = args[0].Simplify(typeChecker);

                if (Function == OpKind.ToString)
                    return new StringCnst(sarg.ToString());
                if (Function == OpKind.ToNatural)
                {
                    UInt32 r;
                    if (UInt32.TryParse(sarg.ToString(), out r))
                        return new RationalCnst(new Rational(r));
                    else
                        return new RationalCnst(new Rational(Math.Abs(sarg.GetHashCode())));
                }                
                
                RationalCnst rarg = sarg as RationalCnst;
                if (rarg != null)
                    switch (Function)
                    {
                        case OpKind.Neg: return new RationalCnst(-rarg.Cnst);
                    }
            }
            else if (args.Count == 2)
            {
                Node sarg1 = args[0].Simplify(typeChecker);
                Node sarg2 = args[1].Simplify(typeChecker);

                if (Function == OpKind.StrJoin)
                    return new StringCnst(sarg1.ToString() + sarg2.ToString());                
                if (Function == OpKind.RflIsMember)
                    return new BoolCnst(typeChecker.isSubtype(typeChecker.GetTypeOf(sarg1), typeChecker.GetTypeOf(sarg2)));

                RationalCnst rarg1 = sarg1 as RationalCnst;
                RationalCnst rarg2 = sarg2 as RationalCnst;
                if (rarg1 != null && rarg2 != null)
                    switch (Function)
                    {
                        case OpKind.Add: return new RationalCnst(rarg1.Cnst + rarg2.Cnst);
                        case OpKind.Sub: return new RationalCnst(rarg1.Cnst - rarg2.Cnst);
                        case OpKind.Mul: return new RationalCnst(rarg1.Cnst * rarg2.Cnst);
                        case OpKind.Div: return new RationalCnst(rarg1.Cnst / rarg2.Cnst);
                        case OpKind.Mod: return new RationalCnst(Rational.Remainder(rarg1.Cnst, rarg2.Cnst));
                    }
            }
            throw new NotImplementedException();
        }        
        public override object Clone()
        {
            var res = new BuiltinFuncTerm(Function) { Span = Span };
            foreach (var arg in args)
                res.args.Add((Node)arg.Clone());
            return res;
        }
        public override Node Substitute(Node match, Node substitution)
        {
            if (Equals(match)) return substitution;
            var res = new BuiltinFuncTerm(Function, args.Select(x => x.Substitute(match, substitution))) { Span = Span };
            return res;
        }
    }
}
