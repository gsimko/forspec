﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe
{
    using Nodes;
//    interface MyToken
    //{
    //}
    interface IType
    {
    }
    interface IUnionType: IType
    {
    }
    interface IRule
    {
    }
    interface IBody
    {
    }
    interface IPattern
    {
    }

    class MyToken : IEquatable<MyToken>
    {
        public MyToken() { }
        //public MyToken(TypeDecl type, Node expr) { this.type = type; this.expr = expr; }
        //public TypeDecl type;
        //public Node expr;
        public List<Var> paramVars = new List<Var>();
        public List<Node> paramValues = new List<Node>();
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            //if (type != null) sb.AppendFormat("Type: {0}\n", type);
            //if (expr != null) sb.AppendLine(String.Format("Expression: {0}", expr.ToString()));
            sb.AppendLine("paramValues:");
            for (int i=0; i<paramValues.Count; ++i)
                sb.AppendFormat("\t{0}:{1}\n", paramVars[i], paramValues[i]);
            return sb.ToString();
        }
        public bool Equals(MyToken token)
        {
            if (paramValues.Count != token.paramValues.Count) return false;
            //if (!Equals(type, token.type)) return false;
            //if (!Equals(expr, token.expr)) return false;
            for (int i = 0; i < paramVars.Count; ++i)
                if (!paramVars[i].Equals(token.paramVars[i]))
                    return false;
            for (int i = 0; i < paramValues.Count; ++i)
                if (!paramValues[i].Equals(token.paramValues[i]))
                    return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is MyToken ? Equals((MyToken)obj) : false;
        }
        public override int GetHashCode()
        {
            int hash = 0;// type == null ? 0 : type.GetHashCode();
            unchecked
            {
                //if (expr != null)
                //hash = hash * 37 + expr.GetHashCode();
                foreach (var var in paramVars)
                    hash = hash*37 + var.GetHashCode();
                foreach (var val in paramValues)
                    hash = hash*37 + val.GetHashCode();
            }
            return hash;
        }
    }
    
    class Rete
    {
        abstract class ReteNode
        {
            public HashSet<ReteNode> children = new HashSet<ReteNode>();
            protected Execution driver;
            public string originalFormula;
            public List<Var> listOfVariables = new List<Var>();
            public void AddChild(ReteNode node)
            {
                children.Add(node);
            }
            public virtual void Evaluate(MyToken token, ReteNode caller)
            {
                throw new NotImplementedException();
            }
            public virtual void Debug(MyToken token)
            {
                return;
                Log.WriteLine(10, "Evaluate ");
                Log.WriteLine(10, "\t{0}", ToString().Replace("\n", "\n\t"));
                if (token != null)
                    Log.WriteLine(10, "\t" + token.ToString().Replace("\n", "\n\t"));
            }
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}, {1} [{2}]\n", originalFormula, this.GetType().ToString(), GetHashCode());
                sb.Append("\t").Append(String.Join("\n\t", children.Select(x => x.ToString().Replace("\n", "\n\t"))));
                return sb.ToString();
            }
            public override bool Equals(object obj)
            {
                throw new NotImplementedException();
            }
            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }
        }
        class RootNode : ReteNode
        {
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Root {0}\n", GetHashCode());
                sb.Append("\t").Append(String.Join("\n\t", children.Select(x => x.ToString().Replace("\n", "\n\t"))));
                return sb.ToString();
            }
            public override int GetHashCode()
            {
                return 0;
            }
        }
        class UnionTypeNode : TypeNode
        {
            public UnionTypeNode(TypeDecl type, Execution driver) : base(type, driver)
            {
            }
            public override bool Equals(object obj)
            {
                if (!(obj is UnionTypeNode)) return false;
                UnionTypeNode un = (UnionTypeNode)obj;
                return type.Equals(un.type);
            }
            public override int GetHashCode()
            {
                return type.GetHashCode();
            }
        }
        class TypeNode : ReteNode
        {
            public TypeDecl type;
            public TypeNode(TypeDecl type, Execution driver)
            {
                this.type = type;
                this.driver = driver;
                this.originalFormula = type.ToString();
            }
            public void EvaluateFact(UserFuncTerm fact)
            {
                foreach (var pat in children)
                    ((PatternNode)pat).EvaluateFact(fact);
            }
            public override bool Equals(object obj)
            {
                if (!(obj is TypeNode)) return false;
                TypeNode un = (TypeNode)obj;
                return type.Equals(un.type);
            }
            public override int GetHashCode()
            {
                return type.GetHashCode();
            }
        }
        class PatternNode : ReteNode
        {
            public Find pattern;
            public PatternNode(Find pattern, Execution driver)
            {
                this.pattern = pattern;
                this.driver = driver;
                this.originalFormula = pattern.ToString();
                this.listOfVariables = pattern.GetListOfVariables();
            }
            public void EvaluateFact(UserFuncTerm fact)
            {
                //Debug(token);
                var subst = new Dictionary<Var, Node>();
                if (!driver.Match(pattern, fact, ref subst))
                {
                    //Log.WriteLine(8, "Pattern not matching: {0} vs. {1}", pattern.ToString(), fact);
                    return;
                }
                MyToken token = new MyToken();
                token.paramVars = listOfVariables;
                token.paramValues = new List<Node>(listOfVariables.Count);
                foreach (var var in listOfVariables)
                    token.paramValues.Add(subst[var]);
                foreach (var child in children)
                    child.Evaluate(token, this);
            }
            public override int GetHashCode()
            {
                return pattern.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (!(obj is PatternNode)) return false;
                var pobj = (PatternNode)obj;
                return pattern.Equals(pobj.pattern);
            }
        }
        class MatchNode : ReteNode
        {
            public RelConstr match;
            public ReteNode parent;
            public MatchNode(RelConstr match, ReteNode parent, Execution driver)
            {
                this.match = match;
                this.driver = driver;
                this.parent = parent;
                this.listOfVariables = new List<Var>(parent.listOfVariables);
                var contains = new HashSet<Var>(parent.listOfVariables);

                var vars = match.GetListOfPositiveVariables();
                foreach (var v in vars)
                {
                    if (!contains.Contains(v) && !contains.Contains(v.AccessorFreeVar))
                        this.listOfVariables.Add(v);
                }
                this.originalFormula = match.ToString();
            }
            public override void Evaluate(MyToken token, ReteNode caller)
            {
                Debug(token);
                
                var substRel = (RelConstr)match.Clone();
                Dictionary<Var, Node> subst = new Dictionary<Var, Node>(token.paramVars.Count);
                for (int i = 0; i < token.paramVars.Count; ++i)
                    subst[token.paramVars[i]] = token.paramValues[i];
                substRel.ReplaceVarsInPlace(subst, null, driver.typeChecker);
                var arg1 = driver.SimplifyTerm(substRel.Arg1);
                var arg2 = driver.SimplifyTerm(substRel.Arg2);

                if (!arg2.isGround())
                {
                    var tmp = arg2; arg2 = arg1; arg1 = tmp;
                }
                if (!arg2.isGround() || !driver.Match(arg1, arg2, ref subst))
                {
                    //Log.WriteLine(8, "Pattern not matching with parameter value {0}", match.ToString());
                    return;
                }
                // Now, check for cases, when accessors variables may mismatch others
                bool inconsistent = false;
                foreach (var kvp in subst)
                {
                    var Qaccessor = kvp.Key;
                    var Q = kvp.Key.AccessorFreeVar;
                    if (Q == kvp.Key) continue;
                    if (subst.ContainsKey(Q))
                    {
                        // Q.accessor = X
                        // Q = Y
                        // If Q.accessor[Q->Y] != X, then mismatch.
                        var Y = subst[Q];
                        var X = kvp.Value;
                        if (!Qaccessor.Substitute(Q, Y).Equals(X))
                        {
                            inconsistent = true;
                            break;
                        }
                    }

                }
                if (inconsistent)
                    return;
                MyToken newtoken = new MyToken();
                newtoken.paramVars = listOfVariables;
                newtoken.paramValues = new List<Node>(listOfVariables.Count);
                foreach (var var in listOfVariables)
                    newtoken.paramValues.Add(subst[var]);

                foreach (var child in children)
                    child.Evaluate(newtoken, this);
            }
            public override int GetHashCode()
            {
                return match.GetHashCode() * 37 + parent.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (!(obj is MatchNode)) return false;
                var pobj = (MatchNode)obj;
                return match.Equals(pobj.match) && parent.Equals(pobj.parent);
            }
        }
        class ConstraintNode : ReteNode
        {
            RelConstr constraint;
            static int g_hash;
            int hash;
            ReteNode parent;
            public ConstraintNode(RelConstr constraint, ReteNode parent, Execution driver)
            {
                this.constraint = constraint;
                this.driver = driver;
                this.originalFormula = parent.originalFormula + "," + constraint.ToString();
                this.hash = g_hash++;
                this.parent = parent;
                this.listOfVariables = parent.listOfVariables;
            }
            public override void Evaluate(MyToken token, ReteNode caller)
            {
                Debug(token);
                Dictionary<Var, Node> subst = new Dictionary<Var, Node>(token.paramVars.Count);
                for (int i = 0; i < token.paramVars.Count; ++i)
                    subst[token.paramVars[i]] = token.paramValues[i];
                if (driver.isConstraintSatisfied(constraint, subst))
                    foreach (var child in children)
                        child.Evaluate(token, this);
            }
            public override int GetHashCode()
            {
                return constraint.GetHashCode() * 37 + parent.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (!(obj is ConstraintNode)) return false;
                var pobj = (ConstraintNode)obj;
                return constraint.Equals(pobj.constraint) && parent.Equals(pobj.parent);
            }
        }

        class JoinConstraintNode : ReteNode
        {
            HashSet<MyToken> LeftMemory = new HashSet<MyToken>();
            HashSet<MyToken> RightMemory = new HashSet<MyToken>();
            ReteNode leftParent;
            ReteNode rightParent;
            List<Tuple<int, int>> equalities = new List<Tuple<int,int>>();
            List<int> varMap = new List<int>();
            public JoinConstraintNode(ReteNode leftParent, ReteNode rightParent, Execution driver)
            {
                this.leftParent = leftParent;
                this.rightParent = rightParent;
                
                // Initialize list of variables to the variables of the left parent
                this.listOfVariables = new List<Var>(leftParent.listOfVariables);
                this.varMap = new List<int>(leftParent.listOfVariables.Count);
                for (int i = 0; i < leftParent.listOfVariables.Count; ++i) varMap.Add(-1);
                for (int i=0; i<rightParent.listOfVariables.Count; ++i)
                {                    
                    // Try to find the same variable in the left parent
                    int found = -1;
                    for (int j=0; j<leftParent.listOfVariables.Count; ++j)
                        if (leftParent.listOfVariables[j].Equals(rightParent.listOfVariables[i]))
                        {
                            found = j;
                            break;
                        }
                    // If found, add equality
                    if (found != -1)
                        equalities.Add(new Tuple<int, int>(found, i));
                    // Else add variable to list of variables
                    else
                    {
                        listOfVariables.Add(rightParent.listOfVariables[i]);
                        varMap.Add(i);
                    }
                }

                this.driver = driver;
                this.originalFormula = leftParent.originalFormula + "," + rightParent.originalFormula;            
            }
            public override void Debug(MyToken token)
            {
                base.Debug(token);
                /*Log.WriteLine(10, "\tLeft memory");
                Log.WriteLine(10, "\t\t{0}", String.Join("\n\t\t", LeftMemory.Select(x => x.ToString().Replace("\n", "\n\t\t"))));
                Log.WriteLine(10, "\tRight memory");
                Log.WriteLine(10, "\t\t{0}", String.Join("\n\t\t", RightMemory.Select(x => x.ToString().Replace("\n", "\n\t\t"))));*/
            }
            public override void Evaluate(MyToken token, ReteNode caller)
            {
                Debug(token);
                                
                var mem = caller == leftParent ? RightMemory : LeftMemory;
                
                foreach (var token2 in mem)
                {
                    var leftToken = caller == leftParent ? token : token2;
                    var rightToken = caller == leftParent ? token2 : token;
                    bool inconsistent = false;
                    foreach (var eq in equalities)
                    {
                        var lValue = leftToken.paramValues[eq.Item1];
                        var rValue = rightToken.paramValues[eq.Item2];
                        if (lValue.GetHashCode() != rValue.GetHashCode() || !lValue.Equals(rValue))
                        {
                            inconsistent = true;
                            break;
                        }
                    }
                    if (inconsistent) continue;

                    List<Node> values = new List<Node>(listOfVariables.Count);
                    foreach (var value in leftToken.paramValues)
                        values.Add(value);
                    for (int i = leftParent.listOfVariables.Count; i < listOfVariables.Count; ++i)
                        values.Add(rightToken.paramValues[varMap[i]]);

                    var substitutions = new Dictionary<Var, Node>(leftToken.paramValues.Count);
                    for (int i = 0; i < values.Count; ++i)
                        substitutions[listOfVariables[i]] = values[i];
                    // Now, check for cases, when accessors variables may mismatch others
                    foreach (var kvp in substitutions)
                    {
                        var Qaccessor = kvp.Key;
                        var Q = kvp.Key.AccessorFreeVar;
                        if (Q == kvp.Key) continue;
                        if (substitutions.ContainsKey(Q))
                        {
                            // Q.accessor = X
                            // Q = Y
                            // If Q.accessor[Q->Y] != X, then mismatch.
                            var Y = substitutions[Q];
                            var X = kvp.Value;
                            if (!Qaccessor.Substitute(Q, Y).Equals(X))
                            {
                                inconsistent = true;
                                break;
                            }
                        }

                    }
                    if (!inconsistent)
                    {
                        var jtoken = new MyToken();
                        jtoken.paramVars = listOfVariables;
                        jtoken.paramValues = values;                        
                        foreach (var child in children)
                            child.Evaluate(jtoken, this);
                    }
                }
                if (caller == leftParent)
                    LeftMemory.Add(token);
                else
                    RightMemory.Add(token);
            }
            public override int GetHashCode()
            {
                return unchecked(leftParent.GetHashCode()*37 + rightParent.GetHashCode());
            }
            public override bool Equals(object obj)
            {
                if (!(obj is JoinConstraintNode)) return false;
                var pobj = (JoinConstraintNode)obj;
                return leftParent == pobj.leftParent && rightParent == pobj.rightParent;
            }
        }
        class TriggeredConstraintNode : ReteNode
        {
            public TypeDecl type;
            public TriggeredConstraintNode(TypeDecl type, Execution driver)
            {
                this.type = type;
                this.driver = driver;
                this.originalFormula = type.ToString();
                this.listOfVariables = new List<Var>();
            }
            public virtual void Trigger()
            {
                Debug(null);
                MyToken token = new MyToken();

                foreach (var child in children)
                    child.Evaluate(token, this);
            }
            public override void Evaluate(MyToken token, ReteNode caller)
            {
            }
            public override int GetHashCode()
            {
                return type.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (!(obj is TriggeredConstraintNode)) return false;
                var pobj = (TriggeredConstraintNode)obj;
                return type.Equals(pobj.type);
            }
        }
        class MemTriggeredConstraintNode : TriggeredConstraintNode
        {
            HashSet<MyToken> Memory = new HashSet<MyToken>();
            public MemTriggeredConstraintNode(TypeDecl type, ReteNode parent, Execution driver)
                : base(type, driver)
            {
                this.originalFormula = parent.originalFormula + "," + type.ToString();
                this.listOfVariables = parent.listOfVariables;
            }
            public override void Debug(MyToken token)
            {
                base.Debug(token);
                //Log.WriteLine(10, "Trigger");
                //Log.WriteLine(10, "\tMemory");
                //Log.WriteLine(10, "\t\t{0}", String.Join("\n\t\t", Memory.Select(x => x.ToString().Replace("\n", "\n\t\t"))));
            }
            public override void Trigger()
            {
                foreach (var token in Memory)
                {
                    Debug(token);
                    foreach (var child in children)
                        child.Evaluate(token, this);
                }
            }
            public override void Evaluate(MyToken token, ReteNode caller)
            {
                //Debug(token);
                Memory.Add(token);
            }
            public override int GetHashCode()
            {
                return type.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (!(obj is MemTriggeredConstraintNode)) return false;
                var pobj = (MemTriggeredConstraintNode)obj;
                return type.Equals(pobj.type);
            }
        }
        class TerminalNode : ReteNode
        {
            List<UserFuncTerm> list;
            UserFuncTerm head;
            public TerminalNode(UserFuncTerm head, List<UserFuncTerm> list, Execution driver)
            {
                this.head = head;
                this.list = list;
                this.driver = driver;
                this.originalFormula = head.ToString();
            }
            public override void Evaluate(MyToken token, ReteNode caller)
            {
                Debug(token);
                //Log.WriteLine(10, "\tHead:{0}", head);
                Dictionary<Var, Node> subst = new Dictionary<Var, Node>(token.paramVars.Count);
                for (int i = 0; i < token.paramVars.Count; ++i)
                    subst[token.paramVars[i]] = token.paramValues[i];
                var term = (UserFuncTerm)head.Clone();
                term.ReplaceVarsInPlace(subst, null, driver.typeChecker);

                // Do type checking on the result
                if (!driver.typeChecker.NodeWelltyped(term) || !term.isGround())
                    throw new UserException(String.Format("Runtime error: {0} badly typed", term.ToString()), term);

                // Expand identity function
                if (term.PureName == "_")
                    term = (UserFuncTerm)term.args[0];

                //Log.WriteLine(10, "\tAdd fact:{0}", term);
                list.Add(term);
            }
            public override int GetHashCode()
            {
                return head.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                if (!(obj is TerminalNode)) return false;
                var pobj = (TerminalNode)obj;
                return head.Equals(pobj.head);
            }
        }

        Dictionary<TypeDecl, List<TriggeredConstraintNode>> toBeTriggeredNodes = new Dictionary<TypeDecl, List<TriggeredConstraintNode>>();
        private void AddTriggerableNode(TriggeredConstraintNode node, TypeDecl type)
        {
            List<TriggeredConstraintNode> list;
            if (!toBeTriggeredNodes.TryGetValue(type, out list))
            {
                list = new List<TriggeredConstraintNode>();
                toBeTriggeredNodes.Add(type, list);
            }
            list.Add(node);
        }

        public int statReuse = 0;

        Dictionary<TypeDecl, TypeNode> typeNodes = new Dictionary<TypeDecl, TypeNode>();
        Dictionary<TypeDecl, TriggeredConstraintNode> triggeredNodes = new Dictionary<TypeDecl,TriggeredConstraintNode>();
        Dictionary<MemTriggeredConstraintNode, MemTriggeredConstraintNode> memTriggeredNodes = new Dictionary<MemTriggeredConstraintNode, MemTriggeredConstraintNode>();
        Dictionary<PatternNode, PatternNode> patternNodes = new Dictionary<PatternNode,PatternNode>();
        Dictionary<ConstraintNode, ConstraintNode> constraintNodes = new Dictionary<ConstraintNode, ConstraintNode>();
        Dictionary<JoinConstraintNode, JoinConstraintNode> joinConstraintNodes = new Dictionary<JoinConstraintNode, JoinConstraintNode>();
        Dictionary<TerminalNode, TerminalNode> terminalNodes = new Dictionary<TerminalNode, TerminalNode>();
        TypeNode CreateTypeNode(TypeDecl type) 
        {
            if (typeNodes.ContainsKey(type))
            {
                statReuse++;
                return (TypeNode)typeNodes[type];
            }
            var node = new TypeNode(type, driver);
            typeNodes.Add(type, node);
            return node;
        }
        UnionTypeNode CreateUnionTypeNode(TypeDecl type) 
        {
            if (typeNodes.ContainsKey(type))
            {
                statReuse++;
                return (UnionTypeNode)typeNodes[type];
            }
            var node = new UnionTypeNode(type, driver);
            typeNodes.Add(type, node);
            return node;
        }
        PatternNode CreatePatternNode(Find pattern) 
        {
            var id = new PatternNode(pattern, driver);
            if (patternNodes.ContainsKey(id))
            {
                statReuse++;
                return patternNodes[id];
            }
            patternNodes.Add(id, id);
            return id;
        }
        ConstraintNode CreateConstraintNode(RelConstr constraint, ReteNode parent) 
        {
            var id = new ConstraintNode(constraint, parent, driver);
            if (constraintNodes.ContainsKey(id))
            {
                statReuse++;
                return constraintNodes[id];
            }
            constraintNodes.Add(id, id);
            return id;
        }
        JoinConstraintNode CreateJoinConstraintNode(ReteNode leftParent, ReteNode rightParent) 
        {
            var id = new JoinConstraintNode(leftParent, rightParent, driver);
            if (joinConstraintNodes.ContainsKey(id))
            {
                statReuse++;
                return joinConstraintNodes[id];
            }
            joinConstraintNodes.Add(id, id);
            return id;
        }
        TriggeredConstraintNode CreateTriggeredConstraintNode(TypeDecl type) 
        {
            if (triggeredNodes.ContainsKey(type))
            {
                statReuse++;
                return triggeredNodes[type];
            }
            var node = new TriggeredConstraintNode(type, driver);
            triggeredNodes.Add(type, node);
            AddTriggerableNode(node, type);
            return node;
        }
        MemTriggeredConstraintNode CreateMemTriggeredConstraintNode(TypeDecl type, ReteNode parent)
        {
            MemTriggeredConstraintNode id = new MemTriggeredConstraintNode(type, parent, driver);
            if (memTriggeredNodes.ContainsKey(id))
            {
                statReuse++;
                return memTriggeredNodes[id];
            }
            memTriggeredNodes.Add(id, id);
            AddTriggerableNode(id, type);
            return id;
        }
        TerminalNode CreateTerminalNode(UserFuncTerm head) 
        {
            TerminalNode id = new TerminalNode(head, resultList, driver);
            if (terminalNodes.ContainsKey(id))
            {
                statReuse++;
                return terminalNodes[id];
            }
            terminalNodes.Add(id, id);
            return id;
        }

        protected Execution driver;

        public Rete(Execution driver)
        {
            this.driver = driver;
        }
                
        /// <summary>
        /// Build a graph expressing a rule
        /// </summary>
        /// <param name="rule"></param>
        private void BuildRuleGraph(RuleNode rule)
        {
            foreach (var body in rule.Bodies)
            {
                // extract constraints in sorted order
                var constraints = new List<RelConstr>();
                foreach (var c in body.constraints)
                    if (c is RelConstr)
                        constraints.Add((RelConstr)c);
                constraints.Sort((x,y) => x.GetHashCode() - y.GetHashCode());
                var constraints_left = new LinkedList<RelConstr>(constraints);

                // extract finds in sorted order
                var finds = new List<Find>();
                foreach (var c in body.constraints)
                    if (c is Find)
                        finds.Add((Find)c);
                finds.Sort((x, y) => x.GetHashCode() - y.GetHashCode());
                                
                // List of variables that are referred to by non-"no" nodes
                HashSet<Var> positiveVars = new HashSet<Var>();
                HashSet<Var> allVars = new HashSet<Var>();
                foreach (var c in body.constraints)
                {
                    allVars.UnionWith(c.GetVariables());
                    positiveVars.UnionWith(c.GetPositiveVariables());
                }
                HashSet<Var> noVars = new HashSet<Var>(allVars);
                noVars.ExceptWith(positiveVars);

                // Assume that variables under only "no" nodes are defined
                HashSet<Var> definedVars = new HashSet<Var>(noVars);

                ReteNode last_node = null;
                foreach (var find in finds)
                {
                    // Create node for pattern, and register it under the appropriate type node
                    ReteNode new_node = CreatePatternNode(find);
                    TypeDecl type = null;
                    if (find.Match is UserFuncTerm)                            
                        type = ((UserFuncTerm)find.Match).Function.decl;
                    else if (find.Match is Id)
                        type = ((Id)find.Match).decl;
                    Debug.Assert(type != null);
                    typeNodes[type].AddChild(new_node);

                    // Update set of defined variables
                    var newVars = find.GetVariablesWithAccessor();
                    definedVars.UnionWith(newVars);

                    // Add constraint nodes to pattern node based on the new variables
                    newVars.UnionWith(noVars);
                    new_node = AddConstraintNodes(constraints_left, newVars, new_node);

                    // If there were other nodes before, join them
                    if (last_node != null)
                    {
                        // Create join node
                        var jnode = CreateJoinConstraintNode(last_node, new_node);
                        last_node.AddChild(jnode);
                        new_node.AddChild(jnode);

                        // Add constraint nodes based on all the defined variables
                        new_node = AddConstraintNodes(constraints_left, definedVars, jnode);
                    }
                    last_node = new_node;
                }
                // here we handle the case when there are no finds to match
                if (last_node == null)
                {
                    last_node = rootNode;
                    
                    // Try adding constraints
                    last_node = AddConstraintNodes(constraints_left, definedVars, last_node);
                }

                // Add equality constraints (assignments)
                while (true)
                {
                    RelConstr addConstraint = null;
                    var toRemove = new HashSet<object>();
                    foreach (var cons in constraints_left)
                    {
                        Nodes.RelConstr rel = (Nodes.RelConstr)cons;
                        if (rel.Op == RelKind.Eq)
                        {
                            var var1 = rel.Arg1.GetVariablesWithAccessor();
                            var var2 = rel.Arg2.GetVariablesWithAccessor();
                            definedVars.UnionWith(var1);
                            definedVars.UnionWith(var2);

                            // if equation contains toList(), no() or other triggerable element, add a triggerable node before
                            var triggers = new LinkedList<Node>(); ;
                            GetTriggeringElements(rel, triggers);
                            foreach (var trigger in triggers)
                            {
                                ReteNode t_node;
                                if (last_node == rootNode)
                                    t_node = CreateTriggeredConstraintNode(driver.typeChecker.GetTypeOf(trigger));
                                else
                                    t_node = CreateMemTriggeredConstraintNode(driver.typeChecker.GetTypeOf(trigger), last_node);
                                last_node.AddChild(t_node);
                                last_node = t_node;
                            }

                            // Don't register them, so each of them are unique. Necessary for handling multiple domains with same names.
                            var new_node = new MatchNode(rel, last_node, driver);
                            last_node.AddChild(new_node);
                            last_node = new_node;
                            addConstraint = cons;
                            break;
                        }
                    }
                    if (addConstraint == null) break;
                    constraints_left.Remove(addConstraint);

                    // Try adding new constraints based on defined variables
                    last_node = AddConstraintNodes(constraints_left, definedVars, last_node);
                }
                Debug.Assert(constraints_left.Count == 0);                    

                // Finally, add each head below the last_node
                foreach (var head in rule.Heads)
                    last_node.AddChild(CreateTerminalNode(head));
            }
        }

        private void GetTriggeringElements(Node node, LinkedList<Node> triggers)
        {
            if (node is RelConstr && ((RelConstr)node).Op == RelKind.No) 
                triggers.AddLast(((RelConstr)node).Arg1);
            else if (node is BuiltinFuncTerm)
            {
                var func = (BuiltinFuncTerm)node;
                switch (func.Function)
                {
                    case OpKind.Count:
                    case OpKind.Max:
                    case OpKind.Min:
                        triggers.AddLast(func.args[0]);
                        break;
                    case OpKind.ToList:
                        triggers.AddLast(func.args[2]);
                        break;
                }
            }
            foreach (var child in node.Children)
                GetTriggeringElements(child, triggers);
        }

        private ReteNode AddConstraintNodes(LinkedList<RelConstr> constraints_left, HashSet<Var> definedVars, ReteNode new_node)
        {
            // Check if we can evaluate any constraints now, and add them one by one
            while (true)
            {
                RelConstr found = null;
                foreach (var c in constraints_left)
                {
                    var cVars = c.GetVariables();
                    if (cVars.IsSubsetOf(definedVars))
                    {
                        // if constraint is toList(), no() or other triggerable element, add a triggerable node before
                        var triggers = new LinkedList<Node>();
                        GetTriggeringElements(c, triggers);
                        foreach (var trigger in triggers)
                        {
                            ReteNode t_node;
                            if (new_node == rootNode)
                                t_node = CreateTriggeredConstraintNode(driver.typeChecker.GetTypeOf(trigger));
                            else
                                t_node = CreateMemTriggeredConstraintNode(driver.typeChecker.GetTypeOf(trigger), new_node);
                            new_node.AddChild(t_node);
                            new_node = t_node;
                        }
                        var c_node = CreateConstraintNode(c, new_node);
                        new_node.AddChild(c_node);
                        new_node = c_node;
                        found = c;
                        break;
                    }
                }
                if (found == null) break;
                constraints_left.Remove(found);
            }
            return new_node;
        }

        public void Initialize()
        {
            // Build graph

            // Root node
            rootNode = new RootNode();

            // Add condecl type nodes below root
            typeNodes = new Dictionary<TypeDecl, TypeNode>();
            foreach (var type in driver.typeChecker.GetConDecls())
                rootNode.AddChild(CreateTypeNode(type));
            // Add logic constants below root
            foreach (var type in driver.typeChecker.GetConstDecls())
                rootNode.AddChild(CreateTypeNode(type));
            // Add union type nodes below root
            foreach (var type in driver.typeChecker.GetUnnDecls())
                rootNode.AddChild(CreateUnionTypeNode(type));
            // Create links from subtype nodes to union type nodes
            foreach (var type in driver.typeChecker.GetUnnDecls())
            {
                var node = typeNodes[type];
                foreach (var subtypeid in type.components)
                {
                    TypeNode subnode;
                    TypeDecl subtype = driver.typeChecker.GetTypeOf(subtypeid);
                    if (typeNodes.TryGetValue(subtype, out subnode))
                        subnode.AddChild(node);
                }
            }
                        
            // Add rules to the graph
            foreach (var rule in driver.GetRules())
                BuildRuleGraph(rule);

            // Replace union type nodes with direct connections from their subtypes to the rules
            HashSet<UnionTypeNode> toRemoveTypes = new HashSet<UnionTypeNode>();
            foreach (var typeNode in typeNodes.Values)
                if (typeNode is TypeNode)
                {
                    HashSet<UnionTypeNode> toRemove = new HashSet<UnionTypeNode>();
                    HashSet<ReteNode> toAdd = new HashSet<ReteNode>();
                    foreach (var child in typeNode.children)
                        if (child is UnionTypeNode)
                        {
                            AddDirectConnections(child, toAdd);
                            toRemove.Add((UnionTypeNode)child);                            
                        }
                    typeNode.children.ExceptWith(toRemove);
                    typeNode.children.UnionWith(toAdd);
                }
                else if (typeNode is UnionTypeNode)
                    toRemoveTypes.Add((UnionTypeNode)typeNode);
            foreach (var rem in toRemoveTypes)
                typeNodes.Remove(rem.type);
            rootNode.children.ExceptWith(toRemoveTypes);

            // Check if we made any cycles
            HashSet<ReteNode> visited = new HashSet<ReteNode>();
            HashSet<ReteNode> processed = new HashSet<ReteNode>();
            foreach (var type in typeNodes.Values)
                foreach (var child in type.children)
                    if (hasCycles(child, visited, processed))
                        throw new UserException("Internal error.", null);

            // Log the graph
            //Log.WriteLine(10, rootNode.ToString());
            foreach (var type in typeNodes.Values)
                Log.WriteLine(10, type.ToString());
            foreach (var trigger in triggeredNodes)
                Log.WriteLine(10, trigger.ToString());
            foreach (var trigger in memTriggeredNodes)
                Log.WriteLine(10, trigger.ToString());
        }

        bool hasCycles(ReteNode node, HashSet<ReteNode> visited, HashSet<ReteNode> processed)
        {
            visited.Add(node);
            foreach (var child in node.children)
                if (visited.Contains(child) && !processed.Contains(child))
                {
                    Console.WriteLine("Internal error. Rete has cycles.");
                    Console.WriteLine(child.originalFormula);
                    Console.WriteLine(node.originalFormula);
                    return true;
                }
                else if (hasCycles(child, visited, processed))
                {
                    Console.WriteLine(node.originalFormula);
                    return true;
                }
            processed.Add(node);
            return false;
        }

        // Collect all the reachable non-union type nodes under a type node such that the path contains
        // only union type nodes.
        void AddDirectConnections(ReteNode node, HashSet<ReteNode> toAdd)
        {
            foreach (var child in node.children)
                if (child is UnionTypeNode)
                    AddDirectConnections(child, toAdd);
                else
                    toAdd.Add(child);
        }

        RootNode rootNode;
        //Dictionary<T, ReteNode> typeNodes;
        //Dictionary<T, List<TriggeredConstraintNode>> triggeredNodes;
        List<UserFuncTerm> resultList = new List<UserFuncTerm>();
        public IEnumerable<UserFuncTerm> AddFact(UserFuncTerm fact, TypeDecl type)
        {
            resultList.Clear();
            if (typeNodes.ContainsKey(type))
                typeNodes[type].EvaluateFact(fact);
            return resultList;
        }

        public IEnumerable<object> Trigger(TypeDecl type)
        {
            resultList.Clear();
            if (toBeTriggeredNodes.ContainsKey(type))
                foreach (var node in toBeTriggeredNodes[type])
                    node.Trigger();
            return resultList;
        }
    }
}
