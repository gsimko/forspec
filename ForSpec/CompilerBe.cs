﻿//#define DEBUG_GS

namespace FormulaBe
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Diagnostics.Contracts;
    using System.Threading;
    using System.Threading.Tasks;

    using System.Linq;
    using System.IO;
    using Nodes;
    using System.Text;

    class CompilerBe
    {
        const string GoalDrivenID = "#";
        const string GoalDrivenErrorCheck = GoalDrivenID + "error_check";
        const string GoalDrivenErrorCheckTerms = GoalDrivenErrorCheck + "_terms";
        const string GoalDrivenNondeterminismCheck = GoalDrivenID + "nondeterministic_check";
        const string GoalDrivenNondeterminismCheckTerms = GoalDrivenNondeterminismCheck + "_terms";
        static string CreateGoalDrivenEvalName(string name)
        {
            int pos = name.LastIndexOf('.');
            if (pos == -1)
                return String.Format(@"{0}{1}", GoalDrivenID, name);
            else
                return String.Format(@"{2}.{0}{1}", GoalDrivenID, name.Substring(pos+1), name.Substring(0,pos));
        }

        /// <summary>
        /// Recursively extract goal directed terms from a node.
        /// </summary>
        /// <param name="gdDecls"></param>
        /// <param name="node"></param>
        /// <param name="list"></param>
        static void ExtractGoalDirectedTerms(Dictionary<string, ConDecl> gdDecls, Node node, ref List<Tuple<string, int, Node>> list)
        {
            if (node is UserFuncTerm)
            {
                UserFuncTerm func = (UserFuncTerm)node;
                Id id = func.Function as Id;
                if (id != null)
                {
                    ConDecl conDecl;
                    if (gdDecls.TryGetValue(id.Name, out conDecl))
                    {
                        var removeFunctional = 0;
                        if (conDecl.IsGoalDriven)
                            removeFunctional = conDecl.Fields.Count - conDecl.CodomainFieldStart;
                        list.Add(new Tuple<string, int, Node>(id.Name, removeFunctional, func));
                    }
                    // don't look inside goaldriven terms
                    if (id.Name.StartsWith(GoalDrivenID))
                        return;
                }
            }
            foreach (var c in node.Children)
                ExtractGoalDirectedTerms(gdDecls, c, ref list);
        }

        static bool ContainsTerm(Node node, Node term)
        {
            return node.Equals(term) || node.Children.Any(x => ContainsTerm(x, term));
        }
        
        /// <summary>
        /// Extract the defined and required variables for a goal-driven find term.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="gdDecls"></param>
        /// <param name="definedVariables"></param>
        /// <param name="requiredVariables"></param>
        static void GetGoalDrivenVariables(Node node, Dictionary<string, ConDecl> gdDecls, ref HashSet<Var> definedVariables, ref HashSet<Var> requiredVariables)
        {
            if (node is Find)
            {
                var find = node as Find;
                if (find.Match is FuncTerm)
                {
                    var func = find.Match as FuncTerm;
                    if (IsGoalDrivenFuncTerm(func, gdDecls))
                    {
                        var conDecl = GetGoalDrivenFuncTermDecl(func, gdDecls);
                        int co = 0;
                        foreach (var child in func.args)
                        {
                            if (co >= conDecl.CodomainFieldStart)
                                definedVariables.UnionWith(child.GetVariables());
                            else
                                requiredVariables.UnionWith(child.GetVariables());
                            co++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// A term is well-defined if all the variables are well-defined within it.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="gdDecls"></param>
        /// <param name="definedVars"></param>
        /// <returns></returns>
        static bool TermWellDefined(Node node, Dictionary<string, ConDecl> gdDecls, HashSet<Var> definedVars)
        {
            if (node == null || node is EnumCnst)
                return true;
            if (node is Var)
            {
                string name = ((Var)node).Name;
                if (name.Length > 0 && name[0] == '%')
                    return true;
                name = name.Split(new char[] { '.' })[0];
                return definedVars.Contains(new Var(name));
            }
            else if (node is RelConstr)
            {
                var rel = node as RelConstr;
                if (rel.Op == RelKind.Eq)
                {
                    return TermWellDefined(rel.Arg1, gdDecls, definedVars) ||
                           TermWellDefined(rel.Arg2, gdDecls, definedVars);
                }
                else if (rel.Op == RelKind.Typ)
                {
                    return TermWellDefined(rel.Arg1, gdDecls, definedVars);
                }
                else if (rel.Op == RelKind.No)
                    return false;
                else
                {
                    return TermWellDefined(rel.Arg1, gdDecls, definedVars) &&
                           TermWellDefined(rel.Arg2, gdDecls, definedVars);
                }                
            }
            else if (node is Find)
            {
                var find = node as Find;
                if (find.Match is FuncTerm)
                {
                    var func = find.Match as FuncTerm;
                    if (IsGoalDrivenFuncTerm(func, gdDecls))
                        return TermWellDefined(func, gdDecls, definedVars);                
                }
                return true;
            }
            else if (node is FuncTerm)
            {
                var func = node as FuncTerm;
                if (IsGoalDrivenFuncTerm(func, gdDecls))
                {
                    var conDecl = GetGoalDrivenFuncTermDecl(func, gdDecls);
                    int co = 0;
                    foreach (var child in func.args)
                    {
                        if (co < conDecl.CodomainFieldStart && 
                            !TermWellDefined(child, gdDecls, definedVars))
                            return false;
                        co++;
                    }
                }
                else
                {
                    foreach (var child in func.args)
                        if (!TermWellDefined(child, gdDecls, definedVars))
                            return false;
                }
            }
            else 
                foreach (var child in node.Children)
                    if (!TermWellDefined(child, gdDecls, definedVars))
                        return false;
            return true;
        }

        /// <summary>
        /// Returns true if the node defines a wanted variable.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="gdDecls"></param>
        /// <param name="wantedVars"></param>
        /// <returns></returns>
        static bool TermNecessary(Node node, Dictionary<string, ConDecl> gdDecls, HashSet<Var> wantedVars)
        {
            if (node is Find)
            {
                var find = node as Find;
                if (find.Match is FuncTerm)
                {
                    var func = find.Match as FuncTerm;
                    if (IsGoalDrivenFuncTerm(func, gdDecls))
                    {
                        var conDecl = GetGoalDrivenFuncTermDecl(func, gdDecls);
                        int co = 0;
                        foreach (var child in func.args)
                        {
                            if (co >= conDecl.CodomainFieldStart)
                            {
                                var defVars = child.GetVariables();
                                if (wantedVars.Overlaps(defVars))
                                    return true;
                            }
                            co++;
                        }
                        return false;
                    }
                }
            }
            return true;
        }

        static ConDecl GetGoalDrivenFuncTermDecl(Node node, Dictionary<string, ConDecl> gdDecls)
        {
            var func = node as UserFuncTerm;
            if (func == null)
                return null;
            ConDecl conDecl;
            gdDecls.TryGetValue(func.Function.Name, out conDecl);
            return conDecl;
        }

        static bool IsGoalDrivenFuncTerm(Node node, Dictionary<string, ConDecl> gdDecls)
        {
            var func = node as UserFuncTerm;
            if (func == null)
                return false;
            if (gdDecls.ContainsKey(func.Function.Name))
                return true;
            return false;
        }

        static bool IsGoalDrivenConstraint(Node node, Dictionary<string, ConDecl> gdDecls)
        {
            var find = node as Find;
            if (find == null)
                return false;
            var func = find.Match as FuncTerm;
            if (func == null)
                return false;
            return IsGoalDrivenFuncTerm(func, gdDecls);
        }

        static bool TermDefinesVariables(Dictionary<string, ConDecl> gdDecls, Node node, ref HashSet<string> variables, bool addGoalDrivenTerms)
        {
            if (node is RelConstr)
            {
                foreach (var child in node.Children)
                {
                    var vars = new HashSet<string>(child.GetVariables().Select(x => x.Name));
                    if (vars.Overlaps(variables))
                        return true;
                }
                return false;
            }
            if (node is Find)
                return TermDefinesVariables(gdDecls, node.Children.First(), ref variables, addGoalDrivenTerms);
            if (node is UserFuncTerm)
            {
                UserFuncTerm func = (UserFuncTerm)node;

                if (gdDecls.ContainsKey(func.Function.Name))
                {
                    return addGoalDrivenTerms;
                }
            }
            return true;
        }

        /// <summary>
        /// Create the goal-driven request term for a node.
        /// E.g., term(X) => (Y,Z) becomes _term(X).
        /// </summary>
        /// <param name="name"></param>
        /// <param name="removeFunctional"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        static UserFuncTerm CreateGoalTerm(string name, int removeFunctional, Node node)
        {
            var func = new UserFuncTerm(new Id(CreateGoalDrivenEvalName(name))) { Span = node.Span };
            if (node is FuncTerm)
            {
                List<Node> args = new List<Node>(((FuncTerm)node).args);
                for (int i = 0; i < removeFunctional; ++i)
                    if (args.Count>0)
                        args.RemoveAt(args.Count-1);
                foreach (var arg in args)
                    func.AddArg(arg);
            }
            else
                func.AddArg(node);
            return func;
        }                
        
        /// <summary>
        /// Handle goal-driven rules.
        /// If the head contains gd function, add eval to its bodies.
        /// If a body contains gd function, derive additional rules for extracting them.
        /// </summary>
        /// <param name="ast"></param>
        /// <param name="gdDecls"></param>
        /// <param name="modDecls"></param>
        /// <returns></returns>
        static bool HandleGoalDrivenTerms(Module ast, Dictionary<string,ConDecl> gdDecls, HashSet<string> constantDefs, IEnumerable<ConDecl> modDecls)
        {
            Contract.Requires(ast != null);
            
            // Create evaluation term for each goal-directed function in current module
            Dictionary<string, ConDecl> newDecls = new Dictionary<string, ConDecl>();
            foreach (var decl in modDecls)
            {
                var newdecl = ConDecl.Create(decl.Span, CreateGoalDrivenEvalName(decl.Name), false, null);
                List<Field> fields =
                    decl.IsGoalDriven ?
                        new List<Field>(decl.Fields.Take(decl.CodomainFieldStart)) :
                        new List<Field>(decl.Fields);                        
                foreach (var field in fields)
                {
                    newdecl.Fields.Add(new Field(null, field.Type, false) { Span = decl.Span });
                }
                newDecls.Add(decl.Name, newdecl);
            }
            foreach (var typedecl in newDecls.Values)
                ast.TypeDecls.Add(typedecl);

            // add eval to bodies of gd-terms (from lhs to rhs)
            foreach (var r in ast.Rules)
            {
                List<Tuple<string, int, Node>> list = new List<Tuple<string, int, Node>>();
                foreach (var h in r.Heads)
                    ExtractGoalDirectedTerms(gdDecls, h, ref list);
                foreach (var b in r.Bodies)
                    foreach (var tuple in list)
                    {
                        var func = CreateGoalTerm(tuple.Item1, tuple.Item2, tuple.Item3);
                        var find = new Find(null, func) { Span = func.Span };
                        b.AddConstr(find, false);
                    }
                // if body is empty, add it
                if (r.Bodies.Count == 0)
                {
                    foreach (var tuple in list)
                    {
                        var func = CreateGoalTerm(tuple.Item1, tuple.Item2, tuple.Item3);
                        var find = new Find(null, func) { Span = func.Span };
                        var body = new Body() { Span = find.Span };
                        body.AddConstr(find, false);
                        r.AddBody(body);
                    }
                }
            }
                        
            // extract evals from RHS of rules (rhs to lhs)
            List<RuleNode> newRules = new List<RuleNode>();
            foreach (var rule in ast.Rules)
            {
                foreach (var body in rule.Bodies)
                {
                    List<Tuple<string, int, Node>> list = new List<Tuple<string, int, Node>>();
                    ExtractGoalDirectedTerms(gdDecls, body, ref list);
                    //int co = 1;
                    foreach (var tuple in list)
                    {
                        var newrule = new RuleNode() { Span = rule.Span };
                        var node = tuple.Item3;
                        newrule.AddHead(CreateGoalTerm(tuple.Item1, tuple.Item2, node));
                        var newbody = new Body() { Span = body.Span };

                        // List of variables on the LHS => wanted
                        var wantedVars = newrule.GetVariables();
#if DEBUG_GS
                        Console.Write("{0}. Evaluating... ", node.Span.StartLine);
                        Console.WriteLine(node);
                        Console.WriteLine();
                        Console.Write("  Wanted vars:");
                        foreach (var v in wantedVars)
                            Console.Write(" {0}", v);
                        Console.WriteLine();
#endif
                        
                        // Step 1. add all the not goal-driven finds to the body, and simultaneously collect the other constraints, as well as the defined variables.
                        List<Node> otherConstraints = new List<Node>();
                        HashSet<Var> defVars = new HashSet<Var>();
                        //bool addMore = true;
                        foreach (var c in body.Children)
                        {
                            if (node is Find &&
                                !IsGoalDrivenConstraint(c, gdDecls))
                            {
                                newbody.AddConstr(c);
                                defVars.UnionWith(c.GetVariables());
#if DEBUG_GS
                                Console.Write("  Add1... ");
                                Console.WriteLine(c);
                                Console.WriteLine();
                                Console.Write("  Well-defined vars:");
                                foreach (var v in defVars)
                                    Console.Write(" {0}", v);
                                Console.WriteLine();
#endif
                            }
                            else
                            {
                               otherConstraints.Add(c);
                            }
                        }

                        // WAS: Step 2. cycle through the other constraints, and try to add them one by one. Stop if nothing can be done.
                        // Step 2. cycle through the other constraints, and add everything up to the current term.
                        int counter = otherConstraints.Count;
                        while (counter > 0)
                        {
                            bool found = false;
                            bool foundCurrentTerm = false;
                            var e = otherConstraints.GetEnumerator();
                            while (e.MoveNext())
                            {
                                if (IsGoalDrivenConstraint(e.Current, gdDecls))
                                {
                                    //if (TermNecessary(e.Current, gdDecls, wantedVars) &&
                                        //!ContainsTerm(e.Current, node))
                                    if (!ContainsTerm(e.Current, node) && !foundCurrentTerm)
                                    {
                                        found = true;
                                        GetGoalDrivenVariables(e.Current, gdDecls, ref defVars, ref wantedVars);
                                        break;
                                    }
                                    else
                                    {
                                        foundCurrentTerm = true;
#if DEBUG_GS
                                        Console.Write("  Goal-driven term not necessary... ");                                    
#endif
                                    }
                                }
                                else
                                {
                                    if (TermWellDefined(e.Current, gdDecls, defVars) &&
                                        !ContainsTerm(e.Current, node))
                                    {
                                        defVars.UnionWith(e.Current.GetVariables());
                                        found = true;
                                        break;
                                    }
                                    else
                                    {
#if DEBUG_GS
                                        Console.Write("  Term not defined... ");
#endif
                                    }
                                }
                                if (!found)
                                {
#if DEBUG_GS
                                    Console.WriteLine(e.Current);
                                    Console.WriteLine();
                                    Console.Write("  Well-defined vars:");
                                    foreach (var v in defVars)
                                        Console.Write(" {0}", v);
                                    Console.WriteLine();
#endif
                                }
                            }
                            if (found)
                            {
                                newbody.AddConstr(e.Current);
                                otherConstraints.Remove(e.Current);
                                --counter;

#if DEBUG_GS
                                Console.Write("  Add2... ");
                                Console.WriteLine(e.Current);
                                Console.WriteLine();
                                Console.Write("  Well-defined vars:");
                                foreach (var v in defVars)
                                    Console.Write(" {0}", v);
                                Console.WriteLine();
#endif
                            }
                            else
                            {
                                var varsToDefine = new HashSet<Var>(wantedVars);
                                varsToDefine.ExceptWith(defVars);
                                if (varsToDefine.Count > 0)
                                {
                                    StringBuilder sb = new StringBuilder("Cannot compile: \r\n");
                                    StringWriter sw = new StringWriter(sb);
                                    Console.WriteLine(rule);
                                    throw new Exception(sb.ToString());
                                }
                                    /*
                                {
                                    flags.Add(new Flag(
                                        SeverityKind.Error,
                                        r,
                                        String.Format("Cannot rewrite rule at {0}", r.Span.StartLine),
                                        80085));
                                }*/
                                break;
                            }
                        }
                        if (newbody.constraints.Count>0)
                            newrule.AddBody(newbody);
                                                
                        // if every variable is defined, add rule
                        //if (vars.Count == 0)
                            newRules.Add(newrule);
                    }
                }
            }

            // add new rules
            foreach (var rule in newRules)
                ast.AddRule(rule);

            // Define error-checking terms...
            UnnDecl errorTerms = new UnnDecl(GoalDrivenErrorCheckTerms, false, null);
            UnnDecl nondeterministicTerms = new UnnDecl(GoalDrivenNondeterminismCheckTerms, false, null);
            foreach (var typedecl in newDecls.Values)
            {
                // add error checking
                {
                    RuleNode rule = new RuleNode();
                    UserFuncTerm func = new UserFuncTerm(new Id(GoalDrivenErrorCheck));
                    Var X = new Var("X");
                    func.AddArg(X);
                    rule.AddHead(func);
                    errorTerms.AddComponent(new Id(typedecl.Name));

                    UserFuncTerm match = new UserFuncTerm(new Id(typedecl.Name));
                    int co = 0;
                    foreach (var field in typedecl.Fields)
                    {
                        co++;
                        Var arg = new Var("X" + co);
                        match.AddArg(arg);
                    }
                    ConDecl orig = gdDecls[typedecl.Name.Substring(GoalDrivenID.Length)];
                    UserFuncTerm nomatch = new UserFuncTerm(new Id(orig.Name));
                    int co2 = 0;
                    foreach (var field in orig.Fields)
                    {
                        co2++;
                        Var arg;
                        if (co2 > co)
                            arg = new Var("_");
                        else
                            arg = new Var("X" + co2);
                        nomatch.AddArg(arg);
                    }

                    Body body = new Body();
                    body.AddConstr(new Find(X, match));
                    Compr compr = new Compr();
                    Body bodynomatch = new Body();
                    bodynomatch.AddConstr(new Find(null, nomatch));
                    compr.AddHead(new BoolCnst(true));
                    compr.AddBody(bodynomatch);
                    body.AddConstr(new RelConstr(RelKind.No, compr));

                    rule.AddBody(body);
                    ast.AddRule(rule);
                }

                // add nondeterminism checking
                {
                    ConDecl orig = gdDecls[typedecl.Name.Substring(GoalDrivenID.Length)];
                    nondeterministicTerms.AddComponent(new Id(orig.Name));

                    UserFuncTerm match1 = new UserFuncTerm(new Id(orig.Name));
                    UserFuncTerm match2 = new UserFuncTerm(new Id(orig.Name));
                    int co = 0;
                    foreach (var field in orig.Fields)
                    {
                        co++;
                        if (co <= orig.CodomainFieldStart)
                        {
                            match1.AddArg(new Var("X" + co));
                            match2.AddArg(new Var("X" + co));
                        }
                        else
                        {
                            match1.AddArg(new Var("Y" + co));
                            match2.AddArg(new Var("Z" + co));
                        }
                    }

                    for (int i = orig.CodomainFieldStart+1; i <= orig.Fields.Count; ++i)
                    {
                        Body body = new Body();
                        body.AddConstr(new Find(new Var("X"), match1));
                        body.AddConstr(new Find(null, match2));
                        body.AddConstr(new RelConstr(RelKind.Neq, new Var("Y" + i), new Var("Z" + i)));

                        var func = new UserFuncTerm(new Id(GoalDrivenNondeterminismCheck));
                        func.AddArg(new Var("X"));

                        RuleNode rule = new RuleNode();
                        rule.AddHead(func);
                        rule.AddBody(body);
                        ast.AddRule(rule);
                    }
                }
            }
            if (errorTerms.components.Count > 0)
            {
                ast.TypeDecls.Add(errorTerms);
                var conDecl = ConDecl.Create(Span.Unknown, GoalDrivenErrorCheck, false, null);
                conDecl.Fields.Add(new Field(null,new Id(GoalDrivenErrorCheckTerms),false));
                ast.TypeDecls.Add(conDecl);
            }
            if (nondeterministicTerms.components.Count > 0)
            {
                ast.TypeDecls.Add(nondeterministicTerms);
                var conDecl = ConDecl.Create(Span.Unknown, GoalDrivenNondeterminismCheck, false, null);
                conDecl.Fields.Add(new Field(null, new Id(GoalDrivenNondeterminismCheckTerms), false));
                ast.TypeDecls.Add(conDecl);
            }
            //Factory.Instance.ToAST(ast).Print(Console.Out);

            return true;
        }

        #region Functional language extension
        static Node ReplaceFunctionalTermsInsideOut(Node node, Dictionary<string,ConDecl> denDecls, ref LinkedList<Find> addToRule)
        {
            if (node is UserFuncTerm)
            {
                Node replacement = node;
                var func = (UserFuncTerm)node;
                ConDecl decl;
                if (denDecls.TryGetValue(func.Name, out decl))
                {
                    if (func.args.Count == decl.Fields.Count - 1)
                    {
                        Var var = new Var(String.Format("#var_{0}", Math.Abs(func.GetHashCode()))) { Span = func.Span };
                        func.AddArg(var);
                        addToRule.AddLast(new Find(null, func) { Span = func.Span });
                        replacement = var;
                    }
                }
                for (int i = 0; i < func.args.Count; ++i)
                {
                    func.args[i] = ReplaceFunctionalTermsInsideOut(func.args[i], denDecls, ref addToRule);
                }
                return replacement;
            }
            else if (node is Find)
            {
                var find = (Find)node;
                find.Match = ReplaceFunctionalTermsInsideOut(find.Match, denDecls, ref addToRule);
                return node;
            }
            else
            {
                foreach (var child in node.Children)
                    ReplaceFunctionalTermsInsideOut(child, denDecls, ref addToRule);
                return node;
            }
        }

        /// <summary>
        ///  Allows one to use FORMULA as a functional language. If a term has less parameters than necessary, its output is substituted into its place.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="denDecls"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        static bool HandleFunctionalTerms(Module module, Dictionary<string, ConDecl> denDecls)
        {
            bool succeeded = true;
            foreach (var rule in module.Rules)
            {   
                // replace functional terms in head
                foreach (var head in rule.Heads)
                {
                    var addToRule = new LinkedList<Find>();
                    ReplaceFunctionalTermsInsideOut(head, denDecls, ref addToRule);
                    foreach (var add in addToRule)
                        foreach (var body in rule.Bodies)
                            body.AddConstr(add);
                    /*if (denDecls.ContainsKey(head.Name))
                    {
                        var func = (UserFuncTerm)head;
                        if (rule.Bodies.Count == 0)
                            rule.Bodies.Add(new Body());
                        foreach (var body in rule.Bodies)
                            body.AddConstr(new Find((Var)func.args[0], new Id(func.args[0].Name)));
                    }*/
                }
                // replace functional terms in bodies
                foreach (var body in rule.Bodies)
                {
                    var addToRule = new LinkedList<Find>();
                    ReplaceFunctionalTermsInsideOut(body, denDecls, ref addToRule);
                }
            }

            return succeeded;
        }
        #endregion

        /// <summary>
        ///  Automatically expands rules where the LHS term is a denotational formula.
        ///  Renames the LHS variable, and adds variable definition on the RHS.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="denDecls"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        static bool HandleDenotationalHeads(Module module, Dictionary<string, ConDecl> denDecls)
        {
            bool succeeded = true;
            foreach (var rule in module.Rules)
            {
                // add extra constraints to denotational rules
                HashSet<string> added = new HashSet<string>();
                foreach (var head in rule.Heads)
                {
                    if (denDecls.ContainsKey(head.Name) && head.args[0] is Var)
                    {
                        var subst = new Dictionary<string,string>();
                        string typename = head.args[0].Name;
                        string varname = "#" + typename;
                        subst.Add(typename, varname);
                        head.ReplaceNames(subst); 
                        var constr = new Find((Var)head.args[0], new Id(typename){ Span = head.args[0].Span});
                        if (rule.Bodies.Count == 0)
                            rule.AddBody(new Body() { Span = constr.Span });
                        if (!added.Contains(head.Name))
                        {
                            foreach (var body in rule.Bodies)
                            {
                                body.ReplaceNames(subst);
                                body.AddConstr(constr);
                            }
                            added.Add(head.Name);
                        }                        
                    }
                }
            }

            return succeeded;
        }

        /// <summary>
        ///  Add rules for instantiating the terms on the RHS of denotational equations.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="denDecls"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        static bool HandleDenotationalTypes(Module module, Dictionary<string, ConDecl> denDecls)
        {
            bool succeeded = true;
            foreach (var typeDecl in denDecls.Values)
            {
                // Instantiate each field
                for (int i = typeDecl.CodomainFieldStart; i < typeDecl.Fields.Count; ++i)
                {
                    var name = typeDecl.Fields[i].Type.Name;
                    RuleNode rule = new RuleNode();
                    var ipos = name.LastIndexOf('.');
                    UserFuncTerm func;
                    if (ipos != -1)
                        func = new UserFuncTerm(new Id(name.Substring(0, ipos) + "._"));
                    else
                        func = new UserFuncTerm(new Id("_"));
                    func.AddArg(new Var("X"));
                    rule.AddHead(func);
                    var bodyfunc = new UserFuncTerm(new Id(typeDecl.Name));
                    for (int j = 0; j < typeDecl.Fields.Count; ++j)
                    {
                        if (j != i)
                            bodyfunc.AddArg(new Var("_"));
                        else
                            bodyfunc.AddArg(new Var("X"));
                    }
                    rule.AddBody(new Body(new Find(null,bodyfunc)));
                    module.AddRule(rule);
                }
            }

            return succeeded;
        }
        
        /// <summary>
        /// If a goal driven term has less arguments as necessary, turns it into a request.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="gdDecls"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        static bool HandleGoalDrivenRequests(Module module, Dictionary<string, ConDecl> gdDecls)
        {
            bool succeeded = true;
            foreach (var rule in module.Rules)
            {
                // replace in head
                foreach (var head in rule.Heads)
                {
                    var funcTerm = head as UserFuncTerm;
                    if (funcTerm == null)
                        continue;
                    
                    ConDecl decl = GetGoalDrivenFuncTermDecl(funcTerm, gdDecls);
                    if (decl != null && decl.IsGoalDriven)
                    {
                        if (funcTerm.args.Count == decl.CodomainFieldStart)
                        {
                            var newName = CreateGoalDrivenEvalName(decl.Name);
                            funcTerm.Function = new Id(newName) { Span = funcTerm.Span };
                        }
                    }
                }
                // replace in bodies
                foreach (var body in rule.Bodies)
                {
                    foreach (var constr in body.constraints)
                    {
                        var find = constr as Find;
                        if (find == null)
                            continue;
                        var funcTerm = find.Match as UserFuncTerm;
                        if (funcTerm == null)
                            continue;                        
                        ConDecl decl = GetGoalDrivenFuncTermDecl(funcTerm, gdDecls);
                        if (decl != null && decl.IsGoalDriven)
                        {
                            if (funcTerm.args.Count == decl.CodomainFieldStart)
                            {
                                var newName = CreateGoalDrivenEvalName(decl.Name);
                                funcTerm.Function = new Id(newName) { Span = funcTerm.Span };
                            }
                        }
                    }
                }
            }

            return succeeded;
        }

        /// <summary>
        /// Extract all the constants below a node.
        /// </summary>
        /// <param name="typedecl"></param>
        /// <param name="constantDefs"></param>
        static void ExtractConstantDefs(Node typedecl, ref HashSet<string> constantDefs)
        {
            if (typedecl is ConDecl || typedecl is UnnDecl || typedecl is Field)
            {
                foreach (var f in typedecl.Children)
                    ExtractConstantDefs(f, ref constantDefs);
            }
            else if (typedecl is FormulaBe.Nodes.EnumDecl)
            {
                var unn = typedecl as FormulaBe.Nodes.EnumDecl;
                foreach (var def in unn.Elements)
                {                    
                    constantDefs.Add(def.Name);
                }
            }
        }

        /// <summary>
        /// Extract goal-driven type declarations and constant defitions in a module.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="module"></param>
        /// <param name="constantDefs"></param>
        /// <returns></returns>
        static Dictionary<string, ConDecl> ExtractTypeDecls(string prefix, Module module, ref HashSet<string> constantDefs, Func<ConDecl,bool> pred)
        {
            Dictionary<string, ConDecl> result = new Dictionary<string, ConDecl>();
            foreach (var typedecl in module.TypeDecls)
            {
                ConDecl decl = typedecl as ConDecl;
                if (decl != null && pred(decl))
                {
                    if (!result.ContainsKey(decl.Name))
                    {
                        result.Add(prefix + decl.Name, decl);
                    }
                }
                ExtractConstantDefs(typedecl, ref constantDefs);
            }
            return result;
        }

        /// <summary>
        /// Extract goal-driven type declarations and constant defitions in a module, as well as in all the referenced modules.
        /// </summary>
        /// <param name="programs"></param>
        /// <param name="declrs"></param>
        /// <param name="constantDefs"></param>
        /// <param name="prefix"></param>
        /// <param name="module"></param>
        public static void ExtractTypeDecls(List<Module> modules, Dictionary<string, ConDecl> declrs, ref HashSet<string> constantDefs, string prefix, Module module, Func<ConDecl,bool> pred)
        {
            // Create a dictionary to access domain definitions by name.
            Dictionary<String, Domain> domDefs = new Dictionary<string, Domain>();
            foreach (var mod in modules)
            {
                var ddom = mod as Domain;
                if (ddom != null && !domDefs.ContainsKey(ddom.Name))
                    domDefs.Add(ddom.Name, ddom);
            }

            // Extract declarations from referenced modules.
            foreach (var mod in module.ModRefs)
            {
                Domain composition = domDefs[mod.Name];                
                string newPrefix = null;
                if (String.IsNullOrEmpty(mod.Rename))
                    newPrefix = prefix;
                else
                {
                    if (String.IsNullOrEmpty(prefix))
                        newPrefix = mod.Rename + ".";
                    else
                        newPrefix = prefix + mod.Rename + ".";
                }
                ExtractTypeDecls(modules, declrs, ref constantDefs, newPrefix, composition, pred);
            }
            // Extract declarations from current module.
            var newDecls = ExtractTypeDecls(prefix, module, ref constantDefs, pred);
            // remove shadowed declarations
            foreach (var decl in module.TypeDecls)
            {
                string name =
                    (decl is ConDecl) ? ((ConDecl)decl).Name :
                    (decl is UnnDecl) ? ((UnnDecl)decl).Name : null;
                if (name != null && declrs.ContainsKey(name))
                    declrs.Remove(name);
            }
            foreach (var decl in newDecls)
                declrs.Add(decl.Key, decl.Value);
        }

        private static RuleNode CreateNotFunctionalRule(ConDecl condecl)
        {
            var rule = new RuleNode();
            rule.AddHead(new UserFuncTerm(new Id("#notFunctional_" + condecl.Name)));
            for (int i=condecl.CodomainFieldStart; i<condecl.Fields.Count; ++i)
            {
                var body = new Body();
                var func1 = new UserFuncTerm(new Id(condecl.Name));
                var func2 = new UserFuncTerm(new Id(condecl.Name));
                // Same domain
                for (int j=0; j<condecl.CodomainFieldStart; ++j)
                {
                    func1.AddArg(new Var("x" + j));
                    func2.AddArg(new Var("x" + j));
                }
                // But image differs at least in one value
                for (int j=condecl.CodomainFieldStart; j<condecl.Fields.Count; ++j)
                {
                    if (i == j)
                    {
                        func1.AddArg(new Var("diff1"));
                        func2.AddArg(new Var("diff2"));
                    }
                    else
                    {
                        func1.AddArg(new Var("_"));
                        func2.AddArg(new Var("_"));
                    }
                }
                body.AddConstr(new Find(null, func1));
                body.AddConstr(new Find(null, func2));
                body.AddConstr(new RelConstr(RelKind.Neq, new Var("diff1"), new Var("diff2")));
                rule.AddBody(body);
            }
            return rule;
        }
        private static RuleNode CreateNotTotalRule(ConDecl condecl)
        {
            var rule = new RuleNode();
            rule.AddHead(new UserFuncTerm(new Id("#notTotalFunction_" + condecl.Name)));
            var body = new Body();
            // Instantiate domain
            for (int j = 0; j < condecl.CodomainFieldStart; ++j)
                body.AddConstr(new Find(new Var("x"+j), condecl.Fields[j].Type));
            // No corresponding image
            var func = new UserFuncTerm(new Id(condecl.Name));
            for (int j = 0; j < condecl.Fields.Count; ++j)
            {
                if (j < condecl.CodomainFieldStart)
                    func.AddArg(new Var("x" + j));
                else
                    func.AddArg(new Var("_"));
            }
            var compr = new Compr();
            compr.AddHead(new BoolCnst(true));
            compr.AddBody(new Body(new Find(null,func)));
            body.AddConstr(new RelConstr(RelKind.No, compr));
            rule.AddBody(body);
            return rule;
        }
        private static RuleNode CreateNotInjectionRule(ConDecl condecl)
        {
            var rule = new RuleNode();
            rule.AddHead(new UserFuncTerm(new Id("#notInjection_" + condecl.Name)));
            for (int i = 0; i < condecl.CodomainFieldStart; ++i)
            {
                var body = new Body();
                var func1 = new UserFuncTerm(new Id(condecl.Name));
                var func2 = new UserFuncTerm(new Id(condecl.Name));
                // Same range
                for (int j = condecl.CodomainFieldStart; j < condecl.Fields.Count;  ++j)
                {
                    func1.AddArg(new Var("x" + j));
                    func2.AddArg(new Var("x" + j));
                }
                // But domain differs at least in one value
                for (int j = 0; j < condecl.CodomainFieldStart; ++j)
                {
                    if (i == j)
                    {
                        func1.AddArg(new Var("diff1"));
                        func2.AddArg(new Var("diff2"));
                    }
                    else
                    {
                        func1.AddArg(new Var("_"));
                        func2.AddArg(new Var("_"));
                    }
                }
                body.AddConstr(new Find(null, func1));
                body.AddConstr(new Find(null, func2));
                body.AddConstr(new RelConstr(RelKind.Neq, new Var("diff1"), new Var("diff2")));
                rule.AddBody(body);
            }
            return rule;
        }
        private static RuleNode CreateNotSurjectionRule(ConDecl condecl)
        {
            var rule = new RuleNode();
            rule.AddHead(new UserFuncTerm(new Id("#notSurjection_" + condecl.Name)));
            var body = new Body();
            // Instantiate codomain
            for (int j = condecl.CodomainFieldStart; j < condecl.Fields.Count; ++j)
                body.AddConstr(new Find(new Var("x" + j), new Id(condecl.Fields[j].Name)));
            // No corresponding domain
            var func = new UserFuncTerm(new Id(condecl.Name));
            for (int j = 0; j < condecl.Fields.Count; ++j)
            {
                if (j >= condecl.CodomainFieldStart)
                    func.AddArg(new Var("x" + j));
                else
                    func.AddArg(new Var("_"));
            }
            var compr = new Compr();
            compr.AddHead(new BoolCnst(true));
            compr.AddBody(new Body(new Find(null,func)));
            body.AddConstr(new RelConstr(RelKind.No, compr));
            rule.AddBody(body);
            return rule;
        }

        public static bool AddSyntacticSugar(List<Module> modules)
        {   
            foreach (var module in modules)
            {
                if (!(module is Domain || module is Transform))
                    continue;
                var enumcnsts = module.GetNodesOfType<EnumCnst>().Select(x => x.Cnst);
                HashSet<string> constantDefs = new HashSet<string>(enumcnsts);
                constantDefs.Add("TRUE");
                constantDefs.Add("FALSE");
                Dictionary<string, ConDecl> modDecls = ExtractTypeDecls(null, module, ref constantDefs, x => x.IsGoalDriven);
                Dictionary<string, ConDecl> gdDecls = new Dictionary<string, ConDecl>();
                Dictionary<string, ConDecl> denDecls = new Dictionary<string, ConDecl>();
                ExtractTypeDecls(modules, gdDecls, ref constantDefs, null, module, x => x.IsGoalDriven);
                ExtractTypeDecls(modules, denDecls, ref constantDefs, null, module, x => x.IsDenotational);

                // extract parameters of Transformation
                if (module is Transform)
                {
                    var t = (Transform)module;
                    foreach (var input in t.Inputs)
                        if (!(input.Type is ModRef))
                            constantDefs.Add("%"+input.Name);
                }

                if (!HandleDenotationalHeads(module, denDecls))
                    return false;
                if (!HandleDenotationalTypes(module, denDecls))
                    return false;
                if (!HandleGoalDrivenRequests(module, gdDecls))
                    return false;
                if (!HandleFunctionalTerms(module, denDecls))
                    return false;
                if (!HandleGoalDrivenTerms(module, gdDecls, constantDefs, modDecls.Values))
                    return false;
                if (module is Domain)
                {
                    // Add rules and conform statements for data constructors (functional, bijective, etc.)
                    List<RuleNode> notFunctional = new List<RuleNode>();
                    List<RuleNode> notInjective = new List<RuleNode>();
                    List<RuleNode> notSurjective = new List<RuleNode>();
                    List<RuleNode> notTotal = new List<RuleNode>();
                    foreach (var typeDecl in module.TypeDecls)
                    {
                        var condecl = typeDecl as ConDecl;
                        if (condecl != null && condecl.IsMap)
                        {
                            if (condecl.Kind == MapKind.Fun)
                                notFunctional.Add(CreateNotFunctionalRule(condecl));
                            else if (condecl.Kind == MapKind.Inj)
                                notInjective.Add(CreateNotInjectionRule(condecl));
                            else if (condecl.Kind == MapKind.Sur)
                                notSurjective.Add(CreateNotSurjectionRule(condecl));
                            else if (condecl.Kind == MapKind.Bij)
                            {
                                notInjective.Add(CreateNotInjectionRule(condecl));
                                notSurjective.Add(CreateNotSurjectionRule(condecl));
                            }
                            if (!condecl.IsPartial)
                                notTotal.Add(CreateNotTotalRule(condecl));
                        }
                    }
                    var dom = (Domain)module;
                    var item = new ContractItem(ContractKind.ConformsProp);
                    foreach (var rule in notFunctional)
                    {
                        var body = new Body();
                        body.AddConstr(new Find(null, new Id(rule.Heads[0].Name)));
                        item.specification.Add(body);
                        module.AddRule(rule);
                    }
                    foreach (var rule in notInjective)
                    {
                        var body = new Body();
                        body.AddConstr(new Find(null, new Id(rule.Heads[0].Name)));
                        item.specification.Add(body);
                        module.AddRule(rule);
                    }
                    foreach (var rule in notSurjective)
                    {
                        var body = new Body();
                        body.AddConstr(new Find(null, new Id(rule.Heads[0].Name)));
                        item.specification.Add(body);
                        module.AddRule(rule);
                    }
                    foreach (var rule in notTotal)
                    {
                        var body = new Body();
                        body.AddConstr(new Find(null, new Id(rule.Heads[0].Name)));
                        item.specification.Add(body);
                        module.AddRule(rule);
                    }
                    //dom.Conforms.Add(item);
                }
                if (module is Domain)
                {
                    var dom = (Domain)module;

                    var rule = new RuleNode();
                    var body = new Body();
                    rule.AddHead(new UserFuncTerm(new Id(module.Name + ".conforms")));
                    rule.AddBody(body);
                    module.AddRule(rule);
                    // Conform to extended domains
                    if (dom.ComposeKind == ComposeKind.Extends)
                        foreach (var comp in dom.Compositions)
                            body.AddConstr(new Find(null, new Id(comp.Name + ".conforms")));
                    // Conform to every conform statement
                    foreach (var con in dom.Conforms)
                        if (con.ContractKind == ContractKind.ConformsProp)
                            foreach (var conspec in con.specification)
                                foreach (var constr in conspec.constraints)
                                    body.AddConstr(constr);

                    // Conform to domain - if model
                    if (module is Model)
                        body.AddConstr(new Find(null, new Id(((Model)module).Domain.Name + ".conforms")));
                    if (body.constraints.Count == 0)
                        rule.Bodies.Clear();
                }
            }
            return true;
        }

        public static bool ResolveImports(List<Module> modules)
        {   
            // Create a dictionary to access domain definitions by name.
            Dictionary<String, Domain> domDefs = new Dictionary<string, Domain>();
            foreach (var module in modules)
            {
                var ddom = module as Domain;
                if (ddom != null && !domDefs.ContainsKey(ddom.Name))
                    domDefs.Add(ddom.Name, ddom);
            }

            // Iterate through each domain.
            foreach (var dom in domDefs.Values)
            {                
                // Iterate through each imported domain.
                foreach (var imp in dom.Imports)
                {
                    // Find the corresponding domain definition for the imported module.
                    Domain impdom;
                    if (domDefs.TryGetValue(imp.Name, out impdom))
                    {
                        // Copy each type declaration.
                        foreach (var typeDecl in impdom.TypeDecls)
                        {
                            dom.TypeDecls.Insert(0,typeDecl);
                        }
                        // Copy each rule.
                        foreach (var rule in impdom.Rules)
                        {
                            dom.Rules.Insert(0,rule);
                        }
                    }                    
                }
            }

            return true;
        }

        public static bool ResolveUnionExtensions(List<Module> modules)
        {
            // Extract all the domains
            List<Domain> domDefs = new List<Domain>();
            foreach (var module in modules)
            {
                var ddom = module as Domain;
                if (ddom != null)
                    domDefs.Add(ddom);
            }

            foreach (var ddom in domDefs)
            {
                // find union declarations
                Dictionary<string, UnnDecl> unionDecls = new Dictionary<string,UnnDecl>();
                foreach (var typeDecl in ddom.TypeDecls)
                {
                    var unnDecl = typeDecl as UnnDecl;
                    if (unnDecl == null || unnDecl.IsExtension)
                        continue;
                    if (!unionDecls.ContainsKey(unnDecl.Name))
                        unionDecls.Add(unnDecl.Name, unnDecl);
                }
                // find union extensions
                List<TypeDecl> toAddDecls = new List<TypeDecl>();
                List<TypeDecl> toRemoveDecls = new List<TypeDecl>();
                foreach (var typeDecl in ddom.TypeDecls)
                {
                    var extensionDecl = typeDecl as UnnDecl;
                    if (extensionDecl == null || !extensionDecl.IsExtension)
                        continue;
                    toRemoveDecls.Add(extensionDecl);
                    UnnDecl unionDecl;
                    if (unionDecls.TryGetValue(extensionDecl.Name, out unionDecl))
                    {
                        UnnDecl union = unionDecl as UnnDecl;
                        if (union == null)
                        {
                            // Create type declaration with new body.
                            // Flag previous version for removal.
                            var newunionDecl = new UnnDecl(unionDecl.Name, false, null) { Span = unionDecl.Span };
                            foreach (var comp in unionDecl.components)
                                newunionDecl.AddComponent(comp);
                            unionDecls[unionDecl.Name] = newunionDecl;

                            toAddDecls.Add(newunionDecl);
                            toRemoveDecls.Add(unionDecl);                            
                        }
                        foreach (var child in extensionDecl.components)
                            union.AddComponent(child);
                    }
                    else
                    {
                        // Create type declaration without flagging 'extension'.
                        unionDecl = new UnnDecl(extensionDecl.Name, false, null) { Span = extensionDecl.Span };
                        foreach (var comp in extensionDecl.components)
                            unionDecl.AddComponent(comp);
                        toAddDecls.Add(unionDecl);
                        unionDecls.Add(unionDecl.Name, unionDecl);
                    }
                }
                // Add new declarations.
                foreach (var add in toAddDecls)
                    ddom.AddTypeDecl(add);
                // Remove the extension declarations, so they do not interfere with others.
                foreach (var rem in toRemoveDecls)
                    ddom.TypeDecls.Remove(rem);
            }

            return true;
        }
    }
}
