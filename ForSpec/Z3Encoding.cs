﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Z3;

namespace FormulaBe
{
    using Nodes;
    class Z3Encoding
    {
        Context context;
        Fixedpoint fixedpoint;
        Dictionary<string, TypeDecl> typeDecls = new Dictionary<string,TypeDecl>();
        Dictionary<string, Sort> sorts = new Dictionary<string,Sort>();
        Dictionary<string, FuncDecl> relations = new Dictionary<string, FuncDecl>();
        Dictionary<string, Expr> constants = new Dictionary<string, Expr>();

        //Dictionary<String, AlgebraicDataType> algebraic_data_types = new Dictionary<string, AlgebraicDataType>();
                
        class AlgebraicDataType
        {
            public class Constructor
            {
                public string name;
                public string[] fields;
                public Id[] ids;
                public Sort[] sorts;
            }
            public AlgebraicDataType(string name) { Name = name; }
            public string Name;
            public Sort sort;
            public FuncDecl rel_func;

            public List<Sort> union = new List<Sort>();

            public List<Constructor> con = new List<Constructor>();
            public void AddConstructor(string name, string[] fields, Id[] ids)
            {
                Constructor c = new Constructor();
                c.name = name;
                c.fields = fields;
                c.ids = ids;
                con.Add(c);
            }
            public void AddConstructor(string name, string[] fields, Sort[] sorts)
            {
                Constructor c = new Constructor();
                c.name = name;
                c.fields = fields;
                c.sorts = sorts;
                con.Add(c);
            }            
            public Sort Create(Context context, Dictionary<string, uint> sort_refs)
            {
                Microsoft.Z3.Constructor[] clist = new Microsoft.Z3.Constructor[con.Count];
                for (int i = 0; i < con.Count; ++i)
                {
                    uint[] refs = new uint[con[i].sorts.Length];
                    clist[i] = context.MkConstructor(
                                    con[i].name,
                                    "#is_" + con[i].name,
                                    con[i].fields,
                                    con[i].sorts,
                                    refs);
                }
                sort = context.MkDatatypeSort(Name, clist);
                rel_func = context.MkFuncDecl(Name, sort, context.BoolSort);
                return sort;
            }
        }
                
        public Z3Encoding(Module module)
        {
            context = new Context();
            Global.SetParameter("model", "true");
            //Global.SetParameter("pp.max_depth", "10");
            //Global.SetParameter("pp.min_alias_size", "1000000");            
            fixedpoint = context.MkFixedpoint();
            
            Dictionary<string, uint> typeDecls_idx = new Dictionary<string, uint>();
            
            // Extract all user defined ConDecl/MapDecl/UnnDecl types and assign an index to them.
            foreach (var typedecl in module.TypeDecls)
            {
                typeDecls.Add(typedecl.Name, typedecl);
                if (typedecl is ConDecl || typedecl is UnnDecl)
                    if (!typeDecls_idx.ContainsKey(typedecl.Name))
                        typeDecls_idx.Add(typedecl.Name, (uint)typeDecls_idx.Count);
            }

            // Map each built-in type and enum declaration to a sort.
            Dictionary<string, Sort> sort_map = new Dictionary<string, Sort>();
            sort_map.Add("Integer", context.IntSort);
            sort_map.Add("Real", context.RealSort);
            sort_map.Add("Boolean", context.BoolSort);
            foreach (var typedecl in module.TypeDecls)
            {
                if (typedecl is EnumDecl)
                {
                    var e = (EnumDecl)typedecl;
                    var sort = context.MkEnumSort(e.Name, e.Elements.Select(x => x.Name).ToArray());
                    sort_map.Add(e.Name, sort);
                    foreach (var c in sort.Consts)
                        constants.Add(c.ToString(), c);
                    //foreach (var el in e.Elements)
                        //constants.Add(el.Name, context.cons.MkConst(el.Name, sort));
                }
            }            
            // Add string sort
            {
                Constructor[] clist = new Constructor[2];
                Sort BV8 = context.MkBitVecSort(8);
                clist[0] = context.MkConstructor(
                                        "strApp",
                                        "#is_strApp",
                                        new string[] { "prefix", "last" },
                                        new Sort[] { null, BV8 },
                                        new uint[] { 0, 0 });
                clist[1] = context.MkConstructor(
                                        "strEmpty",
                                        "is_strEmpty");
                var StringSort = context.MkDatatypeSort("String", clist);
                sort_map.Add("String", StringSort);
            }

            // Create constructors for each ConDecl/MapDecl types.
            Dictionary<string, List<Microsoft.Z3.Constructor>> constructors = new Dictionary<string, List<Constructor>>();
            foreach (var typedecl in module.TypeDecls)
            {
                if (typedecl is ConDecl)
                {
                    var decl = (ConDecl)typedecl;
                    List<Microsoft.Z3.Constructor> clist;
                    if (!constructors.TryGetValue(decl.Name, out clist))
                    {
                        clist = new List<Microsoft.Z3.Constructor>();
                        constructors.Add(decl.Name, clist);
                    }

                    var field_names = new string[decl.Fields.Count];
                    var field_sorts = new Sort[decl.Fields.Count];
                    var field_sort_refs = new uint[decl.Fields.Count];
                    int i = 0;
                    foreach (var f in decl.Fields)
                    {
                        field_names[i] = f.Name;
                        if (!sort_map.TryGetValue(f.Type.Name, out field_sorts[i]) &&
                            !typeDecls_idx.TryGetValue(f.Type.Name, out field_sort_refs[i]))
                        {
                            throw new Exception(String.Format("Undefined sort type {0}: line {1}", f.Type.Name, f.Type.Span.StartLine));
                        }
                        ++i;
                    }
                    var c = context.MkConstructor(decl.Name, "#is_" + decl.Name, field_names, field_sorts, field_sort_refs);
                    clist.Add(c);
                }
            }
            // Create constructors for UnnDecls
            foreach (var typedecl in module.TypeDecls)
            {
                if (typedecl is UnnDecl)
                {
                    var decl = (UnnDecl)typedecl;
                    List<Microsoft.Z3.Constructor> clist;
                    if (!constructors.TryGetValue(decl.Name, out clist))
                    {
                        clist = new List<Microsoft.Z3.Constructor>();
                        constructors.Add(decl.Name, clist);
                    }

                    foreach (var f in decl.components)
                    {
                        Sort sort;
                        uint sort_ref;
                        if (sort_map.TryGetValue(f.Name, out sort))
                        {
                            var c = context.MkConstructor(
                                        decl.Name,
                                        "#is_" + decl.Name,
                                        new string[] { "" },
                                        new Sort[] { sort });
                            clist.Add(c);
                        }
                        else if (typeDecls_idx.TryGetValue(f.Name, out sort_ref))
                        {
                            var c = context.MkConstructor(
                                        decl.Name,
                                        "#is_" + decl.Name,
                                        new string[]{ "" },
                                        new Sort[] { null },
                                        new uint[] { sort_ref });
                            clist.Add(c);
                        }
                        else
                            throw new Exception(String.Format("Undefined sort type {0}: line {1}", f.Name, f.Span.StartLine));
                    }
                    /*List<Microsoft.Z3.Constructor> clist_add;
                    if (constructors.TryGetValue(f.Name, out clist_add))
                        clist.AddRange(clist_add);
                    else
                        throw new Exception(String.Format("Undefined type {0}: line {1}", f.Name, typedecl.Span.StartLine));*/                    
                }
            }
            string[] names = new string[constructors.Count];
            Constructor[][] cons = new Constructor[constructors.Count][];
            foreach (var kvp in constructors)
            {
                uint idx;
                if (typeDecls_idx.TryGetValue(kvp.Key, out idx))
                {
                    names[idx] = kvp.Key;
                    cons[idx] = kvp.Value.ToArray();
                }
            }
            DatatypeSort[] sorts = context.MkDatatypeSorts(names, cons);
            this.sorts = new Dictionary<string, Sort>(sort_map);
            foreach (var kvp in constructors)
            {
                uint idx;
                if (typeDecls_idx.TryGetValue(kvp.Key, out idx))
                {
                    this.sorts.Add(kvp.Key, sorts[idx]);                    
                }
            }

            foreach (var kvp in this.sorts)
            {
                var rel_func = context.MkFuncDecl(kvp.Key.ToUpper(), kvp.Value, context.BoolSort);
                fixedpoint.RegisterRelation(rel_func);                
                relations.Add(kvp.Key, rel_func);
            }
            
            foreach (var rule in module.Rules)
            {
                var rule_exprs = EncodeRule(rule);
                foreach (var rule_expr in rule_exprs)
                    fixedpoint.AddRule(rule_expr);
            }

            Console.WriteLine(fixedpoint);
            BoolExpr query = context.MkTrue();
            if (module is Domain)
            {
                var domain = (Domain)module;
                foreach (var spec in domain.Conforms)
                    foreach (var body in spec.specification)
                    {
                        var body_vars = new Dictionary<string, Sort>();
                        GetVariables(body, null, ref body_vars);
                        BoolExpr[] constrs = body.constraints.Select(x => EncodeConstraint(x, body_vars)).ToArray();
                        query = context.MkAnd(context.MkAnd(constrs));
                    }
            }
            Status result = fixedpoint.Query(query);
            Console.WriteLine(result);
            if (result == Status.SATISFIABLE)
            {
                var expr = fixedpoint.GetAnswer();
                Console.WriteLine(expr);
            }
        }

        private void GetVariables(Node node, Sort sort, ref Dictionary<string,Sort> vars)
        {
            if (node == null) return;
            if (node is Id)
            {
                var name = ((Id)node).Name;
                if (!constants.ContainsKey(name))
                    vars.Add(name,sort);
            }
            else if (node is UserFuncTerm)
            {
                var functerm = (UserFuncTerm)node;
                TypeDecl decl; 
                if (!typeDecls.TryGetValue(functerm.Function.Name, out decl))
                    throw new Exception(String.Format("Undefined type {0} used at {1}", functerm.Function.Name, functerm.Span.StartLine));
                if (decl is ConDecl)
                {
                    Field[] fields = ((ConDecl)decl).Fields.ToArray();
                    if (fields.Length != functerm.args.Count)
                        throw new Exception(String.Format("Invalid number of arguments in {0} at {1}", functerm, functerm.Span.StartLine));
                    int co = 0;
                    foreach (var arg in functerm.args)
                    {
                        Sort argsort;
                        if (sorts.TryGetValue(fields[co++].Type.Name, out argsort))
                            GetVariables(arg, argsort, ref vars);
                    }
                }
                else
                    throw new Exception(String.Format("Undefined type {0} used at {1}", decl, functerm.Span.StartLine));
            }
            else if (node is Find)
            {
                var find = (Find)node;
                Sort argsort;
                if ((find.Match is UserFuncTerm) &&
                    sorts.TryGetValue(((UserFuncTerm)find.Match).Function.Name, out argsort))
                {
                    GetVariables(find.Binding, argsort, ref vars);
                }
                else 
                    throw new Exception(String.Format("Unsupported term {0} used at {1}", find.Match, find.Span.StartLine));
                GetVariables(find.Match, null, ref vars);
            }
            else
                foreach (var child in node.Children)
                    GetVariables(child, null, ref vars);
        }

        private Expr EncodeTerm(Node node, Dictionary<string, Sort> vars)
        {
            if (node is Id)
            {
                var id = (Id)node;
                Sort sort;
                if (vars.TryGetValue(id.Name, out sort))
                    return context.MkConst(id.Name, sort);
                Expr expr;
                if (constants.TryGetValue(id.Name, out expr))
                    return expr;
            }
            else if (node is RationalCnst)
            {
                var cnst = (RationalCnst)node;
                return context.MkDiv(context.MkReal(cnst.Cnst.Numerator.ToString()), context.MkReal(cnst.Cnst.Denominator.ToString()));
            }
            else if (node is UserFuncTerm)
            {
                var functerm = (UserFuncTerm)node;
                var T = functerm.Function.Name;
                Sort sort;
                TypeDecl decl;
                if (sorts.TryGetValue(T, out sort) &&
                    typeDecls.TryGetValue(T, out decl) &&
                    sort is DatatypeSort &&
                    decl is ConDecl)
                {
                    var ds = (DatatypeSort)sort;
                    var cdecl = (ConDecl)decl;
                    Expr[] args = new Expr[cdecl.Fields.Count];
                    for (int i = 0; i<functerm.args.Count; ++i)
                    {
                        Sort argsort;
                        if (!sorts.TryGetValue(cdecl.Fields[i].Type.Name, out argsort))
                            throw new Exception(String.Format("Undefined sort {0}", cdecl.Fields[i].Type.Name));
                        var term = EncodeTerm(functerm.args[i], vars);
                        if (argsort is DatatypeSort &&
                            term.Sort != argsort)
                        {                            
                            var argds = (DatatypeSort)argsort;
                            foreach (var c in argds.Constructors)
                                if (c.Domain[0] == term.Sort)
                                {
                                    args[i] = c.Apply(term);
                                    break;
                                }                            
                            if (args[i]==null)
                                throw new Exception(String.Format("Undefined sort {0}", cdecl.Fields[i].Type.Name));
                        }
                        else
                            args[i] = EncodeTerm(functerm.args[i], vars);
                    }
                    return context.MkApp(ds.Constructors[0], args);
                }                    
            }
            throw new Exception(String.Format("Undefined data type {0} at {1}", node, node.Span.StartLine));
        }

        private BoolExpr EncodeRelation(Node node, Dictionary<string, Sort> vars)
        {
            Expr term = EncodeTerm(node, vars);
            if (node is UserFuncTerm)
            {
                var functerm = (UserFuncTerm)node;
                var T = functerm.Function.Name;
                FuncDecl rel;
                if (relations.TryGetValue(T, out rel))
                {
                    return (BoolExpr)rel.Apply(term);
                }
            }
            throw new Exception(String.Format("Undefined data type {0} at {1}", node, node.Span.StartLine));
        }

        private BoolExpr EncodeConstraint(Node node, Dictionary<string, Sort> vars)
        {
            if (node is Find)
            {
                var find = (Find)node;
                Expr term = EncodeTerm(find.Match, vars);

                if (find.Match is UserFuncTerm)
                {
                    var functerm = (UserFuncTerm)find.Match;
                    var T = functerm.Function.Name;
                    FuncDecl rel;
                    if (relations.TryGetValue(T, out rel))
                    {
                        BoolExpr relexpr = (BoolExpr)rel.Apply(term);
                        if (find.Binding == null)
                            return relexpr;
                        else
                            return context.MkAnd(relexpr, context.MkEq(EncodeTerm(find.Binding, vars), term));
                    }
                    else
                        throw new Exception(String.Format("Missing relation {0} at {1}", node, node.Span.StartLine));
                }
            }
            throw new Exception(String.Format("Unsupported constraint {0} at {1}", node, node.Span.StartLine));
        }

        private BoolExpr[] EncodeRule(RuleNode rule)
        {
            List<BoolExpr> result = new List<BoolExpr>();
            foreach (var head in rule.Heads)
            {
                var head_vars = new Dictionary<string,Sort>();
                GetVariables(head, null, ref head_vars);
                foreach (var body in rule.Bodies)
                {
                    var body_vars = new Dictionary<string,Sort>();
                    GetVariables(body, null,ref body_vars);

                    var undef = head_vars.Except(body_vars);
                    if (undef.Count() > 0)
                        throw new Exception(String.Format("The variables {0} are not defined for the rule at {1}", undef, head.Span.StartLine));

                    BoolExpr[] constrs = body.constraints.Select(x => EncodeConstraint(x, body_vars)).ToArray();
                    BoolExpr b_head = EncodeRelation(head, body_vars);
                    if (body_vars.Count > 0)
                        result.Add(
                            context.MkForall(
                                body_vars.Select(x => context.MkConst(x.Key, x.Value)).ToArray(),
                                context.MkImplies(context.MkAnd(constrs), b_head)));
                    else
                        result.Add(context.MkImplies(context.MkAnd(constrs), b_head));
                }
                // Add facts
                if (rule.Bodies.Count==0)
                {
                    result.Add(EncodeRelation(head, head_vars));
                }
            }
            return result.ToArray();
        }

        /*private Sort EncodeDatatype_memoize(Node node)
        {
            // memoization
            string name;
            if (node is Id)
                name = ((Id)node).Name;
            else if (node is Enum)
                name = String.Join("#", ((Enum)node).Elements.Select(f => ((Id)f).Name));
            else if (node is ConDecl)
                name = ((ConDecl)node).Name;
            else if (node is MapDecl)
                name = ((MapDecl)node).Name;
            else
                throw new Exception(String.Format("Unknown data type: {0} at {1}", node, node.Span.StartLine));

            AlgebraicDataType adt;
            if (algebraic_data_types.TryGetValue(name, out adt))
                return adt.sort;
            Sort sort = EncodeDatatype(node);
            if (sort != null)
                algebraic_data_types.Add(name, sort);
            return sort;
        } */               

        /// <summary>
        /// Returns the sort belonging to an Id
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        /*private Sort GetSort(Id id, Dictionary<string,uint> sort_refs)
        {            
            if (id.Name.Equals("Integer"))
                return context.IntSort;
            else if (id.Name.Equals("Boolean"))
                return context.BoolSort;
            else if (id.Name.Equals("Real"))
                return context.RealSort;
            else if (id.Name.Equals("String"))
            {
                Constructor[] clist = new Constructor[2];
                Sort BV8 = context.MkBitVecSort(8);
                clist[0] = context.MkConstructor(
                                        "strApp", 
                                        "#is_strApp", 
                                        new string[] {"prefix", "last"}, 
                                        new Sort[] {null,BV8},
                                        new uint[] {0,0});
                clist[1] = context.MkConstructor(
                                        "strEmpty",
                                        "is_strEmpty");                                            
                return context.MkDatatypeSort("String", clist);
            }
            return null;
        }*/
        /*
            else if (node is ConDecl)
            {
                var decl = (ConDecl)node;
                AlgebraicDataType adt = new AlgebraicDataType(decl.Name);
                return adt.Create(context);
            }
            else if (node is UnnDecl)
            {
                UnnDecl unn = (UnnDecl)node;
                AlgebraicDataType adt = new AlgebraicDataType(unn.Name);
                return adt.Create(context);
            }*/
            /*throw new Exception(String.Format("Unknown data type {0}: line {1}", node, node.Span.StartLine));
        }*/
    }
}
