﻿namespace FormulaBe
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Numerics;
    using QUT.Gppg;
    using Nodes;

    //using Microsoft.Formula.API;
    //using Microsoft.Formula.Common;
    using System.Text;

    public class FormulaBeParser
    {
        public static ParseResult ParseAll(string filename)
        {
            var result = new ParseResult();

            // Load all the references programs
            Dictionary<string,Program> programs = new Dictionary<string,Program>();
            ParseResult pr;
            result.ToLoad.Add(filename);
            while (result.ToLoad.Count > 0)
            {
                string toload = result.ToLoad.First();
                Console.WriteLine("Loading {0}", toload);
                if (Parse(toload, out pr))
                {                    
                    programs.Add(toload, pr.Program);
                    // Add to loaded files
                    result.Loaded.Add(toload);
                    // Add dependencies to be loaded
                    result.ToLoad.UnionWith(pr.ToLoad);
                    // Except files that are already loaded
                    result.ToLoad.ExceptWith(result.Loaded);
                }
                else
                {
                    throw new FlagException(pr.Flags);
                }
            }

            // Convert model facts to rules
            foreach (var program in programs.Values)
                foreach (var module in program.modules)
                    if (module is Model)
                    {
                        var model = (Model)module;
                                                
                        Dictionary<Id, UserFuncTerm> facts = new Dictionary<Id, UserFuncTerm>();
                        foreach (var fact in model.facts)
                            if (fact.Binding != null)
                                facts.Add(fact.Binding, fact.Match);
                            else
                                facts.Add(new Id(fact.Match.ToString()), fact.Match);
                        
                        // Order the facts topologically
                        Dictionary<Id, HashSet<Id>> factDeps = new Dictionary<Id, HashSet<Id>>();
                        Dictionary<Id, HashSet<Id>> invFactDeps = new Dictionary<Id, HashSet<Id>>();
                        foreach (var fact in facts)
                        {
                            factDeps[fact.Key] = new HashSet<Id>();
                            invFactDeps[fact.Key] = new HashSet<Id>();
                        }
                        foreach (var fact in facts)
                        {
                            var deps = factDeps[fact.Key];

                            var ids = fact.Value.GetNodesOfType<Id>();
                            foreach (var id in ids)
                                if (facts.ContainsKey(id))
                                {
                                    deps.Add(id);
                                    invFactDeps[id].Add(fact.Key);
                                }
                        }                        
                        HashSet<Id> visited_facts = new HashSet<Id>();
                        List<Id> ordered_facts = new List<Id>();
                        foreach (var id in facts.Keys)
                            topological_ordering_ids(id, factDeps, ref visited_facts, ref ordered_facts);

                        Dictionary<Id, UserFuncTerm> tmp = new Dictionary<Id, UserFuncTerm>(facts);
                        foreach (var id in ordered_facts)
                        {
                            var cur = facts[id];
                            foreach (var dependant in invFactDeps[id])
                            {
                                var funcdep = facts[dependant];
                                facts[dependant] = (UserFuncTerm)funcdep.Substitute(id, cur);
                            }
                        }

                        foreach (var fact in facts.Values)
                        {                        
                            // Replace ID-s with UserFuncTerms
                            var rule = new Nodes.RuleNode() { Span = fact.Span };
                            rule.AddHead(fact);
                            model.AddRule(rule);
                        }
                    }

            // If successfully loaded, calculate dependencies
            Dictionary<string, string> module_to_program = new Dictionary<string,string>();
            Dictionary<string, Module> modules = new Dictionary<string, Module>();
            Dictionary<string, HashSet<string>> dependencies = new Dictionary<string, HashSet<string>>();
            List<Flag> flags = new List<Flag>();
            foreach (var program in programs.Values)
            {
                foreach (var module in program.modules)
                {
                    if (modules.ContainsKey(module.Name))
                    {
                        throw new UserException(
                                    String.Format("Multiple definitions: module {0} defined in {1} and {2}", 
                                        module.Name, 
                                        module_to_program[module.Name],
                                        program.Name), program);
                    }
                    modules.Add(module.Name, module);
                    module_to_program.Add(module.Name, program.Name.ToString());
                    var dep = new HashSet<string>();
                    if (module is Model)
                    {
                        Model mod = (Model)module;
                        dep.Add(mod.Domain.Name);
                    }
                    else if (module is Domain)
                    {
                        Domain dom = (Domain)module;
                        foreach (var mod in dom.Compositions)
                            dep.Add(mod.Name);
                        foreach (var mod in dom.Imports)
                            dep.Add(mod.Name);
                    }
                    else if (module is Transform)
                    {
                        Transform tf = (Transform)module;
                        foreach (var mod in tf.Inputs)
                            if (mod.Type is ModRef)
                                dep.Add(((ModRef)mod.Type).Name);
                        foreach (var mod in tf.Outputs)
                            if (mod.Type is ModRef)
                                dep.Add(((ModRef)mod.Type).Name);
                    }
                    dependencies.Add(module.Name, dep);
                }
            }
            
            // Based on dependencies, find a topological ordering
            Dictionary<string, bool> visited = new Dictionary<string, bool>();
            foreach (var s in modules.Keys)
                visited.Add(s, false);
            List<Module> ordered_modules = new List<Module>();
            foreach (var s in modules.Keys)
                if (!visited[s])
                    topological_ordering(s, modules, dependencies, ref visited, ref ordered_modules);                                   

            // Builtin functions
            foreach (var module in ordered_modules)
                ReplaceBuiltinFunctions(module);                       

            // Extract parameter accessors
            //foreach (var module in ordered_modules)
                //foreach (var rule in module.Rules)
                    //ExtractAccessors(rule);

            // Add Data union type for the domain, and identity constructor
            /*foreach (var module in ordered_modules)
            {
                if (!(module is Domain) || !((Domain)module).IsModel)
                {
                    AddDataType(module);
                    var id = ConDecl.Create(Span.Unknown, "_", false, null);
                    id.Fields.Add(new Field("", new Id("Data"), false));
                    module.AddTypeDecl(id);
                }
            }*/

            // Substitute every constant Id with EnumCnst
            Dictionary<string, HashSet<string>> constants = new Dictionary<string, HashSet<string>>();
            foreach (var module in ordered_modules)
            {
                HashSet<string> consts = new HashSet<string>();
                // Add constants from every ancestor
                foreach (var dep in dependencies[module.Name])
                    consts.UnionWith(constants[dep]);
                // Add builtin constants
                consts.Add("TRUE");
                consts.Add("FALSE");
                // Add constants in module
                foreach (var typedecl in module.TypeDecls)
                {
                    if (typedecl is EnumDecl)
                    {
                        var e = (EnumDecl)typedecl;
                        foreach (var c in e.Elements)
                            consts.Add(c.Name);
                    }
                }
                // Finally, substitute every constant Id with Enum
                SubstituteEnums(module, consts);
                constants.Add(module.Name, consts);
            }

            // Subtitute Id-s to Var-s
            foreach (var module in ordered_modules)
            {
                SubstituteVars(module);
            }

            // Remove accidental cycles of X ::= X + Y.
            foreach (var module in ordered_modules)
                foreach (var decl in module.TypeDecls)
                {
                    if (decl is UnnDecl)
                    {
                        var unn = (UnnDecl)decl;
                        unn.components.RemoveAll(x => x.Name == unn.Name);
                    }
                }

            // Perform post-processing
            // Needs to be done before substituting comprehensions.
            if (!CompilerBe.ResolveImports(ordered_modules) ||
                //!CompilerBe.ResolveUnionExtensions(ordered_modules) ||
                !CompilerBe.AddSyntacticSugar(ordered_modules))
            {
                throw new UserException("Adding syntactic sugars failed", null);
            }

            // Substitute every comprehension with fact+rule
            /*foreach (var module in ordered_modules)
            {
                List<Nodes.RuleNode> newRules = new List<Nodes.RuleNode>();
                List<TypeDecl> newTypes = new List<TypeDecl>();
                foreach (var rule in module.Rules)
                    foreach (var body in rule.Bodies)
                        ReplaceComprehensions(body, body, newTypes, newRules);
                module.Rules.AddRange(newRules);
                module.TypeDecls.AddRange(newTypes);
            } */                       
      
            foreach (var p in programs.Values)
                result.Programs.Add(p);
            //foreach (var p in ordered_modules)
                //result.OrderedModules.Add(p);

            foreach (var p in ordered_modules)
            {
                //var merge = MergeModules.MergeAncestors(modules, p);
                result.OrderedModules.Add(p);
            }
            return result;
        }
        private static void topological_ordering(string node, Dictionary<string, Module> modules, Dictionary<string, HashSet<string>> topology, ref Dictionary<string, bool> visited, ref List<Module> result)
        {
            visited[node] = true;
            var neighbors = topology[node];
            foreach (var n in neighbors)
            {
                if (!visited.ContainsKey(n))
                    throw new UserException(String.Format("Module {0} not found", n),null);
                if (!visited[n])
                    topological_ordering(n, modules, topology, ref visited, ref result);
            }
            result.Add(modules[node]);
            //Console.WriteLine(node);
        }
        private static void topological_ordering_ids(Id node, Dictionary<Id, HashSet<Id>> deps, ref HashSet<Id> visited, ref List<Id> result)
        {
            visited.Add(node);
            var neighbors = deps[node];
            foreach (var n in neighbors)
            {
                if (!visited.Contains(n))
                    topological_ordering_ids(n, deps, ref visited, ref result);
            }
            result.Add(node);
            //Console.WriteLine(node);
        }
        private static Node SubstituteEnums(Node node, HashSet<string> constants)
        {
            if (node == null) return null;
            if (node is Id && constants.Contains(node.Name))
                return new EnumCnst(node.Name) { Span = node.Span };
            else if (node is RelConstr)
            {
                var rel = (RelConstr)node;
                rel.Arg1 = SubstituteEnums(rel.Arg1, constants);
                rel.Arg2 = SubstituteEnums(rel.Arg2, constants);
            }
            else if (node is Compr)
            {
                var comp = (Compr)node;
                for (int i = 0; i < comp.Heads.Count; ++i)
                    comp.Heads[i] = SubstituteEnums(comp.Heads[i], constants);
                for (int i = 0; i < comp.Bodies.Count; ++i)
                    comp.Bodies[i] = (Body)SubstituteEnums(comp.Bodies[i], constants);
            }
            else if (node is UserFuncTerm)
            {
                var func = (UserFuncTerm)node;
                for (int i = 0; i < func.args.Count; ++i)
                    func.args[i] = SubstituteEnums(func.args[i], constants);
            }
            else if (node is BuiltinFuncTerm)
            {
                var func = (BuiltinFuncTerm)node;
                for (int i = 0; i < func.args.Count; ++i)
                    func.args[i] = SubstituteEnums(func.args[i], constants);
            }
            else
            {
                foreach (var child in node.Children)
                    SubstituteEnums(child, constants);
            }
            return node;
        }
        private static Node SubstituteVars(Node node)
        {
            if (node == null) return null;
            if (node is Id)
                return new Var(node.Name) { Span = node.Span };
            else if (node is RelConstr)
            {
                var rel = (RelConstr)node;
                rel.Arg1 = SubstituteVars(rel.Arg1);
                if (rel.Op != RelKind.Typ)
                    rel.Arg2 = SubstituteVars(rel.Arg2);
            }
            else if (node is Compr)
            {
                var comp = (Compr)node;
                for (int i = 0; i < comp.Heads.Count; ++i)
                    comp.Heads[i] = SubstituteVars(comp.Heads[i]);
                for (int i = 0; i < comp.Bodies.Count; ++i)
                    comp.Bodies[i] = (Body)SubstituteVars(comp.Bodies[i]);
            }
            else if (node is UserFuncTerm)
            {
                var func = (UserFuncTerm)node;
                for (int i = 0; i < func.args.Count; ++i)
                    func.args[i] = SubstituteVars(func.args[i]);
            }
            else if (node is BuiltinFuncTerm)
            {
                var func = (BuiltinFuncTerm)node;
                if (func.Function == OpKind.ToList)
                    func.args[2] = SubstituteVars(func.args[2]);
                else if (func.Function == OpKind.RflIsMember)
                    func.args[0] = SubstituteVars(func.args[0]);
                else
                    for (int i = 0; i < func.args.Count; ++i)
                        func.args[i] = SubstituteVars(func.args[i]);
            }
            else
            {
                foreach (var child in node.Children)
                    SubstituteVars(child);
            }
            return node;
        }
        private static Node ReplaceBuiltinFunctions(Node node)
        {
            if (node == null) return null;
            // Replace things under relational constraints (these are the places where we can use builtin functions, such as ToList)
            if (node is RelConstr)
            {
                var con = (RelConstr)node;
                con.Arg2 = ReplaceBuiltinFunctions(con.Arg2);
            }
            else 
            {
                if (node is UserFuncTerm)
                {
                    var func = (UserFuncTerm)node;
                    if (func.Function.Name.Equals("toList"))
                    {
                        var res = new BuiltinFuncTerm(OpKind.ToList) { Span = func.Span };
                        foreach (var arg in func.args)
                            res.AddArg(arg);
                        return res;
                    }
                    else if (func.Function.Name.Equals("toString"))
                    {
                        var res = new BuiltinFuncTerm(OpKind.ToString) { Span = func.Span };
                        foreach (var arg in func.args)
                            res.AddArg(ReplaceBuiltinFunctions(arg));
                        return res;
                    }
                    else if (func.Function.Name.Equals("strJoin"))
                    {
                        var res = new BuiltinFuncTerm(OpKind.StrJoin) { Span = func.Span };
                        foreach (var arg in func.args)
                            res.AddArg(ReplaceBuiltinFunctions(arg));
                        return res;
                    }
                }
                foreach (var child in node.Children)
                    ReplaceBuiltinFunctions(child);
            }
            return node;
        }
        private static void AddDataType(Module module)
        {
            // Add Data union type for the domain
            UnnDecl data = new UnnDecl("Data", false, null);
            foreach (var decl in module.TypeDecls)
                data.AddComponent(new Id(decl.Name));
            data.AddComponent(new Id("Integer"));
            data.AddComponent(new Id("Real"));
            data.AddComponent(new Id("String"));
            data.AddComponent(new Id("Boolean"));
            if (module is Domain)
            {
                var domain = (Domain)module;
                foreach (var comp in domain.Compositions)
                {
                    var compdata = String.Format("{0}.Data", String.IsNullOrEmpty(comp.Rename) ? comp.Name : comp.Rename);
                    data.AddComponent(new Id(compdata));
                }
            }
            if (data.components.Count > 0)
                module.AddTypeDecl(data);
        }
        static int compr_counter = 1;
        private static Node ReplaceComprehensions(Node node, Body rule_body, List<TypeDecl> newTypes, List<Nodes.RuleNode> newRules)
        {
            if (node == null) return null;
            if (node is BuiltinFuncTerm)
            {
                var func = (BuiltinFuncTerm)node;
                for (int i = 0; i < func.args.Count; ++i)
                    func.args[i] = ReplaceComprehensions(func.args[i], rule_body, newTypes, newRules);
            }
            else if (node is RelConstr)
            {
                var rel = (RelConstr)node;
                //if (rel.Op == RelKind.No)
                rel.Arg1 = ReplaceComprehensions(rel.Arg1, rule_body, newTypes, newRules);
                rel.Arg2 = ReplaceComprehensions(rel.Arg2, rule_body, newTypes, newRules);
            }
            else if (node is Compr)
            {
                var comp = (Compr)node;

                // Create type term
                var name = String.Format("#compr_{0}#{1}", compr_counter++, comp.Heads.Count);
                var decl = ConDecl.Create(Span.Unknown, name, false, null);
                // One field for each head
                foreach (var head in comp.Heads)
                    decl.Fields.Add(new Field("", new Id("Data"), false));
                // And one field for every variable referred in the body
                var vars = new HashSet<Var>();
                foreach (var head in comp.Bodies)
                    vars.UnionWith(head.GetVariables());
                foreach (var v in vars)
                    decl.Fields.Add(new Field("", new Id("Data"), false));
                newTypes.Add(decl);

                // Create rule for type term
                var newrule = new Nodes.RuleNode();
                // The head of the rule contains the head of the comprehension, plus the referred variables in its body
                var headterm = new UserFuncTerm(new Id(name));
                foreach (var head in comp.Heads)
                    headterm.AddArg(head);
                foreach (var v in vars)
                    headterm.AddArg(v);
                newrule.AddHead(headterm);
                // The body is the body of the comprehension, plus the body of the original rule
                var body = new Body();
                newrule.AddBody(body);
                foreach (var b in comp.Bodies)
                    foreach (var c in b.constraints)
                        body.AddConstr(c);
                // Add the body of the original rule, except for terms that contain the comprehension itself
                foreach (var c in rule_body.constraints)
                    if (!c.Contains(node))
                        body.AddConstr(c);
                newRules.Add(newrule);
                
                // Substitute the comprehension with a corresponding UserFuncTerm (already defined as headterm)
                return headterm;
            }
            else
                foreach (var child in node.Children)
                    ReplaceComprehensions(child, rule_body, newTypes, newRules);
            return node;
        }
        /*private static void ExtractAccessors(Rule rule)
        {
            foreach (var head in rule.Heads)
            {
                ExtractAccessors(head, rule);
            }
        }
        private static void ExtractAccessors(Node node, Node parent, Rule rule)
        {
            foreach (var child in node.Children)
                ExtractAccessors(child, node, rule);
            if (node is Id)
            {
                var id = (Id)node;
                var split = id.Name.Split(new char[]{'.'});
                if (split.Length > 0 && parent is UserFuncTerm)
                {
                    var func = (UserFuncTerm)parent;
                    var accessor = split[1];
                    foreach (var body in rule.Bodies)
                    {
                        var con = new Find(id, 
                        body.AddConstr();
                    }
                }
            }
        }*/

        private static bool Parse(string filename, out ParseResult result)
        {
            ProgramName progName;
            try
            {
                progName = new ProgramName(filename);
            }
            catch (Exception e)
            {
                var flag = new Flag(
                    SeverityKind.Error,
                    default(Span),
                    Constants.BadFile.ToString(string.Format("Could not open {0}; {1}", filename, e.Message)),
                    Constants.BadFile.Code);
                result = new ParseResult();
                result.AddFlag(flag);
                return false;
            }

            Parser p = new Parser();
            p.ParseFile(progName, null, Span.Unknown, new System.Threading.CancellationToken(), out result);  //Factory.Instance.ParseFile(progName, env.c.canceler.Token);            
            
            return result.Succeeded;
        }

        public static Node ParseFact(string text)
        {
            var progName = new ProgramName("");
            Parser p = new Parser();
            ParseResult result;
            var prog = String.Format("domain dummy {{ fact({0}). }}", text);
            p.ParseText(progName, prog, Span.Unknown, new System.Threading.CancellationToken(), out result);  //Factory.Instance.ParseFile(progName, env.c.canceler.Token);            

            if (result.Succeeded)
            {
                UserFuncTerm fact = (UserFuncTerm)result.Program.modules[0].Rules[0].Heads[0];
                return fact.args[0];
            }
            return null;
        }
    }

    internal partial class Parser : ShiftReduceParser<Nodes.Node, LexLocation>
    {
        Model     currentModel;
        Domain    currentDomain;
        Transform currentTransform;
        StringBuilder currentString;
        ParseResult parseResult;
        static int inlinedUnnOrEnumCounter;

        public Parser()
            : base(new Scanner())
        {
            ResetState();
        }

        void ResetState()
        {
            currentDomain = null;
            currentTransform = null;
            currentModel = null;
            currentString = null;
            parseResult = null;
            //inlinedUnnOrEnumCounter = 1;
        }

        public bool ParseFile(ProgramName name, string referrer, Span location, System.Threading.CancellationToken ctok, out ParseResult pr)
        {
            ResetState();
            parseResult = new ParseResult();
            parseResult.Program = new Program(name);
            pr = parseResult;
            
            var fi = new System.IO.FileInfo(name.Uri.AbsolutePath);
            if (!fi.Exists)
            {
                var badFile = new Flag(
                    SeverityKind.Error,
                    default(Span),
                    referrer == null ?
                        Constants.BadFile.ToString(string.Format("The file {0} does not exist", name.ToString())) :
                        Constants.BadFile.ToString(string.Format("The file {0} referred to in {1} ({2}, {3}) does not exist", name.ToString(), referrer, location.StartLine, location.StartCol)),
                    Constants.BadFile.Code,
                    name);
                parseResult.AddFlag(badFile);
                return false;
            }
        
            bool result;
            using (System.IO.Stream str = new System.IO.FileStream(name.Uri.AbsolutePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                result = ParseStream(str, name, referrer, location, ctok);

            return result;
        }

        public bool ParseText(ProgramName name, string programText, Span location, System.Threading.CancellationToken ctok, out ParseResult pr)
        {
            ResetState();
            parseResult = new ParseResult();
            parseResult.Program = new Program(name);
            pr = parseResult;
            bool result;
            using (System.IO.Stream str = new System.IO.MemoryStream(System.Text.Encoding.ASCII.GetBytes(programText)))
                result = ParseStream(str, name, null, location, ctok);
            return result;
        }

        bool ParseStream(System.IO.Stream stream, ProgramName name, string referrer, Span location, System.Threading.CancellationToken ctok)
        {
            ((Scanner)Scanner).SetSource(stream);
            ((Scanner)Scanner).ParseResult = parseResult;
            
            bool result = Parse(ctok);

            if (ctok.IsCancellationRequested)
            {
                var badFile = new Flag(
                    SeverityKind.Error,
                    default(Span),
                    referrer == null ?
                       Constants.OpCancelled.ToString(string.Format("Cancelled parsing of {0}", name.ToString())) :
                       Constants.OpCancelled.ToString(string.Format("Cancelled parsing of {0} referred to in {1} ({2}, {3})", name.ToString(), referrer, location.StartLine, location.StartCol)),
                    Constants.OpCancelled.Code,
                    name);
                parseResult.AddFlag(badFile);
                return false;
            }
            return result;
        }

        private Span ToSpan(LexLocation loc)
        {
            return new Span(loc.StartLine, loc.StartColumn + 1, loc.EndLine, loc.EndColumn + 1);
        }

        private Cnst ParseNumeric(string str, bool isFraction, Span span = default(Span))
        {
            Contract.Requires(!string.IsNullOrEmpty(str));
            Rational numVal;
            bool result;
            if (isFraction)
            {
                result = Rational.TryParseFraction(str, out numVal);
            }
            else
            {
                result = Rational.TryParseDecimal(str, out numVal);
            }

            Contract.Assert(result);
            return new RationalCnst(numVal) { Span = span };
        }

        internal void StartModel(string name, bool isPartial, Node domainref, Span span)
        {
            currentModel = new Model(name, isPartial) { Span = span };
            currentModel.Domain = (ModRef)domainref;
            currentModel.ParsedFrom = parseResult.Program.Name.ToString();
            currentDomain = null;
            currentTransform = null;
            parseResult.Program.modules.Add(currentModel);
        }
        internal void StartDomain(Id name, Span span)
        {
            currentModel = null;
            currentDomain = new Domain(name.Name) { Span = span };
            currentDomain.ParsedFrom = parseResult.Program.Name.ToString();
            currentTransform = null;
            parseResult.Program.modules.Add(currentDomain);
        }
        internal void StartTransform(string name, Span span)
        {
            currentModel = null;
            currentDomain = null;
            currentTransform = new Transform(name) { Span = span };
            currentTransform.ParsedFrom = parseResult.Program.Name.ToString();
            parseResult.Program.modules.Add(currentTransform);
        }

        internal void AppendFact(Node binding, Node fact)
        {
            currentModel.facts.Add(new ModelFact((Id)binding, (UserFuncTerm)fact));
            if (binding != null)
                ((UserFuncTerm)fact).Id = binding.Name;
        }

        internal void AppendPropContract(ContractKind kind, Node bodies, Span span)
        {
            var contract = new ContractItem(kind);
            foreach (var b in (NodeList<Body>)bodies)
                contract.specification.Add(b);
            if (currentModel != null)
                currentModel.contracts.Add(contract);
            else if (currentDomain != null)
                currentDomain.Conforms.Add(contract);
            else if (currentTransform != null)
                currentTransform.Contracts.Add(contract);
        }

        /*internal void AppendCardContract(ContractKind kind, RationalCnst card, Node body, Span span)
        {
            var contract = new ContractItem(kind);
            contract.specification.Add((Body)body);
            if (currentModel != null)
                currentModel.contracts.Add(contract);
            else if (currentDomain != null)
                currentDomain.Conforms.Add(contract);
            else if (currentTransform != null)
                currentTransform.Contracts.Add(contract);
        } */ 

        internal void AppendParameters(Node paramList, bool isInput)
        {
            var list = (NodeList<Param>)paramList;
            if (isInput)
                foreach (var p in list)
                    currentTransform.AddInput(p);
            else
                foreach (var p in list)
                    currentTransform.AddOutput(p);
        }

        internal void AppendRule(Node rule)
        {
            var r = (RuleNode)rule;
            if (currentDomain != null)
                currentDomain.Rules.Add(r);
            else if (currentTransform != null)
                currentTransform.Rules.Add(r);
        }

        internal void AppendTypeDecl(Node decl)
        {
            var tdecl = (TypeDecl)decl;
            if (currentDomain != null)
                currentDomain.AddTypeDecl(tdecl);
            else if (currentTransform != null)
                currentTransform.AddTypeDecl(tdecl);
        }        

        internal void AppendCompositions(Node compositions, ComposeKind kind)
        {
            var list = (NodeList<ModRef>)compositions;
            currentDomain.ComposeKind = kind;
            foreach (var p in list)
                currentDomain.AddComposition(p);
        }

        internal ModRef MkModRef(Span span, string name, string rename, string location)
        {
            var modref = new Nodes.ModRef(name, rename, location) { Span = span };
            if (location!=null && location.Length >= 2)
                parseResult.ToLoad.Add(location.Substring(1,location.Length-2));
            return modref;
        }
        
        internal RelConstr MkNoConstr(Span span, Node match)
        {
            var body = new Body();
            body.AddConstr(new Find(null, match) { Span = span });
            var compr = new Compr();
            compr.AddHead(new BoolCnst(true));
            compr.AddBody(body);
            return new RelConstr(RelKind.No, compr) { Span = span };
        }
        internal RelConstr MkNoConstr(Span span, Node id, Node match)
        {
            var body = new Body();
            body.AddConstr(new Find((Var)id, match) { Span = span });
            var compr = new Compr();
            compr.AddHead(id);
            compr.AddBody(body);
            return new RelConstr(RelKind.No, compr) { Span = span };
        }

        internal BuiltinFuncTerm MkExpr(Span span, OpKind op, Node arg1, Node arg2)
        {
            var res = new BuiltinFuncTerm(op) { Span = span };
            res.AddArg(arg1);
            if (arg2 != null)
                res.AddArg(arg2);
            return res;
        }

        internal BuiltinFuncTerm MkRflIsMember(Span span, Node node, Node type)
        {
            var res = new BuiltinFuncTerm(OpKind.RflIsMember) { Span = span };
            res.AddArg(node);            
            res.AddArg(new Id(type.Name.Replace("#","")));
            return res;
        }

        internal BuiltinFuncTerm MkToList(Span span, Node constructor, Node nil, Node compr)
        {
            var res = new BuiltinFuncTerm(OpKind.ToList) { Span = span };
            res.AddArg(constructor);
            res.AddArg(nil);
            res.AddArg(compr);
            return res;
        }

        internal BuiltinFuncTerm MkToString(Span span, Node expr)
        {
            var res = new BuiltinFuncTerm(OpKind.ToString) { Span = span };
            res.AddArg(expr);
            return res;
        }

        internal BuiltinFuncTerm MkStrJoin(Span span, Node expr1, Node expr2)
        {
            var res = new BuiltinFuncTerm(OpKind.StrJoin) { Span = span };
            res.AddArg(expr1);
            res.AddArg(expr2);
            return res;
        }

        internal UserFuncTerm MkUserFuncTerm(Span span, Node function, Node arglist1, Node arglist2, Node arglist3 = null)
        {
            var res = new UserFuncTerm((Id)function, (NodeList<Node>)arglist1) { Span = span };
            if (arglist2 is NodeList<Node>)
                foreach (var arg in (NodeList<Node>)arglist2)
                    res.args.Add(arg);
            else if (arglist2 != null)
                res.args.Add(arglist2);
            if (arglist3 is NodeList<Node>)
                foreach (var arg in (NodeList<Node>)arglist3)
                    res.args.Add(arg);
            else if (arglist3 != null)
                res.args.Add(arglist3);
            return res;        
        }

        internal NodeList<UserFuncTerm> MkDenotationalHead(Span span, Node function, Node id, Node argsList, Node result)
        {
            NodeList<UserFuncTerm> res = null;
            if (result is NodeList<Node>)
            {
                foreach (var resultElem in (NodeList<Node>)result)
                {
                    var head = new UserFuncTerm((Id)function, null) { Span = span };
                    head.AddArg(id);
                    if (argsList is NodeList<Node>)
                        foreach (var arg in (NodeList<Node>)argsList)
                            head.AddArg(arg);
                    if (resultElem is NodeList<Node>)
                    {
                        foreach (var r in (NodeList<Node>)resultElem)
                            head.AddArg(r);
                    }
                    else
                        head.AddArg(resultElem);
                    res = new NodeList<UserFuncTerm>(head, res);
                }
            }
            else
            {
                var head = new UserFuncTerm((Id)function, null) { Span = span };
                head.AddArg(id);
                if (argsList is NodeList<Node>)
                    foreach (var arg in (NodeList<Node>)argsList)
                        head.AddArg(arg);
                if (result != null)
                    head.AddArg(result);
                res = new NodeList<UserFuncTerm>(head, res);
            }
            return res;
        }
        internal UserFuncTerm MkDenotationalTerm(Span span, Node function, Node id, Node argsList, Node result)
        {
            var head = new UserFuncTerm((Id)function, null) { Span = span };
            head.AddArg(id);
            if (argsList is NodeList<Node>)
                foreach (var arg in (NodeList<Node>)argsList)
                    head.AddArg(arg);
            if (result is NodeList<Node>)
                foreach (var r in (NodeList<Node>)result)
                    head.AddArg(r);
            else
                head.AddArg(result);
            return head;
        }

        internal Node MkEquality(Span span, Node arg1, Node arg2)
        {
            return new RelConstr(RelKind.Eq, arg1, arg2) { Span = span };
        }

        internal TypeDecl MkUnnOrEnumDecl(Span span, string name, bool isExtension, IEnumerable<Node> components)
        {
            List<EnumCnst> enumcnsts = new List<EnumCnst>();
            List<Id> unn = new List<Id>();
            foreach (var comp in components)
            {
                if (comp is EnumCnst)
                    enumcnsts.Add((EnumCnst)comp);
                else if (comp is NodeList<EnumCnst>)
                {
                    foreach (var cnst in (NodeList<EnumCnst>)comp)
                        enumcnsts.Add(cnst);
                }
                else if (comp is Id)
                    unn.Add((Id)comp);
            }
            if (unn.Count == 0)
                return new EnumDecl(name, enumcnsts) { Span = span };
            if (enumcnsts.Count == 0)
                return new UnnDecl(name, isExtension, unn) { Span = span };

            // else mix of Unn and EnumCnsts
            Id enumId = MkInlinedUnnOrEnum(span, enumcnsts);
            var res = new UnnDecl(name, isExtension, unn) { Span = span };
            res.AddComponent(enumId);
            return res;
        }

        internal Id MkInlinedUnnOrEnum(Span span, IEnumerable<Node> components)
        {
            var name = "#inlined_" + inlinedUnnOrEnumCounter++;
            var decl = MkUnnOrEnumDecl(span, name, false, components);
            AppendTypeDecl(decl);
            return new Id(name) { Span = span };
        }

        internal RelConstr MkInlineTypeCheck(Span span, Node lhs, Node rhs)
        {
            var list = rhs as NodeList<Node>;
            if (list != null)
            {
                if (list.rest == null)
                    return new RelConstr(RelKind.Typ, lhs, list.head) { Span = span };
                var id = MkInlinedUnnOrEnum(span, list);
                return new RelConstr(RelKind.Typ, lhs, id) { Span = span };
            }
            return new RelConstr(RelKind.Typ, lhs, rhs) { Span = span };
        }

        internal Field MkField(Span span, string name, NodeList<Node> type, bool isAny)
        {
            if (type.Count() == 1 && (type.First() is Id))
            {
                return new Field(name, (Id)type.First(), isAny) { Span = span };
            }
            else
            {
                var typeid = MkInlinedUnnOrEnum(span, type);
                return new Field(name, typeid, isAny) { Span = span };
            }
        }

        internal void StartString(Span span)
        {
            currentString = new StringBuilder();
        }

        internal void AppendString(string app)
        {
            currentString.Append(app);
        }

        internal void AppendSingleEscape(string escape)
        {
            currentString.Append(escape);
        }

        internal void EndString(Span span)
        {
        }

        internal StringCnst GetString(Span span)
        {
            return new StringCnst(String.Format("\"{0}\"", currentString.ToString())) { Span = span };
        }
    }
}
