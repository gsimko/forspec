﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe
{
    class UserException : Exception
    {
        public string msg;
        public Nodes.Node node;
        public UserException(string msg, Nodes.Node node)
        {
            this.msg = msg;
            this.node = node;
        }
        public UserException(Nodes.Node node, string format, params object[] args)
        {
            this.msg = String.Format(format, args);
            this.node = node;
        }
    }
    class FlagException : Exception
    {
        public List<Flag> flags;
        public FlagException(List<Flag> flags)
        {
            this.flags = new List<Flag>(flags);
        }
    }
}
