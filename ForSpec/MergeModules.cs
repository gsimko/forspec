﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaBe
{
	using Nodes;
    class MergeModules
    {        
        /// <summary>
        /// Convert the output of an execution to a model (add model facts and ids)
        /// </summary>
        public static Model ToModel(Model model)
        {
            var outmodel = new Model(model.Name, false);
            outmodel.Domain = model.Domain;
            int counter = 0;
            model.Rules.Sort((x, y) => x.ToString().CompareTo(y.ToString()));
            foreach (var rule in model.Rules)
            {
                if (rule.Bodies.Count != 0) continue;
                foreach (var head in rule.Heads)
                {
                    var newhead = (UserFuncTerm)head.Clone();
                    var newrule = new RuleNode();
                    newrule.AddHead(newhead);
                    outmodel.AddRule(newrule);
                    var name = String.Format("{0}__{1}__0x{2:x}", outmodel.Name, newhead.Name.Replace('.','_'), counter++);
                    outmodel.facts.Add(new ModelFact(new Id(name), newhead));
                }
            }
            return outmodel;
        }
        /// <summary>
        /// Extract the outputs of the execution of a transformation (add model facts and ids)
        /// </summary>
        public static LinkedList<Model> ToModels(Dictionary<string,Module> modules, Model model, Transform tf, string[] output)
        {
            LinkedList<Model> result = new LinkedList<Model>();
            for (int i = 0; i < tf.Outputs.Count; ++i)
            {
                var outname = tf.Outputs[i].Name;
                var outdomain = new Model(output[i], false);
                if (tf.Outputs[i].Type is ModRef)
                {
                    var modref = (ModRef)tf.Outputs[i].Type;                    
                    outdomain.SetDomain(new ModRef(modref.Name, null, modules[modref.Name].ParsedFrom));
                }
                result.AddLast(outdomain);
                int counter = 0;
                model.Rules.Sort((x,y) => x.ToString().CompareTo(y.ToString()));
                foreach (var fact in model.Rules)
                {
                    if (fact.Bodies.Count > 0)
                        continue;
                    var head = fact.Heads[0];
                    if (head.Namespace != null && (head.Namespace == outname || head.Namespace.StartsWith(outname+'.')))
                    {
                        var node = (RuleNode)fact.Clone();
                        node.RemoveNamespace(outname);
                        outdomain.AddRule(node);
                        var name = String.Format("{0}__{1}__0x{2:x}", outdomain.Name, node.Heads[0].Name.Replace('.', '_'), counter++);
                        outdomain.facts.Add(new ModelFact(new Id(name), node.Heads[0]));
                    }
                }
            }
            return result;
        }
        /// Merge all the referred modules into one module, and resolve qualified naming.
        public static Domain MergeAncestors(Dictionary<string, Module> modules, Module module, bool replaceComprehension)
        {
            Dictionary<string, string> fullyQualifiedNames = new Dictionary<string, string>();
            var addedNames = new HashSet<string>();
            Domain mod = null;
            if (module is Domain)
            {
                var dom = (Domain)module;
                mod = new Domain(dom.Name, ComposeKind.None, null) { Span = dom.Span };
                AddToDomain(mod, dom, "", "", modules, fullyQualifiedNames, addedNames, false, replaceComprehension);
            }
            else if (module is Transform)
            {
                var tf = (Transform)module;
                mod = new Domain(tf.Name, ComposeKind.None, null) { Span = tf.Span };
                AddToDomain(mod, tf, modules, fullyQualifiedNames, addedNames, replaceComprehension);
            }
            return mod;
        }
        /// <summary>
        /// Merge ancestors and inputs of a transformation.
        /// </summary>
        public static Domain MergeAncestorsAndInputs(Dictionary<string, Module> modules, Transform tf, IList<string> inputs)
        {
            Dictionary<string, string> fullyQualifiedNames = new Dictionary<string, string>();
            var addedNames = new HashSet<string>();
            var mod = new Domain(tf.Name, ComposeKind.Includes, null) { Span = tf.Span };
            AddToDomain(mod, tf, modules, fullyQualifiedNames, addedNames, true);
            
            for (int i = 0; i < tf.Inputs.Count; ++i)
            {
                var modref = tf.Inputs[i].Type as ModRef;
                if (modref != null)
                {
					Module inputModule;
                    if (modules.TryGetValue(inputs[i], out inputModule) && inputModule is Domain)
					{
                        foreach (var rule in inputModule.Rules)
                        {
                            var newrule = (RuleNode)rule.Clone();
                            newrule.AppendNamespace(modref.Rename + ".");
                            AddInheritedNamespace(newrule, "");
                            newrule.ReplaceNamesExceptVars(fullyQualifiedNames);
                            mod.AddRule(newrule);
                        }
                    }
                }
                else
                {
                    Node fact = FormulaBeParser.ParseFact(inputs[i]);
                    for (int j = 0; j < mod.Rules.Count; ++j)
                        mod.Rules[j] = (RuleNode)mod.Rules[j].Substitute(new Id("%" + tf.Inputs[i].Name), fact);
                }
            }
            return mod;
        }
        /// <summary>
        /// Resolve all the namespaces for a transformation.
        /// </summary>
        public static Transform ResolveNamespaces(Dictionary<string, Module> modules, Transform transform)
        {
            Dictionary<string, string> fullyQualifiedNames = new Dictionary<string, string>();
            var addedNames = new HashSet<string>();
            var mod = new Transform(transform.Name) { Span = transform.Span };
            foreach (var input in transform.Inputs)
                mod.AddInput(input);
            foreach (var output in transform.Outputs)
                mod.AddOutput(output);
            ResolveNamespaces(mod, transform, modules, fullyQualifiedNames, addedNames);
            return mod;
        }

        /// <summary>
        /// Resolve all the namespaces for a model.
        /// </summary>
        public static Model ResolveNamespaces(Dictionary<string, Module> modules, Model model)
        {
            Dictionary<string, string> fullyQualifiedNames = new Dictionary<string, string>();
            var addedNames = new HashSet<string>();
            var mod = new Model(model.Name, model.IsPartial) { Span = model.Span };
            mod.Domain = model.Domain;
            ResolveNamespaces(mod, model, modules, fullyQualifiedNames, addedNames);
            return mod;
        }

        /// <summary>
        /// Replace all qualified names with sanitized version.
        /// </summary>
        public static void SanitizeDots(Node node)
        {
            if (node == null) return;
            var func = node as UserFuncTerm;
            if (func != null)
                func.Function.Name = func.Function.Name.Replace('.', '_');
            var unndecl = node as UnnDecl;
            if (unndecl != null)
                unndecl.Name = unndecl.Name.Replace('.', '_');
            var enumdecl = node as EnumDecl;
            if (enumdecl != null)
                enumdecl.Name = enumdecl.Name.Replace('.', '_');
            var condecl = node as ConDecl;
            if (condecl != null)
                condecl.Name = condecl.Name.Replace('.', '_');
            var id = node as Id;
            if (id != null)
                id.Name = id.Name.Replace('.', '_');
            var fact = node as ModelFact;
            if (fact != null)
            {
                SanitizeDots(fact.Binding);
                SanitizeDots(fact.Match);
            }
            foreach (var child in node.Children)
                SanitizeDots(child);
        }

        private static string SanitizeFrom2ndDot(string str)
        {
            StringBuilder sb = new StringBuilder();
            bool first = true;
            foreach (var c in str)
            {
                if (c == '.')
                {
                    if (first)
                    {
                        first = false;
                        sb.Append('.');
                    }
                    else
                        sb.Append('_');
                }
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Replace all qualified names with sanitized version in transformation (only from 2nd dot).
        /// </summary>
        public static void SanitizeFrom2ndDot(Node node)
        {
            var func = node as UserFuncTerm;
            if (func != null)
                func.Function.Name = SanitizeFrom2ndDot(func.Function.Name);
            var unndecl = node as UnnDecl;
            if (unndecl != null)
                unndecl.Name = SanitizeFrom2ndDot(unndecl.Name);
            var enumdecl = node as EnumDecl;
            if (enumdecl != null)
                enumdecl.Name = SanitizeFrom2ndDot(enumdecl.Name);
            var condecl = node as ConDecl;
            if (condecl != null)
                condecl.Name = SanitizeFrom2ndDot(condecl.Name);
            var id = node as Id;
            if (id != null)
                id.Name = SanitizeFrom2ndDot(id.Name);
            foreach (var child in node.Children)
                SanitizeFrom2ndDot(child);
        }

        /// <summary>
        /// Add the name of the module before each "Data"
        /// </summary>
        public static void AddNamespaceBeforeData(Module module, Node node)
        {
            var id = node as Id;
            if (id != null && id.Name.Equals("Data"))
                id.Name = module.Name + ".Data";
            foreach (var child in node.Children)
                AddNamespaceBeforeData(module, child);
        }

        /// <summary>
        /// Add namespace to each UserFuncTerm.
        /// </summary>
        private static void AddInheritedNamespace(Node node, string ns)
        {
            if (node is UserFuncTerm)
            {
                var func = (UserFuncTerm)node;
                // If not qualified, apply the namespace of its parent.
                if (!String.IsNullOrEmpty(ns) && !func.Name.Contains('.'))
                    func.Name = ns + func.Name;
                else
                {
                    // If it is already qualified, use that namespace for its children.
                    string newns = func.Name.Substring(0,func.Name.LastIndexOf('.')+1);
                    foreach (var child in func.Children)
                        AddInheritedNamespace(child, newns);
                    return;
                }
            }
            foreach (var child in node.Children)
                AddInheritedNamespace(child, ns);
        }
        
        /// Extract qualified names from declarations (and mask existing entries if necessary).
        private static void ExtractQualifiedNames(Module from, string ns, string completens, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames, bool overwrite)
        {
            //var prefixns = String.IsNullOrEmpty(ns) ? "" : (ns + ".");
            fullyQualifiedNames["Data"] = completens + "Data";
            foreach (var decl in from.TypeDecls)
            {
                if (!overwrite && fullyQualifiedNames.ContainsKey(decl.Name))
                {
                    var cname = fullyQualifiedNames[decl.Name];
                    if (cname != ns + decl.Name)
                        fullyQualifiedNames[decl.Name] = "$ambiguous$";
                }
                else
                    fullyQualifiedNames[decl.Name] = ns + decl.Name;
                addedNames.Add(decl.Name);
            }
            foreach (var rule in from.Rules)
                foreach (var head in rule.Heads)
                    // Bool constants in head of rules
                    if (head.args.Count == 0)
                    {
                        if (!overwrite && fullyQualifiedNames.ContainsKey(head.Name))
                        {
                            var cname = fullyQualifiedNames[head.Name];
                            if (cname != ns + head.Name)
                                fullyQualifiedNames[head.Name] = "$ambiguous$";
                        }
                        else
                            fullyQualifiedNames[head.Name] = ns + head.Name;
                        addedNames.Add(head.Name);
                    }
        }
        /// <summary>
        /// Create builtin "Data" type
        /// </summary>
        private static UnnDecl CreateDataType(Module module, string ns)
        {
            var name = "Data";
            // Add Data union type for the domain
            UnnDecl data = new UnnDecl(name, false, null);
            foreach (var decl in module.TypeDecls)
                data.AddComponent(new Id(decl.Name));
            data.AddComponent(new Id("Integer"));
            data.AddComponent(new Id("Real"));
            data.AddComponent(new Id("String"));
            data.AddComponent(new Id("Boolean"));
            foreach (var comp in module.ModRefs)
                data.AddComponent(new Id(ns + (String.IsNullOrEmpty(comp.Rename) ? comp.Name : comp.Rename) + ".Data"));
            return data;
        }
        /// <summary>
        /// Add "Data" type, and identity function
        /// </summary>
        private static void AddDataAndIdentity(Module from, Domain to, string ns, string completens, Dictionary<string, string> fullyQualifiedNames)
        {
            // Add Data
            var data = CreateDataType(from, completens);
            data.ReplaceNamesExceptVars(fullyQualifiedNames);
            to.AddTypeDecl(data);
            // Add identity constructor
            var id = ConDecl.Create(Span.Unknown, completens + "_", false, null);
            id.Fields.Add(new Field("", new Id(data.Name), false));
            to.AddTypeDecl(id);
        }
        /// <summary>
        /// Copy declarations and (optionally) rules between modules and update them according to the fully qualified namings.
        /// </summary>
        private static void CopyDeclAndRules(Module from, Module to, string ns, string completens, Dictionary<string, string> fullyQualifiedNames, bool copyOnlyDecls, bool replaceComprehension)
        {            
            // Add type declarations
            foreach (var decl in from.TypeDecls)
            {
				var newdecl = (TypeDecl)decl.Clone();
                newdecl.ReplaceNamesExceptVars(fullyQualifiedNames);

                bool ext = false;
                if (newdecl is UnnDecl)
                {
                    var unndecl = (UnnDecl)newdecl;
                    if (unndecl.IsExtension)
                    {
                        foreach (var olddecl in to.TypeDecls)
                            if (olddecl.Name.Equals(unndecl.Name) && olddecl is UnnDecl)
                            {
                                var oldunndecl = (UnnDecl)olddecl;
                                foreach (var comp in unndecl.components)
                                    oldunndecl.AddComponent(comp);
                                ext = true;
                                break;
                            }
                    }
                }
                if (!ext)
                    to.AddTypeDecl(newdecl);            
            }

            if (!copyOnlyDecls)
            {
                // Extract any inline comprehensions into separate declarations, and add regular rules
                List<Nodes.RuleNode> newRules = new List<Nodes.RuleNode>();
                List<TypeDecl> newTypes = new List<TypeDecl>();
                foreach (var rule in from.Rules)
                {
                    var newrule = (RuleNode)rule.Clone();
                    AddInheritedNamespace(newrule, "");
                    newrule.ReplaceNamesExceptVars(fullyQualifiedNames);
                    to.AddRule(newrule);

                    // replace comprehensions in newrule, and collect additional rules and declarations.
                    if (replaceComprehension)
                        foreach (var body in newrule.Bodies)
                            ReplaceComprehensions(from, body, body, newTypes, newRules);                    
                }
                to.Rules.AddRange(newRules);
                to.TypeDecls.AddRange(newTypes);
            }
        }
        /// <summary>
        /// Copy facts between models and update them according to the fully qualified namings.
        /// </summary>
        private static void CopyFacts(Model from, Model to, string ns, string completens, Dictionary<string, string> fullyQualifiedNames)
        {
            // Add type declarations
            foreach (var fact in from.facts)
            {
                var newfact = (ModelFact)fact.Clone();
                newfact.ReplaceNamesExceptVars(fullyQualifiedNames);
                to.facts.Add(newfact);
            }
        }

        /// <summary>
        /// Update qualified and added names with current local namespace prefix.
        /// Also keep the previous version (without the new local namespace).
        /// </summary>
        private static void UpdateQualifiedNames(string localns, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames)
        {
            if (String.IsNullOrEmpty(localns))
                return;
            var toAdd = new Dictionary<string, string>();
            var toRemove = new HashSet<string>();
            foreach (var kvp in fullyQualifiedNames)
            {
                if (!kvp.Key.Contains('.'))
                {
                    toAdd[localns + kvp.Key] = kvp.Value;
                    //toRemove.Add(kvp.Key);
                }
            }
            //foreach (var key in toRemove)
                //fullyQualifiedNames.Remove(key);
            foreach (var kvp in toAdd)
            {
                fullyQualifiedNames[kvp.Key] = kvp.Value;
                //addedNames.Add(kvp.Key);
            }
        }

        /// Inject a domain into another one using a global namespace prefix. 
        /// Collect all the fully qualified names, as well as the added names from the current domain.
        private static void AddToDomain(Domain domain, Domain toAdd, string ns, string completens, Dictionary<string, Module> modules, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames, bool copyOnlyDecls, bool replaceComprehension)
        {
            // If this module has no references, copy its declarations and rules.
            if (toAdd.ModRefs.Count() == 0)
            {
                //Dictionary<string, string> localNames = new Dictionary<string, string>(fullyQualifiedNames);
                ExtractQualifiedNames(toAdd, ns, completens, fullyQualifiedNames, addedNames, overwrite: true);
                CopyDeclAndRules(toAdd, domain, ns, completens, fullyQualifiedNames, copyOnlyDecls, replaceComprehension);
                if (!toAdd.IsModel)
                    AddDataAndIdentity(toAdd, domain, ns, completens, fullyQualifiedNames);
                return;
            }
            // Otherwise, add its references first, and then the domain itself.
            else
            {
                // Collect the added identifiers for each reference.
				var localAddedNames = new List<HashSet<string>>();
                var localQualNames = new List<Dictionary<string, string>>();
                foreach (var comp in toAdd.ModRefs)
                {
                    Module mod;
                    // Only domain references are allowed.
                    if (!modules.TryGetValue(comp.Name, out mod) || !(mod is Domain))
                        throw new UserException("Invalid module reference", comp);
                    // newns is the new namespace for the added stuff
                    var localns = String.IsNullOrEmpty(comp.Rename) ? "" : (comp.Rename + ".");
                    var newns = ns + localns;
                    var newcompletens = completens + (String.IsNullOrEmpty(comp.Rename) ? comp.Name : comp.Rename) + ".";
                    if (toAdd.IsModel)
                    {
                        newns = ns;
                        newcompletens = completens;
                    }
                    
                    var localAddNames = new HashSet<string>();
                    var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                    AddToDomain(domain, (Domain)mod, newns, newcompletens, modules, fullyQualifiedNames, localAddNames, copyOnlyDecls, replaceComprehension);
                    if (!toAdd.IsModel)
                    {
                        localns = (String.IsNullOrEmpty(comp.Rename) ? comp.Name : comp.Rename) + ".";
                        UpdateQualifiedNames(localns, fullyQualifiedNames, localAddNames);
                    }
                    localAddedNames.Add(localAddNames);
                    localQualNames.Add(localQualName);
                }

                // And now merge all the results
                foreach (var localQualName in localQualNames)
                    foreach (var kvp in localQualName)
                        if (!fullyQualifiedNames.ContainsKey(kvp.Key))
                            fullyQualifiedNames[kvp.Key] = kvp.Value;
                
                //addedNames.ExceptWith(overlap);
                ExtractQualifiedNames(toAdd, ns, completens, fullyQualifiedNames, addedNames, overwrite: true);
                CopyDeclAndRules(toAdd, domain, ns, completens, fullyQualifiedNames, copyOnlyDecls, replaceComprehension);

                if (!toAdd.IsModel)
                    AddDataAndIdentity(toAdd, domain, ns, completens, fullyQualifiedNames);
            }
        }
        /// <summary>
        /// Extract a domain by merging a transformation with its input parameters
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="toAdd"></param>
        /// <param name="modules"></param>
        /// <param name="fullyQualifiedNames"></param>
        /// <param name="addedNames"></param>
        private static void AddToDomain(Domain domain, Transform toAdd, Dictionary<string, Module> modules, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames, bool replaceComprehension)
        {
            HashSet<string> qualifiedNames = new HashSet<string>();
            var localAddedNames = new List<HashSet<string>>();
            var localQualNames = new List<Dictionary<string, string>>();
			foreach (var child in toAdd.Inputs)
            {
                if (child is Param && ((Param)child).Type is ModRef)
                {
                    var modref = (ModRef)((Param)child).Type;
                    Module dom;
                    if (!modules.TryGetValue(modref.Name, out dom) || !(dom is Domain))
                        throw new UserException("Invalid module reference", modref);
                    var local = new HashSet<string>();
                    var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                    var ns = modref.Rename + ".";
                    AddToDomain(domain, (Domain)dom, ns, ns, modules, fullyQualifiedNames, local, copyOnlyDecls: false, replaceComprehension: replaceComprehension);
                    UpdateQualifiedNames(ns, fullyQualifiedNames, local);
                    localAddedNames.Add(local);
                    localQualNames.Add(localQualName);
                }
            }
            foreach (var child in toAdd.Outputs)
            {
                if (child is Param && ((Param)child).Type is ModRef)
                {
                    var modref = (ModRef)((Param)child).Type;
                    Module dom;
                    if (!modules.TryGetValue(modref.Name, out dom) || !(dom is Domain))
                        throw new UserException("Invalid module reference", modref);
                    var local = new HashSet<string>();
                    var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                    var ns = modref.Rename + ".";
                    AddToDomain(domain, (Domain)dom, ns, ns, modules, fullyQualifiedNames, local, copyOnlyDecls: true, replaceComprehension: replaceComprehension);
                    UpdateQualifiedNames(ns, fullyQualifiedNames, local);
                    localAddedNames.Add(local);
                    localQualNames.Add(localQualName);
                }
            }

            // And now merge all the results
            foreach (var localQualName in localQualNames)
                foreach (var kvp in localQualName)
                    if (!fullyQualifiedNames.ContainsKey(kvp.Key))
                        fullyQualifiedNames[kvp.Key] = kvp.Value;
            HashSet<string> toRemove = new HashSet<string>();
            foreach (var kvp in fullyQualifiedNames)
                if (kvp.Value == "$ambiguous$")
                    toRemove.Add(kvp.Key);
            foreach (var key in toRemove)
                fullyQualifiedNames.Remove(key);
            ExtractQualifiedNames(toAdd, "", "", fullyQualifiedNames, addedNames, overwrite:true);
            CopyDeclAndRules(toAdd, domain, "", "", fullyQualifiedNames, copyOnlyDecls: false, replaceComprehension: replaceComprehension);
            AddDataAndIdentity(toAdd, domain, "", "", fullyQualifiedNames);
        }
        private static void GetNamespaces(Domain module, string ns, string completens, Dictionary<string, Module> modules, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames)
        {
            // If this module has no references, copy its declarations and rules.
            if (module.ModRefs.Count() == 0)
                ExtractQualifiedNames(module, ns, completens, fullyQualifiedNames, addedNames, overwrite: true);
            // Otherwise, add its references first, and then the domain itself.
            else
            {
                // Collect the added identifiers for each reference.
                var localAddedNames = new List<HashSet<string>>();
                var localQualNames = new List<Dictionary<string, string>>();
                foreach (var comp in module.ModRefs)
                {
                    Module mod;
                    // Only domain references are allowed.
                    if (!modules.TryGetValue(comp.Name, out mod) || !(mod is Domain))
                        throw new UserException("Invalid module reference", comp);
                    // newns is the new namespace for the added stuff
                    var localns = String.IsNullOrEmpty(comp.Rename) ? "" : (comp.Rename + ".");
                    var newns = ns + localns;
                    var newcompletens = completens + (String.IsNullOrEmpty(comp.Rename) ? comp.Name : comp.Rename) + ".";
                    
                    var localAddNames = new HashSet<string>();
                    var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                    GetNamespaces((Domain)mod, newns, newcompletens, modules, fullyQualifiedNames, localAddNames);
                    localAddedNames.Add(localAddNames);
                    localQualNames.Add(localQualName);
                }

                // And now merge all the namespaces
                foreach (var localQualName in localQualNames)
                    foreach (var kvp in localQualName)
                        if (!fullyQualifiedNames.ContainsKey(kvp.Key))
                            fullyQualifiedNames[kvp.Key] = kvp.Value;

                ExtractQualifiedNames(module, ns, completens, fullyQualifiedNames, addedNames, overwrite: true);                
            }
        }
        /// <summary>
        /// Resolve namespaces in a transformation to their fully qualified version.
        /// </summary>
        private static void ResolveNamespaces(Transform result, Transform original, Dictionary<string, Module> modules, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames)
        {
            HashSet<string> qualifiedNames = new HashSet<string>();
            var localAddedNames = new List<HashSet<string>>();
            var localQualNames = new List<Dictionary<string, string>>();
            foreach (var child in original.Inputs)
            {
                if (child is Param && ((Param)child).Type is ModRef)
                {
                    var modref = (ModRef)((Param)child).Type;
                    Module dom;
                    if (!modules.TryGetValue(modref.Name, out dom) || !(dom is Domain))
                        throw new UserException("Invalid module reference", modref);
                    var local = new HashSet<string>();
                    var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                    var ns = modref.Rename + ".";
                    GetNamespaces((Domain)dom, ns, ns, modules, fullyQualifiedNames, local);
                    UpdateQualifiedNames(ns, fullyQualifiedNames, local);
                    localAddedNames.Add(local);
                    localQualNames.Add(localQualName);
                }
            }
            foreach (var child in original.Outputs)
            {
                if (child is Param && ((Param)child).Type is ModRef)
                {
                    var modref = (ModRef)((Param)child).Type;
                    Module dom;
                    if (!modules.TryGetValue(modref.Name, out dom) || !(dom is Domain))
                        throw new UserException("Invalid module reference", modref);
                    var local = new HashSet<string>();
                    var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                    var ns = modref.Rename + ".";
                    GetNamespaces((Domain)dom, ns, ns, modules, fullyQualifiedNames, local);
                    UpdateQualifiedNames(ns, fullyQualifiedNames, local);
                    localAddedNames.Add(local);
                    localQualNames.Add(localQualName);
                }
            }

            // And now merge all the namespaces
            foreach (var localQualName in localQualNames)
                foreach (var kvp in localQualName)
                    if (!fullyQualifiedNames.ContainsKey(kvp.Key))
                        fullyQualifiedNames[kvp.Key] = kvp.Value;
            // Remove ambiguous declarations
            HashSet<string> toRemove = new HashSet<string>();
            foreach (var kvp in fullyQualifiedNames)
                if (kvp.Value == "$ambiguous$")
                    toRemove.Add(kvp.Key);
            foreach (var key in toRemove)
                fullyQualifiedNames.Remove(key);

            ExtractQualifiedNames(original, "", "", fullyQualifiedNames, addedNames, overwrite: true);
            CopyDeclAndRules(original, result, "", "", fullyQualifiedNames, copyOnlyDecls: false, replaceComprehension: false);
        }
        /// <summary>
        /// Resolve namespaces in a transformation to their fully qualified version.
        /// </summary>
        private static void ResolveNamespaces(Model result, Model original, Dictionary<string, Module> modules, Dictionary<string, string> fullyQualifiedNames, HashSet<string> addedNames)
        {
            HashSet<string> qualifiedNames = new HashSet<string>();
            var localAddedNames = new List<HashSet<string>>();
            var localQualNames = new List<Dictionary<string, string>>();
            foreach (var modref in original.Compositions)
            {
                Module dom;
                if (!modules.TryGetValue(modref.Name, out dom) || !(dom is Domain))
                    throw new UserException("Invalid module reference", modref);
                var local = new HashSet<string>();
                var localQualName = new Dictionary<string, string>(fullyQualifiedNames);
                var ns = modref.Rename + ".";
                GetNamespaces((Domain)dom, ns, ns, modules, fullyQualifiedNames, local);
                UpdateQualifiedNames(ns, fullyQualifiedNames, local);
                localAddedNames.Add(local);
                localQualNames.Add(localQualName);
            }

            // And now merge all the namespaces
            foreach (var localQualName in localQualNames)
                foreach (var kvp in localQualName)
                    if (!fullyQualifiedNames.ContainsKey(kvp.Key))
                        fullyQualifiedNames[kvp.Key] = kvp.Value;
            // Remove ambiguous declarations
            HashSet<string> toRemove = new HashSet<string>();
            foreach (var kvp in fullyQualifiedNames)
                if (kvp.Value == "$ambiguous$")
                    toRemove.Add(kvp.Key);
            foreach (var key in toRemove)
                fullyQualifiedNames.Remove(key);

            ExtractQualifiedNames(original, "", "", fullyQualifiedNames, addedNames, overwrite: true);
            //CopyDeclAndRules(original, result, "", "", fullyQualifiedNames, copyOnlyDecls: false);
            CopyFacts(original, result, "", "", fullyQualifiedNames);
        }

        /// <summary>
        /// Replace the comprehensions in a rule, and return a list of new rules for the extracted comprehensions.
        /// </summary>
        static int compr_counter = 0;
        private static Node ReplaceComprehensions(Module module, Node node, Body rule_body, List<TypeDecl> newTypes, List<Nodes.RuleNode> newRules)
        {
            if (node == null) return null;
            if (node is BuiltinFuncTerm)
            {
                var func = (BuiltinFuncTerm)node;
                for (int i = 0; i < func.args.Count; ++i)
                    func.args[i] = ReplaceComprehensions(module, func.args[i], rule_body, newTypes, newRules);
            }
            else if (node is RelConstr)
            {
                var rel = (RelConstr)node;
                //if (rel.Op == RelKind.No)
                rel.Arg1 = ReplaceComprehensions(module, rel.Arg1, rule_body, newTypes, newRules);
                rel.Arg2 = ReplaceComprehensions(module, rel.Arg2, rule_body, newTypes, newRules);
            }
            else if (node is Compr)
            {
                var comp = (Compr)node;

                // Create type term
                var name = String.Format("#compr_{0}#{1}", compr_counter++, comp.Heads.Count);
                var decl = ConDecl.Create(Span.Unknown, name, false, null);
                // One field for each head
                foreach (var head in comp.Heads)
                    decl.Fields.Add(new Field("", new Id("Data"), false));
                // And one field for every variable referred in the body
                var vars = new HashSet<Var>();
                foreach (var head in comp.Bodies)
                    vars.UnionWith(head.GetVariables());
                foreach (var v in vars)
                    decl.Fields.Add(new Field("", new Id("Data"), false));
                newTypes.Add(decl);

                // Create rule for type term
                var newrule = new Nodes.RuleNode();
                // The head of the rule contains the head of the comprehension, plus the referred variables in its body
                var headterm = new UserFuncTerm(new Id(name));
                foreach (var head in comp.Heads)
                    headterm.AddArg(head);
                foreach (var v in vars)
                    headterm.AddArg(v);
                newrule.AddHead(headterm);
                // The body is the body of the comprehension, plus the body of the original rule
                var body = new Body();
                newrule.AddBody(body);
                // Add the body of the original rule, except for terms that contain the comprehension itself and for negated terms
                foreach (var c in rule_body.constraints)
                    if (!c.Contains(node) && !(c is RelConstr && ((RelConstr)c).Op == RelKind.No))
                        body.AddConstr(c);
                // Add the body of the comprehension
                foreach (var b in comp.Bodies)
                    foreach (var c in b.constraints)
                        body.AddConstr(c);
                newRules.Add(newrule);

                // Substitute the comprehension with a corresponding UserFuncTerm (already defined as headterm)
                return headterm;
            }
            else
                foreach (var child in node.Children)
                    ReplaceComprehensions(module, child, rule_body, newTypes, newRules);
            return node;
        }
    }
}
